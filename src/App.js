import 'react-perfect-scrollbar/dist/css/styles.css';
import React from 'react';
import { useSelector } from 'react-redux';
import { useRoutes } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core';
import GlobalStyles from 'src/app/components/GlobalStyles';
import theme from 'src/app/theme';
import routes from 'src/routes';
import moment from 'moment';

import 'moment/locale/vi';
require('dotenv');

const App = () => {
  moment.locale('vi');
  const accInfo = useSelector(state => state.userSlice.accInfo);
  const routing = useRoutes(routes(accInfo?.authorities));

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      {routing}
    </ThemeProvider>
  );
};

export default App;
