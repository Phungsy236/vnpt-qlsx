import * as Yup from 'yup';

export const PAGE_SIZE_LIST = 10;

export const MESSAGE_TYPE = {
  error: 'error',
  warning: 'warning',
  info: 'info',
  success: 'success'
};

export const userShape_const = {
  email: Yup.string()
    .email('Email chưa đúng định dạng')
    .max(255)
    .required('Bạn cần nhập Email'),
  fullName: Yup.string()
    .min(2, 'Tên quá ngắn')
    .max(50, 'Tên quá dài')
    .required('Bạn cần nhập đầy đủ họ tên'),
  phone: Yup.string()
    .matches(new RegExp('^[0-9-+]{9,11}$'), 'Số điện thoại phải đúng định dạng')
    .required('Bạn cần nhập số điện thoại'),
  address: Yup.string()
    .max(100, 'Quá dài')
    .required('Bạn cần điền địa chỉ')
  // dob: Yup.date().required('Bạn cần chọn ngày sinh')
};

export const STATUS_API = {
  PENDING: 0,
  SUCCESS: 1,
  ERROR: 2
};

export const ACTION_TABLE = {
  CREATE: 'create',
  EDIT: 'edit',
  PREVIEW: 'preview',
  DELETE: 'delete'
};

export const HTTP_GETTYPE = {
  ALL: 0,
  ALL_PAGINATION: 1,
  DETAIL: 2
};
export const TIMEOUT_DEFAULT = 5000;
export const MSG_TIMEOUT_REQUEST = 'Server phản hồi quá lâu , vui lòng thử lại';

export const SORT_TYPE = {
  DESC: 1,
  ASC: 2
};
export const HEADER_AUTH_KEY_NAME = 'access-token';
export const ACTIVE_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0
};
export const ROLE_TYPE = {
  SYSTEM: 1,
  CUSTOM: 0
};
export const TYPE_DELETE = {
  SINGLE: 'single',
  MULTI: 'multi'
};
export const LIST_ROLE = {
  Station_Create: 'Station_Create',
  Station_View: 'Station_View',
  Station_Update: 'Station_Update',
  Station_Delete: 'Station_Delete',
  User_Create: 'User_Create',
  User_View: 'User_View',
  User_Update: 'User_Update',
  User_Delete: 'User_Delete',
  Role_Create: 'Role_Create',
  Role_View: 'Role_View',
  Role_Edit: 'Role_Edit',
  Role_Delete: 'Role_Delete',
  Privilege_View: 'Privilege_View',
  Product_Create: 'Product_Create',
  Product_View: 'Product_View',
  Product_Update: 'Product_Update',
  Product_Delete: 'Product_Delete',
  Cycle_Time_Create: 'Cycle_Time_Create',
  Cycle_Time_View: 'Cycle_Time_View',
  Cycle_Time_Update: 'Cycle_Time_Update',
  Cycle_Time_Delete: 'Cycle_Time_Delete',

  Indentify_View: 'Indentify_View',
  Strategy_View: 'Strategy_View',
  Result_View: 'Result_View',

  Indentify_Create: 'Indentify_Create',
  // Strategy_Create: 'Strategy_Create',
  // Result_Create: 'Result_Create',

  Indentify_Update: 'Indentify_Update',
  Strategy_Update: 'Strategy_Update',
  Result_Update: 'Result_Update'
};
export const MANUFACTURING_TYPE = {
  VIEW_ALL: 0,
  INDENTIFY: 1,
  STRATEGY: 2,
  RESULT: 3,
  CREATE: 4
};
export const SHIFT = ['Sáng', 'Chiều', 'Tối'];
