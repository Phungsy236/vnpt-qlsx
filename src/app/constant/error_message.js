export const error = {
  userAndPermission: {
    roleExist: 'Tên chức vụ đã tồn tại , vui lòng chọn tên khác',
    emailExists: 'Email này đã được sử dụng'
  },
  emailIncorect: 'Email không đúng',
  passwordIncorect: 'Mật khẩu không đúng',
  accountInactive: 'Tài khoản chưa được kích hoạt'
};
