export const MESSAGE = {
  /*==================role===============================*/
  CONFIRM_DELETE_ROLE: 'Bạn có muốn xóa chức vụ này?',
  DELETE_ROLE_SUCCESS: 'Xóa chức vụ thành công.',
  DELETE_ROLE_FAIL: 'Xóa chức vụ thất bại',
  UPDATE_ROLE_SUCCESS: 'Cập nhật chức vụ thành công!',
  UPDATE_ROLE_FAIL: 'Cập nhật chức vụ thất bại!',
  CREATE_ROLE_FAIL: 'Tạo mới chức vụ thất bại!',
  CREATE_ROLE_SUCCESS: 'Tạo mới chức vụ thành công!',

  /*==================privilege=======================*/
  CONFIRM_DELETE_PRIVILEGE: 'Bạn có muốn xóa quyền này?',
  DELETE_PRIVILEGE_SUCCESS: 'Xóa quyền thành công.',
  DELETE_PRIVILEGE_FAIL: 'Xóa quyền thất bại',
  UPDATE_PRIVILEGE_SUCCESS: 'Cập nhật quyền thành công!',
  UPDATE_PRIVILEGE_FAIL: 'Cập nhật quyền thất bại!',
  CREATE_PRIVILEGE_FAIL: 'Tạo mới quyền thất bại!',
  CREATE_PRIVILEGE_SUCCESS: 'Tạo mới quyền thành công!',
  /*===============user==========================*/
  CONFIRM_DELETE_USER: 'Bạn có muốn xóa người dùng này?',
  DELETE_USER_SUCCESS: 'Xóa người dùng thành công.',
  DELETE_USER_FAIL: 'Xóa người dùng thất bại',
  UPDATE_USER_SUCCESS: 'Cập nhật tài khoản thành công!',
  UPDATE_USER_FAIL: 'Cập nhật tài khoản thất bại!',
  CREATE_USER_FAIL: 'Tạo mới tài khoản thất bại!',
  CREATE_USER_SUCCESS: 'Tạo mới tài khoản thành công!',
  CHANGE_STATUS_USER_SUCCESS: 'Thay đổi trạng thái tài khoản thành công!',
  CHANGE_STATUS_USER_FAIL: 'Thay đổi trạng thái tài khoản thất bại!',
  CHANGE_ROLE_USER_SUCCESS: 'Thay đổi quyền hạn tài khoản thành công!',
  CHANGE_ROLE_USER_FAIL: 'Thay đổi quyền hạn tài khoản thất bại!',
  /*===============station===================*/
  CONFIRM_DELETE_STATION: 'Bạn có muốn xóa công đoạn sản xuất này?',
  CONFIRM_DELETE_MULTI_STATION:
    'Bạn có muốn xóa tất cả công đoạn sản xuất này?',
  DELETE_STATION_SUCCESS: 'Xóa công đoạn sản xuất thành công.',
  DELETE_STATION_FAIL: 'Xóa công đoạn sản xuất thất bại',
  UPDATE_STATION_SUCCESS: 'Cập nhật công đoạn sản xuất thành công',
  UPDATE_STATION_FAIL: 'Cập nhật công đoạn sản xuất thất bại',
  CREATE_STATION_SUCCESS: 'Tạo mới công đoạn sản xuất thành công',
  CREATE_STATION_FAIL: 'Tạo mới công đoạn sản xuất thất bại',

  /*=================product===================*/
  CONFIRM_DELETE_PRODUCT: 'Bạn có muốn xóa sản phẩm này?',
  DELETE_PRODUCT_SUCCESS: 'Xóa sản phẩm thành công.',
  DELETE_PRODUCT_FAIL: 'Xóa sản phẩm thất bại',
  UPDATE_PRODUCT_SUCCESS: 'Cập nhật sản phẩm thành công',
  UPDATE_PRODUCT_FAIL: 'Cập nhật sản phẩm thất bại',
  CREATE_PRODUCT_SUCCESS: 'Tạo mới sản phẩm thành công',
  CREATE_PRODUCT_FAIL: 'Tạo mới sản phẩm thất bại',

  /*==================manufacturing=================*/
  CONFIRM_DELETE_MANUFACTURING: 'Bạn có muốn xóa bản ghi sản xuất này?',
  DELETE_MANUFACTURING_SUCCESS: 'Xóa bản ghi sản xuất thành công.',
  DELETE_MANUFACTURING_FAIL: 'Xóa bản ghi sản xuất thất bại',
  UPDATE_MANUFACTURING_SUCCESS: 'Cập nhật bản ghi sản xuất thành công',
  UPDATE_MANUFACTURING_FAIL: 'Cập nhật bản ghi sản xuất thất bại',
  CREATE_MANUFACTURING_SUCCESS: 'Tạo mới bản ghi sản xuất thành công',
  CREATE_MANUFACTURING_FAIL: 'Tạo mới bản ghi sản xuất thất bại',
  BTN_YES: 'Đồng ý',
  BTN_CANCEL: 'Hủy'
};
