export const _convertObjectToQuery = obj => {
  let query = '';
  for (let key in obj) {
    if (obj[key] !== undefined) {
      if (query) {
        query += `&${key}=${obj[key]}`;
      } else {
        query += `${key}=${obj[key]}`;
      }
    }
  }
  return query;
};
export const checkPermission = (listPermissionOfUser, permissionId) => {
  return listPermissionOfUser.includes(permissionId);
};
export const parseObjToSearchQuery = obj => {
  let query = '';
  for (let key in obj) {
    if (!obj[key]) continue;
    if (isNaN(obj[key])) {
      query += `;${key}==*${obj[key]}*`;
    } else {
      query += `;${key}==${obj[key]}`;
    }
  }
  return query.slice(1);
};
export const mapStringToObjectProperties = (string, object) => {
  if (!string) return;
  let temp = string.split('.');
  temp.splice(0, 1);
  let result = object;
  for (let item of temp) {
    result = result[item];
  }
  return result;
};
