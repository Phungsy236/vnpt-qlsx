import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Box,
  Divider,
  Drawer,
  Hidden,
  List,
  ListItem,
  makeStyles
} from '@material-ui/core';
import { LIST_ROLE } from 'src/app/constant/config';
import NavItem from './NavItem';
import {
  HomeWorkOutlined,
  LibraryBooksOutlined,
  MemoryOutlined,
  TimelineOutlined,
  GroupAddOutlined,
  AccessAlarmOutlined,
  FormatIndentDecreaseOutlined,
  DehazeOutlined
} from '@material-ui/icons';
import clsx from 'clsx';

const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
  mobileDrawer: {
    width: 256
  },
  desktopDrawer: {
    width: 256,
    top: 64,
    height: 'calc(100% - 64px)'
  },
  avatar: {
    cursor: 'pointer',
    width: 64,
    height: 64
  },
  nested: {
    paddingLeft: 50
  },
  drawer: {
    width: drawerWidth,
    top: 64,
    flexShrink: 0,
    whiteSpace: 'nowrap'
  },
  drawerOpen: {
    top: 64,
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerClose: {
    top: 64,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1
    }
  }
}));

const NavBar = ({ onMobileClose, openMobile, accInfo }) => {
  const classes = useStyles();
  const location = useLocation();
  let authorities = accInfo.authorities;
  // const dataLogin = useSelector(state => state.authSlice.dataLogin);
  const [expand, setExpand] = React.useState(true);
  useEffect(() => {
    if (openMobile && onMobileClose) {
      onMobileClose();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location.pathname]);
  const [isOpen] = useState(true);

  const content = (
    <Box height="100%" display="flex" flexDirection="column">
      <Divider />
      <Box p={2}>
        <List>
          <Hidden mdDown>
            <ListItem>
              <Box
                style={{
                  display: 'flex',
                  width: '100%',
                  justifyContent: 'center',
                  cursor: 'pointer'
                }}
                onClick={() => setExpand(!expand)}
              >
                {expand ? <FormatIndentDecreaseOutlined /> : <DehazeOutlined />}
              </Box>
            </ListItem>
          </Hidden>
          <NavItem
            href={'/vnpt/dashboard'}
            icon={HomeWorkOutlined}
            title="Trang chủ"
          />

          <NavItem
            icon={GroupAddOutlined}
            title="Quản lý user"
            urlBase={'/vnpt/user'}
            globalExpand={expand}
            authorization={authorities?.some(item =>
              [
                LIST_ROLE.User_View,
                LIST_ROLE.Role_View,
                LIST_ROLE.Privilege_View
              ].includes(item)
            )}
            arrayNested={[
              {
                href: `/vnpt/user/all`,
                title: 'Nhân sự',
                authorization: authorities.includes(LIST_ROLE.User_View)
              },
              {
                href: `/vnpt/user/role`,
                title: 'Chức vụ',
                authorization: authorities.includes(LIST_ROLE.Role_View)
              },
              {
                href: `/vnpt/user/privilege`,
                title: 'Quyền hạn',
                authorization: authorities.includes(LIST_ROLE.Privilege_View)
              }
            ]}
          />
          {
            <NavItem
              authorization={authorities.includes(LIST_ROLE.Station_View)}
              href={'/vnpt/station'}
              icon={TimelineOutlined}
              title="Công đoạn sản xuất"
            />
          }
          {
            <NavItem
              authorization={authorities.includes(LIST_ROLE.Product_View)}
              href={'/vnpt/product'}
              icon={MemoryOutlined}
              title="Sản phẩm"
            />
          }
          {
            <NavItem
              authorization={authorities.includes(LIST_ROLE.Cycle_Time_View)}
              href={'/vnpt/lifecycle_time'}
              icon={AccessAlarmOutlined}
              title="Ước lượng thời gian"
            />
          }
          {
            <NavItem
              authorization={authorities?.some(item =>
                [
                  LIST_ROLE.Indentify_View,
                  LIST_ROLE.Result_View,
                  LIST_ROLE.Strategy_View
                ].includes(item)
              )}
              href={'/vnpt/manufacturing'}
              icon={LibraryBooksOutlined}
              title="Kết quả sản xuất"
            />
          }
        </List>
      </Box>
      <Box flexGrow={1} />
    </Box>
  );

  return (
    <>
      <Hidden lgUp>
        <Drawer
          anchor="left"
          classes={{ paper: classes.mobileDrawer }}
          onClose={onMobileClose}
          open={openMobile}
          variant="temporary"
        >
          {content}
        </Drawer>
      </Hidden>
      <Hidden mdDown>
        <Drawer
          anchor="left"
          // classes={{ paper: classes.desktopDrawer }}
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: expand,
            [classes.drawerClose]: !expand
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: expand,
              [classes.drawerClose]: !expand
            })
          }}
          open={isOpen}
          variant="permanent"
        >
          {content}
        </Drawer>
      </Hidden>
    </>
  );
};

NavBar.propTypes = {
  onMobileClose: PropTypes.func,
  openMobile: PropTypes.bool
};

NavBar.defaultProps = {
  onMobileClose: () => {},
  openMobile: false
};

export default NavBar;
