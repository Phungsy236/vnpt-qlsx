import {
  Checkbox,
  makeStyles,
  TableBody,
  TableCell,
  TableRow,
  Tooltip
} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import moment from 'moment';
import PropTypes from 'prop-types';
import React from 'react';
import TableNullData from '../../components/TableNullData';
import TextHightLight from '../../components/TextHightLight';
import { ACTION_TABLE } from '../../constant/config';

TableResults.propTypes = {
  listProduct: PropTypes.array.isRequired,
  listIdCheck: PropTypes.array.isRequired,
  setCheckAll: PropTypes.func.isRequired,
  setListIdCheck: PropTypes.func.isRequired,
  onEditProduct: PropTypes.func,
  actionDeleteProductRef: PropTypes.func,
  isNullData: PropTypes.bool
};

function TableResults({
  listProduct,
  listIdCheck,
  setCheckAll,
  setListIdCheck,
  onEditProduct,
  actionDeleteProductRef,
  permission,
  isNullData
}) {
  const classes = useStyles();
  if (isNullData) return <TableNullData numberOfTableColumn={7} />;
  return (
    <TableBody>
      {listProduct?.map((product, index) => (
        <TableRow hover key={product.id}>
          <TableCell>
            <Checkbox
              checked={listIdCheck.includes(product.id)}
              onChange={() => {
                setCheckAll(true);
                if (listIdCheck.includes(product.id)) {
                  setListIdCheck(
                    listIdCheck.filter(item => item !== product.id)
                  );
                } else {
                  setListIdCheck(listIdCheck.concat(product.id));
                }
              }}
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
          </TableCell>
          <TableCell
            onClick={() => onEditProduct(ACTION_TABLE.PREVIEW, product)}
          >
            <TextHightLight
              type={'blue'}
              content={product.name}
              isCursor={true}
            />
          </TableCell>
          <TableCell>{product.description}</TableCell>
          <TableCell>{moment(product.created).format('LLLL')}</TableCell>
          <TableCell>{product.createdBy}</TableCell>
          <TableCell>
            <div className={classes.groupAction}>
              <div
                className="action"
                onClick={() => onEditProduct(ACTION_TABLE.PREVIEW, product)}
              >
                <Tooltip title="Chi tiết">
                  <VisibilityIcon />
                </Tooltip>
              </div>

              {permission.update && (
                <IconButton
                  className="action"
                  onClick={() => onEditProduct(ACTION_TABLE.EDIT, product)}
                >
                  <Tooltip title="Chỉnh sửa">
                    <EditIcon />
                  </Tooltip>
                </IconButton>
              )}
              {permission.delete && (
                <IconButton
                  className="action"
                  onClick={() => actionDeleteProductRef(product)}
                >
                  <Tooltip title="Xóa">
                    <DeleteIcon />
                  </Tooltip>
                </IconButton>
              )}
            </div>
          </TableCell>
        </TableRow>
      ))}
    </TableBody>
  );
}

const useStyles = makeStyles(theme => ({
  groupAction: {
    display: 'flex',
    alignItems: 'center',
    '& .action': {
      cursor: 'pointer',
      color: '#333535',
      padding: '0 5px',
      '&:hover': {
        color: '#3f51b5'
      }
    }
  }
}));
export default TableResults;
