import { Container, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DialogConfirm from 'src/app/components/DialogConfirm';
import Page from 'src/app/components/Page';
import ToastMessage from 'src/app/components/ToastMessage';
import {
  ACTION_TABLE,
  HEADER_AUTH_KEY_NAME,
  LIST_ROLE,
  MESSAGE_TYPE,
  PAGE_SIZE_LIST,
  STATUS_API,
  TYPE_DELETE
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { DefaultSearchParams } from 'src/app/models/DefaultSearchParams';
import {
  deleteMultiProduct,
  deleteProduct,
  getListProduct,
  resetStatus
} from 'src/features/productSlice';
import {
  closeDialogConfirm,
  showDialogConfirm,
  showToast
} from 'src/features/uiSlice';
import DetailsProduct from './product_detail/index';
import TableView from './TableView';
import Cookie from 'js-cookie';

const ProductListView = ({ authorities }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  // get data api
  const listProduct = useSelector(state => state.productSlice.listProduct);
  const totalProduct = useSelector(state => state.productSlice.totalProduct);
  const statusGetAll = useSelector(state => state.productSlice.statusGetAll);
  const statusDeleteMulti = useSelector(
    state => state.productSlice.statusDeleteMulti
  );
  const statusDeleteProduct = useSelector(
    state => state.productSlice.statusDeleteProduct
  );

  const [isShowModalProductDetails, setIsShowModalProductDetails] = useState(
    false
  );
  const [isDeleted, setIsDeleted] = useState(false);
  const [deleteItem, setDeleteItem] = useState(null);
  const permission = {
    create: authorities.includes(LIST_ROLE.Product_Create),
    delete: authorities.includes(LIST_ROLE.Product_Delete),
    update: authorities.includes(LIST_ROLE.Product_Update)
  };
  const [sendData, setSendData] = useState({
    data: undefined,
    type: null
  });

  const [params, setParams] = useState(new DefaultSearchParams());

  useEffect(() => {
    console.log('api call');
    if (!Cookie.get(HEADER_AUTH_KEY_NAME)) return;
    dispatch(getListProduct(params));
  }, [dispatch, params]);

  const getListProductWithParams = data => {
    console.log('called');
    const paramValue = Object.assign({}, params, data);
    setParams(paramValue);
  };

  const showDetailsProduct = data => {
    if (!data) return;
    setSendData(data);
    setIsShowModalProductDetails(true);
  };
  const createNewProduct = () => {
    setSendData({ data: {}, type: ACTION_TABLE.CREATE });
    setIsShowModalProductDetails(true);
  };

  const [typeDelete, setTypeDelete] = useState(TYPE_DELETE.SINGLE);
  const handleDeleteMultiProduct = data => {
    setTypeDelete(TYPE_DELETE.MULTI);
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };

  const confirmDeleteMultiProduct = () => {
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deleteMultiProduct(deleteItem));
  };

  const handleDeleteProduct = data => {
    setTypeDelete(TYPE_DELETE.SINGLE);
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };

  const confirmDeleteProduct = () => {
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deleteProduct(deleteItem));
  };

  const willShowToast =
    statusDeleteProduct === STATUS_API.SUCCESS ||
    statusDeleteProduct === STATUS_API.ERROR ||
    statusDeleteMulti === STATUS_API.SUCCESS ||
    statusDeleteMulti === STATUS_API.ERROR;
  useEffect(() => {
    if (willShowToast) dispatch(showToast());
  }, [statusDeleteProduct, statusDeleteMulti]);
  const closeModalProductDetails = () => {
    dispatch(resetStatus());
    setIsShowModalProductDetails(false);
  };

  return (
    <Page className={classes.root} title="Product">
      <Container maxWidth={false}>
        <TableView
          actionDetailsProductRef={showDetailsProduct}
          actionDeleteProductRef={handleDeleteProduct}
          actionDeleteMultiProductRef={handleDeleteMultiProduct}
          listProduct={listProduct}
          totalProduct={totalProduct}
          isLoading={statusGetAll === STATUS_API.PENDING}
          getListProductRef={getListProductWithParams}
          createRef={createNewProduct}
          permission={permission}
        />
      </Container>
      {typeDelete === TYPE_DELETE.SINGLE ? (
        <DialogConfirm
          title={MESSAGE.CONFIRM_DELETE_STATION}
          textOk={MESSAGE.BTN_YES}
          textCancel={MESSAGE.BTN_CANCEL}
          callbackOk={() => confirmDeleteProduct()}
          callbackCancel={() => dispatch(closeDialogConfirm())}
        />
      ) : (
        <DialogConfirm
          title={MESSAGE.CONFIRM_DELETE_STATION}
          textOk={MESSAGE.BTN_YES}
          textCancel={MESSAGE.BTN_CANCEL}
          callbackOk={() => confirmDeleteMultiProduct()}
          callbackCancel={() => dispatch(closeDialogConfirm())}
        />
      )}

      {isShowModalProductDetails && sendData && (
        <DetailsProduct
          open={isShowModalProductDetails}
          sendData={sendData}
          closeRef={closeModalProductDetails}
        />
      )}

      {(statusDeleteProduct === STATUS_API.SUCCESS ||
        statusDeleteProduct === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteProduct === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_STATION_SUCCESS
                : MESSAGE.DELETE_STATION_FAIL
            }
            type={
              statusDeleteProduct === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusDeleteMulti === STATUS_API.SUCCESS ||
        statusDeleteMulti === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_STATION_SUCCESS
                : MESSAGE.DELETE_STATION_FAIL
            }
            type={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </Page>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

export default ProductListView;
