import {
  Checkbox,
  makeStyles,
  TableBody,
  TableCell,
  TableRow,
  Tooltip
} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import moment from 'moment';
import PropTypes from 'prop-types';
import React from 'react';
import TableNullData from '../../components/TableNullData';
import TextHightLight from '../../components/TextHightLight';
import { ACTION_TABLE } from '../../constant/config';

TableResults.propTypes = {
  listStation: PropTypes.array.isRequired,
  listIdCheck: PropTypes.array.isRequired,
  setCheckAll: PropTypes.func.isRequired,
  setListIdCheck: PropTypes.func.isRequired,
  onEditStation: PropTypes.func,
  actionDeleteStationRef: PropTypes.func,
  isNullData: PropTypes.bool
};

function TableResults({
  listStation,
  listIdCheck,
  setCheckAll,
  setListIdCheck,
  onEditStation,
  actionDeleteStationRef,
  isNullData,
  permission
}) {
  const classes = useStyles();
  if (isNullData) return <TableNullData numberOfTableColumn={7} />;
  return (
    <TableBody>
      {listStation?.map(station => (
        <TableRow hover key={station.id}>
          <TableCell>
            <Checkbox
              checked={listIdCheck.includes(station.id)}
              onChange={() => {
                setCheckAll(true);
                if (listIdCheck.includes(station.id)) {
                  setListIdCheck(
                    listIdCheck.filter(item => item !== station.id)
                  );
                } else {
                  setListIdCheck(listIdCheck.concat(station.id));
                }
              }}
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
          </TableCell>
          <TableCell
            onClick={() => onEditStation(ACTION_TABLE.PREVIEW, station)}
          >
            <TextHightLight
              type={'blue'}
              content={station.name}
              isCursor={true}
            />
          </TableCell>
          <TableCell>{station.description}</TableCell>
          <TableCell>{moment(station.created).format('LLLL')}</TableCell>
          <TableCell>{station.createdBy}</TableCell>
          <TableCell>
            <div className={classes.groupAction}>
              <IconButton
                className="action"
                onClick={() => onEditStation(ACTION_TABLE.PREVIEW, station)}
              >
                <Tooltip title="Chi tiết">
                  <VisibilityIcon />
                </Tooltip>
              </IconButton>

              {permission.update && (
                <IconButton
                  className="action"
                  onClick={() => onEditStation(ACTION_TABLE.EDIT, station)}
                >
                  <Tooltip title="Chỉnh sửa">
                    <EditIcon />
                  </Tooltip>
                </IconButton>
              )}
              {permission.delete && (
                <IconButton
                  className="action"
                  onClick={() => actionDeleteStationRef(station)}
                >
                  <Tooltip title="Xóa">
                    <DeleteIcon />
                  </Tooltip>
                </IconButton>
              )}
            </div>
          </TableCell>
        </TableRow>
      ))}
    </TableBody>
  );
}

const useStyles = makeStyles(theme => ({
  groupAction: {
    display: 'flex',
    alignItems: 'center',
    '& .action': {
      cursor: 'pointer',
      padding: '0 5px',
      color: '#333535',
      '&:hover': {
        color: '#3f51b5'
      }
    }
  }
}));
export default TableResults;
