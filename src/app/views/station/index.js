import { Container, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DialogConfirm from 'src/app/components/DialogConfirm';
import Page from 'src/app/components/Page';
import ToastMessage from 'src/app/components/ToastMessage';
import {
  ACTION_TABLE,
  HEADER_AUTH_KEY_NAME,
  LIST_ROLE,
  MESSAGE_TYPE,
  PAGE_SIZE_LIST,
  STATUS_API,
  TYPE_DELETE
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { DefaultSearchParams } from 'src/app/models/DefaultSearchParams';
import {
  deleteMultiStation,
  deleteStation,
  getListStation,
  resetStatus
} from 'src/features/stationSlice';
import {
  closeDialogConfirm,
  showDialogConfirm,
  showToast
} from 'src/features/uiSlice';
import DetailsStation from './station_detail/index';
import TableView from './TableView';
import Cookie from 'js-cookie';

const StationListView = ({ authorities }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  // get data api
  const listStation = useSelector(state => state.stationSlice.listStation);
  const totalStation = useSelector(state => state.stationSlice.totalStation);
  const statusGetAll = useSelector(state => state.stationSlice.statusGetAll);
  const statusDeleteMulti = useSelector(
    state => state.stationSlice.statusDeleteMulti
  );
  const statusDeleteStation = useSelector(
    state => state.stationSlice.statusDeleteStation
  );

  const [isShowModalStationDetails, setIsShowModalStationDetails] = useState(
    false
  );
  const [isDeleted, setIsDeleted] = useState(false);
  const [deleteItem, setDeleteItem] = useState(null);

  const [sendData, setSendData] = useState({
    data: undefined,
    type: null
  });

  const [params, setParams] = useState(new DefaultSearchParams());

  useEffect(() => {
    if (!Cookie.get(HEADER_AUTH_KEY_NAME)) return;
    dispatch(getListStation(params));
  }, [dispatch, params]);

  const getListStationWithParams = data => {
    const paramValue = Object.assign({}, params, data);
    setParams(paramValue);
  };
  const permission = {
    create: authorities.includes(LIST_ROLE.Station_Create),
    delete: authorities.includes(LIST_ROLE.Station_Delete),
    update: authorities.includes(LIST_ROLE.Station_Update)
  };
  const showDetailsStation = data => {
    if (!data) return;
    setSendData(data);
    setIsShowModalStationDetails(true);
  };
  const createNewStation = () => {
    setSendData({ data: {}, type: ACTION_TABLE.CREATE });
    setIsShowModalStationDetails(true);
  };

  const [typeDelete, setTypeDelete] = useState(TYPE_DELETE.SINGLE);
  const handleDeleteMultiStation = data => {
    setTypeDelete(TYPE_DELETE.MULTI);
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };

  const confirmDeleteMultiStation = () => {
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deleteMultiStation(deleteItem));
  };

  const handleDeleteStation = data => {
    setTypeDelete(TYPE_DELETE.SINGLE);
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };

  const confirmDeleteStation = () => {
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deleteStation(deleteItem));
  };

  const willShowToast =
    statusDeleteStation === STATUS_API.SUCCESS ||
    statusDeleteStation === STATUS_API.ERROR ||
    statusDeleteMulti === STATUS_API.SUCCESS ||
    statusDeleteMulti === STATUS_API.ERROR;
  useEffect(() => {
    if (willShowToast) dispatch(showToast());
  }, [statusDeleteStation, statusDeleteMulti]);
  const closeModalStationDetails = () => {
    dispatch(resetStatus());
    setIsShowModalStationDetails(false);
  };

  return (
    <Page className={classes.root} title="Station">
      <Container maxWidth={false}>
        <TableView
          actionDetailsStationRef={showDetailsStation}
          actionDeleteStationRef={handleDeleteStation}
          actionDeleteMultiStationRef={handleDeleteMultiStation}
          listStation={listStation}
          totalStation={totalStation}
          isLoading={statusGetAll === STATUS_API.PENDING}
          getListStationRef={getListStationWithParams}
          createRef={createNewStation}
          permission={permission}
        />
      </Container>
      {typeDelete === TYPE_DELETE.SINGLE ? (
        <DialogConfirm
          title={MESSAGE.CONFIRM_DELETE_STATION}
          textOk={MESSAGE.BTN_YES}
          textCancel={MESSAGE.BTN_CANCEL}
          callbackOk={() => confirmDeleteStation()}
          callbackCancel={() => dispatch(closeDialogConfirm())}
        />
      ) : (
        <DialogConfirm
          title={MESSAGE.CONFIRM_DELETE_STATION}
          textOk={MESSAGE.BTN_YES}
          textCancel={MESSAGE.BTN_CANCEL}
          callbackOk={() => confirmDeleteMultiStation()}
          callbackCancel={() => dispatch(closeDialogConfirm())}
        />
      )}

      {isShowModalStationDetails && sendData && (
        <DetailsStation
          open={isShowModalStationDetails}
          sendData={sendData}
          closeRef={closeModalStationDetails}
        />
      )}

      {(statusDeleteStation === STATUS_API.SUCCESS ||
        statusDeleteStation === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteStation === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_STATION_SUCCESS
                : MESSAGE.DELETE_STATION_FAIL
            }
            type={
              statusDeleteStation === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusDeleteMulti === STATUS_API.SUCCESS ||
        statusDeleteMulti === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_STATION_SUCCESS
                : MESSAGE.DELETE_STATION_FAIL
            }
            type={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </Page>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

export default StationListView;
