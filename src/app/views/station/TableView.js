import {
  Box,
  Button,
  Card,
  CardContent,
  Checkbox,
  Fab,
  Hidden,
  makeStyles,
  Table,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  useTheme,
  Zoom
} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { Trash2 } from 'react-feather';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useDispatch, useSelector } from 'react-redux';
import {
  ACTION_TABLE,
  PAGE_SIZE_LIST,
  STATUS_API
} from 'src/app/constant/config';
import { parseObjToSearchQuery } from 'src/app/utils/apiService';
import { sortStationByName } from 'src/features/stationSlice';
import HeaderTableCustom from '../../components/HeaderTableCustom';
import LoadingTableResult from '../../components/LoadingTableResult';
import TableResults from './TableResults';

const TableView = ({
  className,
  listStation,
  isLoading,
  getListStationRef,
  actionDetailsStationRef,
  totalStation,
  actionDeleteStationRef,
  actionDeleteMultiStationRef,
  handleShowDeleteIcon,
  createRef,
  permission,
  ...rest
}) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [limit, setLimit] = useState(PAGE_SIZE_LIST);
  const [page, setPage] = useState(0);
  const [params, setParams] = useState({
    page: page,
    size: limit,
    query: ''
  });
  const theme = useTheme();
  const transitionDuration = {
    enter: theme.transitions.duration.enteringScreen,
    exit: theme.transitions.duration.leavingScreen
  };
  const [queryValue, setQueryValue] = useState({});
  let isNullData = !listStation || listStation.length === 0;
  const statusDeleteMulti = useSelector(
    state => state.stationSlice.statusDeleteMulti
  );
  const handleLimitChange = event => {
    setLimit(event.target.value);
    if (!getListStationRef) return;
    const newparams = Object.assign({}, params, {
      size: event.target.value,
      page: 0
    });
    setParams(newparams);
    getListStationRef(newparams);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
    if (!getListStationRef) return;
    const newparams = Object.assign({}, params, { page: newPage });
    setParams(newparams);
    getListStationRef(newparams);
  };
  function handleSearchByName(data, propertyName) {
    let temp = { ...queryValue };
    temp[propertyName] = data;
    setQueryValue(temp);
    let newParams = Object.assign({}, params, {
      page: 0,
      query: parseObjToSearchQuery(temp)
    });
    setParams(newParams);
    return getListStationRef(newParams);
  }

  const handleSort = type => {
    return !isNullData && dispatch(sortStationByName(type));
  };
  const onEditStation = (type, station) => {
    if (!actionDetailsStationRef) return;
    const sendData = { type: type, data: station };
    actionDetailsStationRef(sendData);
  };
  const onEditStationViaId = (type, id) => {
    if (!actionDetailsStationRef) return;
    const sendData = {
      type: type,
      data: listStation.filter(item => item.id === id[0])[0]
    };
    actionDetailsStationRef(sendData);
  };
  const [listIdCheck, setListIdCheck] = useState([]);
  const [isCheckAll, setCheckAll] = useState(false);
  const [isShowDeleteIcon, setIsShowDeleteIcon] = useState(false);
  function handleDeleteMulti() {
    actionDeleteMultiStationRef(listIdCheck);
  }
  // if delete success remove listIdcheck and uncheckALl
  useEffect(() => {
    if (statusDeleteMulti === STATUS_API.SUCCESS) {
      setCheckAll(false);
      setListIdCheck([]);
    }
  }, [statusDeleteMulti]);
  useEffect(() => {
    if (listIdCheck.length >= 2) {
      setIsShowDeleteIcon(true);
    } else {
      setCheckAll(false);
      setIsShowDeleteIcon(false);
    }
  }, [listIdCheck]);

  return (
    <div>
      <Box>
        <Card>
          <CardContent>
            <Box className={classes.toolbar}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-around',
                  width: '30%'
                }}
              >
                {permission.view && (
                  <Zoom
                    key={'preview'}
                    in={listIdCheck.length === 1}
                    timeout={transitionDuration}
                    style={{
                      transitionDelay: `${
                        isShowDeleteIcon ? transitionDuration.exit : 0
                      }ms`
                    }}
                    unmountOnExit
                  >
                    <Box
                      onClick={() =>
                        onEditStationViaId(ACTION_TABLE.PREVIEW, listIdCheck)
                      }
                    >
                      <Fab
                        aria-label={'delete'}
                        size={'small'}
                        style={{ color: 'white', backgroundColor: '#3f51b5' }}
                      >
                        <VisibilityIcon />
                      </Fab>
                      <Hidden mdDown>
                        <Typography variant={'caption'} color={'primary'}>
                          {' '}
                          Chi tiết
                        </Typography>
                      </Hidden>
                    </Box>
                  </Zoom>
                )}
                {permission.update && (
                  <Zoom
                    key={'edit'}
                    in={listIdCheck.length === 1}
                    timeout={transitionDuration}
                    style={{
                      transitionDelay: `${
                        isShowDeleteIcon ? transitionDuration.exit : 0
                      }ms`
                    }}
                    unmountOnExit
                  >
                    <Box
                      onClick={() =>
                        onEditStationViaId(ACTION_TABLE.EDIT, listIdCheck)
                      }
                    >
                      <Fab
                        aria-label={'delete'}
                        size={'small'}
                        style={{ color: 'white', backgroundColor: 'orange' }}
                      >
                        <EditIcon />
                      </Fab>
                      <Hidden mdDown>
                        <Typography variant={'caption'} color={'primary'}>
                          {' '}
                          Cập nhật
                        </Typography>
                      </Hidden>
                    </Box>
                  </Zoom>
                )}
                {permission.delete && (
                  <Zoom
                    key={'delete'}
                    in={listIdCheck.length !== 0}
                    timeout={transitionDuration}
                    style={{
                      transitionDelay: `${
                        isShowDeleteIcon ? transitionDuration.exit : 0
                      }ms`
                    }}
                    unmountOnExit
                  >
                    <Box onClick={handleDeleteMulti}>
                      <Fab
                        aria-label={'delete'}
                        size={'small'}
                        style={{ color: 'white', backgroundColor: '#f80759' }}
                      >
                        <Trash2 />
                      </Fab>
                      <Hidden mdDown>
                        <Typography variant={'caption'} color={'primary'}>
                          {' '}
                          Xóa
                        </Typography>
                      </Hidden>
                    </Box>
                  </Zoom>
                )}
              </div>
              <Fab
                variant={'extended'}
                color="secondary"
                aria-label="create"
                size={'small'}
                disabled={!permission.create}
                onClick={() => createRef()}
              >
                <EditIcon
                  style={{ color: 'white', marginRight: theme.spacing(1) }}
                />
                <Typography variant={'button'} style={{ color: 'white' }}>
                  {' '}
                  Tạo mới
                </Typography>
              </Fab>
            </Box>
          </CardContent>
        </Card>
      </Box>

      <Box mt={3}>
        <Card className={clsx(classes.root, className)} {...rest}>
          <PerfectScrollbar>
            <Box minWidth={1050}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>
                      <Checkbox
                        checked={isCheckAll}
                        onChange={() => {
                          if (isNullData) return;
                          setCheckAll(!isCheckAll);
                          if (!isCheckAll)
                            setListIdCheck(listStation.map(item => item.id));
                          else setListIdCheck([]);
                        }}
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                      />
                    </TableCell>
                    <HeaderTableCustom
                      title={'Công đoạn sản xuất'}
                      searchName={'stationName'}
                      callbackSearch={data =>
                        handleSearchByName(data.trim(), 'name')
                      } // data is Object
                      callbackSort={handleSort}
                      isEnableSearch={true}
                      isEnableSort={true}
                    />
                    <HeaderTableCustom
                      title={'Mô tả'}
                      searchName={''}
                      isEnableSearch={false}
                      isEnableSort={false}
                    />
                    <HeaderTableCustom
                      title={'Ngày cập nhật'}
                      searchName={''}
                      isEnableSearch={true}
                      isEnableSort={true}
                    />
                    <HeaderTableCustom
                      title={'Tạo bởi'}
                      searchName={'createdBy'}
                      callbackSearch={data =>
                        handleSearchByName(data.trim(), 'createdBy')
                      } // data is Object
                      callbackSort={handleSort}
                      isEnableSearch={true}
                      isEnableSort={false}
                    />
                    <TableCell>Thao tác</TableCell>
                  </TableRow>
                </TableHead>
                {isLoading ? (
                  <LoadingTableResult numberOfTableColumn={7} />
                ) : (
                  <TableResults
                    permission={permission}
                    setCheckAll={value => setCheckAll(value)}
                    listStation={listStation}
                    setListIdCheck={value => setListIdCheck(value)}
                    listIdCheck={listIdCheck}
                    onEditStation={onEditStation}
                    actionDeleteStationRef={actionDeleteStationRef}
                    isNullData={isNullData}
                  />
                )}
              </Table>
            </Box>
          </PerfectScrollbar>
          <TablePagination
            component="div"
            count={totalStation}
            onChangePage={handlePageChange}
            onChangeRowsPerPage={handleLimitChange}
            page={page}
            rowsPerPage={limit}
            rowsPerPageOptions={isNullData ? [] : [5, 10, 25]}
          />
        </Card>
      </Box>
    </div>
  );
};

TableView.propTypes = {
  className: PropTypes.string,
  listStation: PropTypes.array.isRequired
};

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative'
  },

  minWithColumn: {
    minWidth: '150px'
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    minWidth: '50%'
  }
}));

export default TableView;
