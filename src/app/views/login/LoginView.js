import {
  Box,
  Button,
  CircularProgress,
  Container,
  Link,
  makeStyles,
  TextField,
  Typography
} from '@material-ui/core';
import { Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import CustomErrorMsg from 'src/app/components/CustomErrorMsg';
import Page from 'src/app/components/Page';
import { STATUS_API } from 'src/app/constant/config';
import * as Yup from 'yup';
import { getToken } from 'src/features/authSlice';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const LoginView = () => {
  const classes = useStyles();
  const navigate = useNavigate();

  const loginStatus = useSelector(state => state.authSlice.statusLogin);
  const err = useSelector(state => state.authSlice.err);
  const [isSubmitted, setSubmit] = useState(false);
  const dispatch = useDispatch();
  useEffect(() => {
    if (loginStatus === STATUS_API.SUCCESS) {
      navigate('/vnpt/dashboard', { replace: true });
    }
  }, [navigate, loginStatus]);
  return (
    <Page className={classes.root} title="Đăng nhập">
      <Box
        display="flex"
        flexDirection="column"
        height="100%"
        justifyContent="center"
      >
        <Container maxWidth="sm">
          <Formik
            initialValues={{
              email: '',
              password: ''
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string()
                .email('Email chưa đúng định dạng ')
                .max(255)
                .required('Bạn cần nhập email'),
              password: Yup.string()
                .max(255)
                .required('Bạn cần nhập mật khẩu')
            })}
            onSubmit={values => {
              setSubmit(true);
              let tmp = { ...values, rememberMe: true };
              dispatch(getToken(tmp));
            }}
          >
            {({
              errors,
              handleBlur,
              handleChange,
              handleSubmit,
              isSubmitting,
              touched,
              values
            }) => (
              <form onSubmit={handleSubmit}>
                <Box mb={3}>
                  <Typography color="textPrimary" variant="h1">
                    Trang đăng nhập
                  </Typography>
                  <br />
                  <Typography
                    color="textSecondary"
                    gutterBottom
                    variant="body2"
                  >
                    Xin chào đến với Trang quản lý sản xuất
                  </Typography>
                </Box>
                <TextField
                  error={Boolean(touched.email && errors.email)}
                  fullWidth
                  helperText={touched.email && errors.email}
                  label="Địa chỉ email"
                  margin="normal"
                  name="email"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="text"
                  value={values.email}
                  variant="outlined"
                />
                <TextField
                  error={Boolean(touched.password && errors.password)}
                  fullWidth
                  helperText={touched.password && errors.password}
                  label="Mật khẩu"
                  margin="normal"
                  name="password"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="password"
                  value={values.password}
                  variant="outlined"
                />
                {isSubmitted && <CustomErrorMsg content={err} />}
                <Box my={2}>
                  <Button
                    color="primary"
                    disabled={loginStatus === STATUS_API.PENDING}
                    fullWidth
                    size="large"
                    type="submit"
                    variant="contained"
                  >
                    Đăng nhập
                    {loginStatus === STATUS_API.PENDING && (
                      <div style={{ marginLeft: 20 }}>
                        <CircularProgress color={'secondary'} size={20} />
                      </div>
                    )}
                  </Button>
                </Box>
                {/*TODO : enable later if allow user register*/}
                {/*<Typography color="textSecondary" variant="body1">*/}
                {/*  Chưa có tài khoản{' '}*/}
                {/*  <Link component={RouterLink} to={'/register'} variant="h6">*/}
                {/*    Đăng ký !*/}
                {/*  </Link>*/}
                {/*</Typography>*/}
              </form>
            )}
          </Formik>
        </Container>
      </Box>
    </Page>
  );
};

export default LoginView;
