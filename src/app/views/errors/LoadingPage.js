import {
  Backdrop,
  Box,
  CircularProgress,
  Container,
  makeStyles,
  Typography
} from '@material-ui/core';
import Cookie from 'js-cookie';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Page from 'src/app/components/Page';
import { STATUS_API } from 'src/app/constant/config';
import { GetAccInfo } from 'src/features/userSlice';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff'
  }
}));

const LoadingPageView = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const statusGetCurrent = useSelector(
    state => state.userSlice.statusGetCurrent
  );
  useEffect(() => {
    if (!Cookie.get('access-token')) navigate('/login');
    else {
      dispatch(GetAccInfo());
    }
  }, [dispatch, navigate]);

  useEffect(() => {
    if (statusGetCurrent === STATUS_API.ERROR) navigate('/login');
  }, [statusGetCurrent]);
  return (
    <Page className={classes.root} title="Vui lòng chờ">
      <Box
        display="flex"
        flexDirection="column"
        height="100%"
        justifyContent="center"
      >
        <Backdrop
          className={classes.backdrop}
          open={true}
          onClick={() => {
            return false;
          }}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Box>
    </Page>
  );
};

export default LoadingPageView;
