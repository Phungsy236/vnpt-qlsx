import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import moment from 'moment';
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Chip,
  Divider,
  Grid,
  makeStyles,
  TextField,
  Typography
} from '@material-ui/core';
import { useSelector } from 'react-redux';

const useStyles = makeStyles(() => ({
  root: {},
  avatar: {
    height: 100,
    width: 100
  },
  wrap: {
    padding: 5
  }
}));

const BaseInfo = ({ className, ...rest }) => {
  const classes = useStyles();
  const data = useSelector(state => state.userSlice.accInfo);
  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <CardContent>
        <Box className={classes.wrap}>
          <Box display={'flex'} p={1} alignItems={'center'}>
            <Typography variant={'subtitle2'}>Người tạo : </Typography>
            <Divider orientation={'vertical'} />
            <Typography variant={'caption'}>{data.created_by} </Typography>
          </Box>
          <Box display={'flex'} p={1} alignItems={'center'}>
            <Typography variant={'subtitle2'}>Chức vụ : </Typography>
            <Divider orientation={'vertical'} />
            {data.roles?.map(item => (
              <Chip
                color="primary"
                title={item.description}
                label={item.name}
                size={'small'}
                style={{ margin: 1 }}
              />
            )) || (
              <Typography variant={'caption'} color={'secondary'}>
                Chưa phân quyền
              </Typography>
            )}
          </Box>
          {/*<Box display={'flex'} alignItems={'center'} p={1}>*/}
          {/*  <Typography variant={'subtitle2'}>Ngày tham gia : </Typography>*/}

          {/*  <Typography color="textSecondary" variant="body1">*/}
          {/*    {moment(data?.created * 1000).format('LLL')}*/}
          {/*  </Typography>*/}
          {/*</Box>*/}
          <Box display={'flex'} alignItems={'center'} p={1}>
            <Typography variant={'subtitle2'}>Lần sửa gần nhất : </Typography>

            <Typography color="textSecondary" variant="body1">
              {moment(data?.updated).format('LLL')}
            </Typography>
          </Box>
        </Box>
      </CardContent>
      <Divider />
    </Card>
  );
};

BaseInfo.propTypes = {
  className: PropTypes.string
};

export default BaseInfo;
