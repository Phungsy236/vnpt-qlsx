import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Divider,
  Grid,
  IconButton,
  TextField,
  Typography
} from '@material-ui/core';
import { BorderColor } from '@material-ui/icons';
import { Formik } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import ToastMessage from 'src/app/components/ToastMessage';
import { GetAccInfo, resetStatus, updateSelf } from 'src/features/userSlice';
import * as Yup from 'yup';
import {
  MESSAGE_TYPE,
  STATUS_API,
  TYPE_UPDATE_USER,
  userShape_const
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { showToast } from 'src/features/uiSlice';

const ProfileDetails = ({ className, ...rest }) => {
  const obj_shape = userShape_const;
  const data = useSelector(state => state.userSlice.accInfo);
  const [isDisable_ls, setDisable] = useState(true);
  const [editObj_ls, setEditObj] = useState();

  const dispatch = useDispatch();

  const statusUpdateSelf = useSelector(
    state => state.userSlice.statusUpdateSelf
  );
  // const err = useSelector(state => state.userSlice.err);
  const handleCancel = () => {
    setDisable(true);
    setEditObj(null);
  };

  useEffect(() => {
    setEditObj(data);
  }, [data, isDisable_ls]);

  useEffect(() => {
    if (data) return;
    dispatch(GetAccInfo());
  }, [dispatch]);
  const [isSubmitted, setSubmitted] = React.useState(false);
  useEffect(() => {
    if (
      statusUpdateSelf === STATUS_API.SUCCESS ||
      statusUpdateSelf === STATUS_API.ERROR
    ) {
      dispatch(showToast());
    }
  }, [statusUpdateSelf]);

  return (
    <>
      {(statusUpdateSelf === STATUS_API.SUCCESS ||
        statusUpdateSelf === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusUpdateSelf === STATUS_API.SUCCESS
                ? MESSAGE.UPDATE_USER_SUCCESS
                : MESSAGE.UPDATE_USER_FAIL
            }
            type={
              statusUpdateSelf === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {data && (
        <Formik
          enableReinitialize
          initialValues={{ ...editObj_ls }}
          validationSchema={Yup.object().shape(obj_shape)}
          onSubmit={values => {
            setSubmitted(true);
            dispatch(
              updateSelf({
                ...values
              })
            );
          }}
        >
          {({
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            touched,
            values
          }) => (
            <form onSubmit={handleSubmit}>
              <Card>
                <CardHeader
                  title="Thông tin cá nhân"
                  action={
                    <IconButton
                      aria-label="edit"
                      onClick={() => setDisable(false)}
                    >
                      <BorderColor />
                    </IconButton>
                  }
                />
                <Divider />
                <CardContent>
                  <Grid container spacing={3}>
                    <Grid item md={6} xs={12}>
                      <Typography variant={'subtitle2'}>Họ và tên</Typography>
                      <TextField
                        fullWidth
                        error={Boolean(touched.fullName && errors.full_name)}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        helperText={touched.fullName && errors.fullName}
                        name="full_name"
                        required
                        disabled={isDisable_ls}
                        value={values.fullName}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <Typography variant={'subtitle2'}>
                        Số điện thoại
                      </Typography>
                      <TextField
                        fullWidth
                        error={Boolean(touched.phone && errors.phone)}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        helperText={touched.phone && errors.phone}
                        name="phone"
                        disabled={isDisable_ls}
                        value={values.phone}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item md={6} xs={12} style={{ pointerEvents: 'none' }}>
                      <Typography variant={'subtitle2'}>
                        Địa chi Email
                      </Typography>

                      <TextField
                        fullWidth
                        error={Boolean(touched.email && errors.email)}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        helperText={touched.email && errors.email}
                        name="email"
                        required
                        disabled
                        value={values.email}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <Typography variant={'subtitle2'}>Địa chỉ</Typography>

                      <TextField
                        fullWidth
                        error={Boolean(touched.address && errors.address)}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        helperText={touched.address && errors.address}
                        name="address"
                        required
                        disabled={isDisable_ls}
                        value={values.address}
                        variant="outlined"
                      />
                    </Grid>
                  </Grid>
                </CardContent>
                <Divider />
                <Box display="flex" justifyContent="flex-end" p={2}>
                  {!isDisable_ls && (
                    <Grid container spacing={2} justify={'flex-end'}>
                      <Grid item>
                        <Button
                          variant="contained"
                          type="submit"
                          color={'primary'}
                        >
                          Cập nhật
                          {statusUpdateSelf === STATUS_API.PENDING && (
                            <div style={{ marginLeft: 20 }}>
                              <CircularProgress
                                style={{ color: 'gray' }}
                                size={20}
                              />
                            </div>
                          )}
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button variant="contained" onClick={handleCancel}>
                          Thoát
                        </Button>
                      </Grid>
                    </Grid>
                  )}
                </Box>
              </Card>
            </form>
          )}
        </Formik>
      )}
      {/*{errUpdateBySelf && <CustomErrorMessage content={errUpdateBySelf} />}*/}
    </>
  );
};
ProfileDetails.propTypes = {
  className: PropTypes.string
};

export default ProfileDetails;
