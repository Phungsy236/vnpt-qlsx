import {
  AppBar,
  Box,
  Card,
  CardContent,
  Container,
  Dialog,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  makeStyles,
  Select,
  Slide,
  TextField
} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { green } from '@material-ui/core/colors';
import Typography from '@material-ui/core/Typography';
import { Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AutoCompleteMulti from 'src/app/components/AutoCompleteMulti';
import LoadingComponent from 'src/app/components/Loading';
import NullData from 'src/app/components/NullData';
import UploadFile from 'src/app/components/UploadFile';
import * as Yup from 'yup';
import {
  createUser,
  getDetailUser,
  resetStatus,
  updateUser
} from 'src/features/userSlice';
import { showToast } from 'src/features/uiSlice';
import CustomErrorMessage from 'src/app/components/CustomErrorMsg';
import LoadingInButton from 'src/app/components/LoadingInButton';
import ToastMessage from 'src/app/components/ToastMessage';
import {
  ACTION_TABLE,
  MESSAGE_TYPE,
  STATUS_API
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { User } from 'src/app/models/User';
import ToolBarEdit from './ToolBar.js';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function DetailsUser({ open, sendData, closeRef, allRole }) {
  const classes = useStyles();
  const dispatch = useDispatch();

  const typeAction = sendData.type;
  const err = useSelector(state => state.userSlice.err);

  const handleClose = isSaved => {
    if (!closeRef) return;
    closeRef();
  };
  const statusCreateUser = useSelector(
    state => state.userSlice.statusCreateUser
  );
  const statusUpdateUser = useSelector(
    state => state.userSlice.statusUpdateUser
  );
  // const statusGetDetail = useSelector(state => state.userSlice.statusGetDetail);
  const userDetail = useSelector(state => state.userSlice.userDetail);
  const willLoading =
    statusCreateUser === STATUS_API.SUCCESS ||
    statusCreateUser === STATUS_API.ERROR ||
    statusUpdateUser === STATUS_API.SUCCESS ||
    statusUpdateUser === STATUS_API.ERROR;
  useEffect(() => {
    if (willLoading) dispatch(showToast());
  }, [statusCreateUser, statusUpdateUser]);
  useEffect(() => {
    if (typeAction !== ACTION_TABLE.CREATE)
      dispatch(getDetailUser(sendData.data.id));
  }, [dispatch]);

  const [isSubmitted, setSubmitted] = useState(false);
  function handleSubmit(value) {
    console.log(123);
    setSubmitted(true);
    let objSubmitted;
    if (typeAction === ACTION_TABLE.CREATE) {
      objSubmitted = {
        fullName: value.fullName,
        email: value.email,
        address: value.address,
        phone: value.phone,
        roles: value.roles,
        password: value.password
      };
      dispatch(createUser(objSubmitted));
    } else {
      objSubmitted = {
        ...sendData.data,
        fullName: value.fullName,
        email: value.email,
        address: value.address,
        phone: value.phone,
        roles: value.roles,
        password: value.password
      };
      dispatch(updateUser(objSubmitted));
    }
  }
  const initUserModel =
    typeAction === ACTION_TABLE.CREATE ? new User() : userDetail;
  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={() => handleClose()}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <ToolBarEdit closeToolbarRef={handleClose} />
        </AppBar>
        {initUserModel && (
          <Container maxWidth="lg">
            <Grid container spacing={3}>
              <Grid
                item
                lg={12}
                md={12}
                xs={12}
                style={{
                  paddingTop: '50px',
                  pointerEvents:
                    typeAction === ACTION_TABLE.PREVIEW ? 'none' : ''
                }}
              >
                <Card className={classes.shadowBox}>
                  <CardContent>
                    <Formik
                      initialValues={initUserModel}
                      validationSchema={Yup.object().shape({
                        fullName: Yup.string()
                          .max(255)
                          .required('Tên không được để trống'),
                        email: Yup.string()
                          .email('Email chưa đúng định dạng')
                          .required('email không được để trống'),
                        phone: Yup.string().matches(
                          new RegExp('^[0-9-+]{9,11}$'),
                          'Số điện thoại phải đúng định dạng'
                        ),
                        password:
                          typeAction === ACTION_TABLE.CREATE
                            ? Yup.string().required(
                                'Password không được để trống'
                              )
                            : null
                      })}
                      onSubmit={handleSubmit}
                    >
                      {({
                        errors,
                        handleBlur,
                        handleChange,
                        handleSubmit,
                        setFieldValue,
                        touched,
                        values
                      }) => (
                        <form onSubmit={handleSubmit}>
                          <Grid container spacing={2}>
                            <Grid
                              item
                              lg={5}
                              md={5}
                              xs={12}
                              style={{
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center'
                              }}
                            >
                              <InputLabel htmlFor="name">
                                Ảnh đại diện :
                              </InputLabel>
                              <br />
                              <UploadFile />
                            </Grid>

                            <Grid
                              item
                              md={7}
                              lg={7}
                              xs={12}
                              style={{
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center'
                              }}
                            >
                              <Grid
                                container
                                spacing={1}
                                style={{ marginTop: 10 }}
                              >
                                <Grid item lg={6} md={6} xs={12}>
                                  <InputLabel htmlFor="name">
                                    Tên Người dùng *:
                                  </InputLabel>
                                  <TextField
                                    error={Boolean(
                                      touched.fullName && errors.fullName
                                    )}
                                    fullWidth
                                    helperText={
                                      touched.fullName && errors.fullName
                                    }
                                    margin="normal"
                                    name="fullName"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values?.fullName}
                                    variant="outlined"
                                  />
                                </Grid>
                                <Grid item lg={6} md={6} xs={12}>
                                  <InputLabel htmlFor="name">
                                    Địa chỉ :
                                  </InputLabel>
                                  <TextField
                                    error={Boolean(
                                      touched.address && errors.address
                                    )}
                                    fullWidth
                                    helperText={
                                      touched.address && errors.address
                                    }
                                    margin="normal"
                                    name="address"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values?.address}
                                    variant="outlined"
                                  />
                                </Grid>
                              </Grid>
                              <Grid container spacing={1}>
                                <Grid item lg={6} md={6} xs={12}>
                                  <InputLabel htmlFor="name">
                                    Email *:
                                  </InputLabel>
                                  <TextField
                                    error={Boolean(
                                      touched.email && errors.email
                                    )}
                                    fullWidth
                                    helperText={touched.email && errors.email}
                                    margin="normal"
                                    name="email"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values?.email}
                                    variant="outlined"
                                  />
                                </Grid>
                                <Grid item lg={6} md={6} xs={12}>
                                  <InputLabel htmlFor="name">
                                    Số điện thoại :
                                  </InputLabel>
                                  <TextField
                                    error={Boolean(
                                      touched.phone && errors.phone
                                    )}
                                    fullWidth
                                    helperText={touched.phone && errors.phone}
                                    margin="normal"
                                    name="phone"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values?.phone}
                                    variant="outlined"
                                  />
                                </Grid>
                                {typeAction === ACTION_TABLE.CREATE && (
                                  <Grid item lg={6} md={6} xs={12}>
                                    <InputLabel htmlFor="name">
                                      Mật khẩu :
                                    </InputLabel>
                                    <TextField
                                      error={Boolean(
                                        touched.password && errors.password
                                      )}
                                      fullWidth
                                      helperText={
                                        touched.password && errors.password
                                      }
                                      margin="normal"
                                      name="password"
                                      onBlur={handleBlur}
                                      onChange={handleChange}
                                      value={values?.password}
                                      variant="outlined"
                                    />
                                  </Grid>
                                )}

                                <Grid
                                  item
                                  lg={
                                    typeAction === ACTION_TABLE.CREATE ? 6 : 12
                                  }
                                  md={
                                    typeAction === ACTION_TABLE.CREATE ? 6 : 12
                                  }
                                  xs={12}
                                >
                                  <InputLabel htmlFor="name">
                                    Chức vụ :
                                  </InputLabel>
                                  <FormControl
                                    variant="outlined"
                                    className={classes.formControl}
                                  >
                                    {allRole && (
                                      <AutoCompleteMulti
                                        isMulti={true}
                                        displayAttributeName={'name'}
                                        options={allRole}
                                        // isError={Boolean(
                                        //   touched.stationId && errors.stationId
                                        // )}
                                        // helperText={
                                        //   touched.stationId && errors.stationId
                                        // }
                                        placeHoldInput={'Chức vụ của tài khoản'}
                                        defaultValue={values?.roles || []}
                                        inputName={'roles'}
                                        handleSendValue={value => {
                                          setFieldValue('roles', value);
                                          // setStation_id_choosed(value);
                                        }}
                                      />
                                    )}
                                  </FormControl>
                                </Grid>
                              </Grid>
                            </Grid>

                            {/*=======================if preview render full information==================*/}
                            {/*{typeAction === ACTION_TABLE.PREVIEW && (*/}
                            {/*  <>*/}
                            {/*    <Grid item md={6} xs={12}>*/}
                            {/*      <InputLabel htmlFor="created_at">*/}
                            {/*        Tạo lúc :*/}
                            {/*      </InputLabel>*/}
                            {/*      <TextField*/}
                            {/*        fullWidth*/}
                            {/*        margin="normal"*/}
                            {/*        name="created"*/}
                            {/*        value={values.created}*/}
                            {/*        variant="outlined"*/}
                            {/*      />*/}
                            {/*    </Grid>*/}
                            {/*    {values.updated && (*/}
                            {/*      <Grid item md={6} xs={12}>*/}
                            {/*        <InputLabel htmlFor="updated">*/}
                            {/*          Cập nhật gần nhất :*/}
                            {/*        </InputLabel>*/}
                            {/*        <TextField*/}
                            {/*          fullWidth*/}
                            {/*          margin="normal"*/}
                            {/*          name="updated"*/}
                            {/*          value={values.updated}*/}
                            {/*          variant="outlined"*/}
                            {/*        />*/}
                            {/*      </Grid>*/}
                            {/*    )}*/}

                            {/*    <Grid item md={6} xs={12}>*/}
                            {/*      <InputLabel htmlFor="created_by">*/}
                            {/*        Tạo bởi :*/}
                            {/*      </InputLabel>*/}
                            {/*      <TextField*/}
                            {/*        fullWidth*/}
                            {/*        margin="normal"*/}
                            {/*        name="created_by"*/}
                            {/*        value={values.createdBy}*/}
                            {/*        variant="outlined"*/}
                            {/*      />*/}
                            {/*    </Grid>*/}
                            {/*    {values.updatedBy && (*/}
                            {/*      <Grid item md={6} xs={12}>*/}
                            {/*        <InputLabel htmlFor="updated_by">*/}
                            {/*          Sửa đổi gần nhất bởi :*/}
                            {/*        </InputLabel>*/}
                            {/*        <TextField*/}
                            {/*          fullWidth*/}
                            {/*          margin="normal"*/}
                            {/*          name="updated_by"*/}
                            {/*          value={values.updatedBy}*/}
                            {/*          variant="outlined"*/}
                            {/*        />*/}
                            {/*      </Grid>*/}
                            {/*    )}*/}
                            {/*  </>*/}
                            {/*)}*/}
                          </Grid>
                          <CustomErrorMessage content={err} />
                          <Box my={2} mt={5}>
                            <div className={classes.groupButtonSubmit}>
                              {Boolean(typeAction !== ACTION_TABLE.PREVIEW) && (
                                <div className="left-button">
                                  <div className={classes.wrapper}>
                                    <Button
                                      style={{ marginRight: '10px' }}
                                      color="primary"
                                      size="large"
                                      type="submit"
                                      variant="contained"
                                    >
                                      {typeAction === ACTION_TABLE.CREATE
                                        ? 'Tạo mới'
                                        : 'Cập nhật'}
                                    </Button>
                                    {(statusUpdateUser === STATUS_API.PENDING ||
                                      statusCreateUser ===
                                        STATUS_API.PENDING) && (
                                      <LoadingInButton />
                                    )}
                                  </div>
                                  <Button
                                    size="large"
                                    variant="contained"
                                    onClick={handleClose}
                                  >
                                    Thoát
                                  </Button>
                                </div>
                              )}
                            </div>
                          </Box>
                        </form>
                      )}
                    </Formik>
                  </CardContent>
                  <Divider />
                </Card>
              </Grid>
            </Grid>
          </Container>
        )}
      </Dialog>
      {(statusCreateUser === STATUS_API.SUCCESS ||
        statusCreateUser === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusCreateUser === STATUS_API.SUCCESS
                ? MESSAGE.CREATE_USER_SUCCESS
                : MESSAGE.CREATE_USER_FAIL
            }
            type={
              statusCreateUser === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusUpdateUser === STATUS_API.SUCCESS ||
        statusUpdateUser === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusUpdateUser === STATUS_API.SUCCESS
                ? MESSAGE.UPDATE_USER_SUCCESS
                : MESSAGE.UPDATE_USER_FAIL
            }
            type={
              statusUpdateUser === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </div>
  );
}

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative'
  },
  shadowBox: {
    boxShadow: '0 2px 5px rgba(0,0,0,.18)'
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  root: {
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  headerDivider: {
    backgroundColor: '#e8e7e7',
    padding: theme.spacing(1),
    marginBottom: '15px'
  },
  formControl: {
    marginTop: theme.spacing(2),
    width: '100%'
  },

  groupButtonSubmit: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '15px',

    '& .left-button': {
      display: 'flex'
    }
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  wrapper: {
    position: 'relative'
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  disableForm: {
    pointerEvents: 'none'
  },
  colorWhite: {
    color: '#fff'
  },
  tabHeader: {
    backgroundColor: 'aliceblue'
  }
}));

export default DetailsUser;
