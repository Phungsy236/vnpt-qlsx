import { Container, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DialogConfirm from 'src/app/components/DialogConfirm';
import Page from 'src/app/components/Page';
import ToastMessage from 'src/app/components/ToastMessage';
import {
  ACTION_TABLE,
  LIST_ROLE,
  MESSAGE_TYPE,
  PAGE_SIZE_LIST,
  STATUS_API,
  TYPE_DELETE
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { DefaultSearchParams } from 'src/app/models/DefaultSearchParams';
import { getAllRole } from 'src/features/roleSlice';
import {
  closeDialogConfirm,
  showDialogConfirm,
  showToast
} from 'src/features/uiSlice';
import {
  deleteUser,
  deleteMultiUser,
  getListUser,
  resetStatus
} from 'src/features/userSlice';
import DataTable from './TableView';
import DetailsUser from './user_detail/index';

const UserListView = ({ authorities }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  // get data api
  const listUser = useSelector(state => state.userSlice.listUser);
  const totalUser = useSelector(state => state.userSlice.totalUser);
  const statusGetAll = useSelector(state => state.userSlice.statusGetAll);
  const allRole = useSelector(state => state.roleSlice.allRole);
  const statusDeleteUser = useSelector(
    state => state.userSlice.statusDeleteUser
  );
  const statusDeleteMulti = useSelector(
    state => state.userSlice.statusDeleteMulti
  );

  const [isShowModalUserDetails, setIsShowModalUserDetails] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const [deleteItem, setDeleteItem] = useState(null);

  const [sendData, setSendData] = useState({
    data: undefined,
    type: null
  });

  const [params, setParams] = useState(new DefaultSearchParams());

  useEffect(() => {
    // TODO: ENABLE THIS
    // if (!Cookie.get('access-token')) return;
    dispatch(getListUser(params));
  }, [dispatch, params]);

  const getListUserWithParams = data => {
    const paramValue = Object.assign({}, params, data);
    setParams(paramValue);
  };

  const showDetailsUser = data => {
    if (!data) return;
    setSendData(data);
    setIsShowModalUserDetails(true);
  };
  const createNewUser = () => {
    setSendData({ data: {}, type: ACTION_TABLE.CREATE });
    setIsShowModalUserDetails(true);
  };

  const handleDeleteUser = data => {
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };

  const confirmDeleteUser = () => {
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deleteUser(deleteItem));
  };
  const [typeDelete, setTypeDelete] = useState(TYPE_DELETE.SINGLE);

  const handleDeleteMultiUser = data => {
    setTypeDelete(TYPE_DELETE.MULTI);
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };
  const confirmDeleteMultiUser = () => {
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deleteMultiUser(deleteItem));
  };

  const willLoading =
    statusDeleteUser === STATUS_API.SUCCESS ||
    statusDeleteUser === STATUS_API.ERROR ||
    statusDeleteMulti === STATUS_API.SUCCESS ||
    statusDeleteMulti === STATUS_API.ERROR;

  useEffect(() => {
    if (willLoading) dispatch(showToast());
  }, [statusDeleteUser, statusDeleteMulti]);
  const closeModalUserDetails = () => setIsShowModalUserDetails(false);
  useEffect(() => {
    if (!allRole) dispatch(getAllRole());
  }, [dispatch]);
  const permission = {
    create: authorities.includes(LIST_ROLE.User_Create),
    delete: authorities.includes(LIST_ROLE.User_Delete),
    update: authorities.includes(LIST_ROLE.User_Update)
  };
  return (
    <Page className={classes.root} title="User">
      <Container maxWidth={false}>
        <DataTable
          actionDetailsUserRef={showDetailsUser}
          actionDeleteUserRef={handleDeleteUser}
          actionDeleteMultiUserRef={handleDeleteMultiUser}
          listUser={listUser}
          totalUser={totalUser}
          isLoading={statusGetAll === STATUS_API.PENDING}
          getListUserRef={getListUserWithParams}
          createRef={createNewUser}
          permission={permission}
        />
      </Container>
      {typeDelete === TYPE_DELETE.SINGLE ? (
        <DialogConfirm
          title={MESSAGE.CONFIRM_DELETE_USER}
          textOk={MESSAGE.BTN_YES}
          textCancel={MESSAGE.BTN_CANCEL}
          callbackOk={() => confirmDeleteUser()}
          callbackCancel={() => dispatch(closeDialogConfirm())}
        />
      ) : (
        <DialogConfirm
          title={MESSAGE.CONFIRM_DELETE_USER}
          textOk={MESSAGE.BTN_YES}
          textCancel={MESSAGE.BTN_CANCEL}
          callbackOk={() => confirmDeleteMultiUser()}
          callbackCancel={() => dispatch(closeDialogConfirm())}
        />
      )}

      {isShowModalUserDetails && sendData && (
        <DetailsUser
          allRole={allRole}
          open={isShowModalUserDetails}
          sendData={sendData}
          closeRef={closeModalUserDetails}
        />
      )}

      {(statusDeleteUser === STATUS_API.SUCCESS ||
        statusDeleteUser === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteUser === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_USER_SUCCESS
                : MESSAGE.DELETE_USER_FAIL
            }
            type={
              statusDeleteUser === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusDeleteMulti === STATUS_API.SUCCESS ||
        statusDeleteMulti === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_STATION_SUCCESS
                : MESSAGE.DELETE_STATION_FAIL
            }
            type={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </Page>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

export default UserListView;
