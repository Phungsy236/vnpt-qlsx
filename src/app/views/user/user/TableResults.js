import {
  Checkbox,
  Chip,
  makeStyles,
  TableBody,
  TableCell,
  TableRow,
  Tooltip
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import PropTypes from 'prop-types';
import React from 'react';
import TableNullData from '../../../components/TableNullData';
import TextHightLight from '../../../components/TextHightLight';
import { ACTION_TABLE } from 'src/app/constant/config';

TableResults.propTypes = {
  listUser: PropTypes.array.isRequired,
  listIdCheck: PropTypes.array.isRequired,
  setCheckAll: PropTypes.func.isRequired,
  setListIdCheck: PropTypes.func.isRequired,
  onEditUser: PropTypes.func,
  actionDeleteUserRef: PropTypes.func,
  isNullData: PropTypes.bool
};

function TableResults({
  listUser,
  listIdCheck,
  setCheckAll,
  setListIdCheck,
  onEditUser,
  permission,
  actionDeleteUserRef,
  isNullData
}) {
  const classes = useStyles();
  if (isNullData) return <TableNullData numberOfTableColumn={7} />;
  return (
    <TableBody>
      {listUser?.map((user, index) => (
        <TableRow hover key={user.id}>
          <TableCell>
            <Checkbox
              checked={listIdCheck.includes(user.id)}
              onChange={() => {
                setCheckAll(true);
                if (listIdCheck.includes(user.id)) {
                  setListIdCheck(listIdCheck.filter(item => item !== user.id));
                } else {
                  setListIdCheck(listIdCheck.concat(user.id));
                }
              }}
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
          </TableCell>
          <TableCell onClick={() => onEditUser(ACTION_TABLE.PREVIEW, user)}>
            <TextHightLight
              type={'blue'}
              content={user.fullName}
              isCursor={true}
            />
          </TableCell>
          <TableCell>{user.address}</TableCell>
          <TableCell>{user.phone}</TableCell>
          <TableCell>{user.email}</TableCell>
          {/*<TableCell>*/}
          {/*  {user.roles?.map(item => (*/}
          {/*    <Chip*/}
          {/*      style={{ margin: 2 }}*/}
          {/*      label={item.name}*/}
          {/*      clickable*/}
          {/*      color="primary"*/}
          {/*      onDelete={() => false}*/}
          {/*      onClick={() => false}*/}
          {/*      deleteIcon={<Done />}*/}
          {/*    />*/}
          {/*  ))}*/}
          {/*</TableCell>*/}
          <TableCell>
            <div className={classes.groupAction}>
              <div
                className="action"
                onClick={() => onEditUser(ACTION_TABLE.PREVIEW, user)}
              >
                <Tooltip title="Chi tiết">
                  <VisibilityIcon />
                </Tooltip>
              </div>
              {permission.update && (
                <div
                  className="action"
                  onClick={() => onEditUser(ACTION_TABLE.EDIT, user)}
                >
                  <Tooltip title="Chỉnh sửa">
                    <EditIcon />
                  </Tooltip>
                </div>
              )}
              {permission.delete && (
                <div
                  className="action"
                  onClick={() => actionDeleteUserRef(user)}
                >
                  <Tooltip title="Xóa">
                    <DeleteIcon />
                  </Tooltip>
                </div>
              )}
            </div>
          </TableCell>
        </TableRow>
      ))}
    </TableBody>
  );
}

const useStyles = makeStyles(theme => ({
  groupAction: {
    display: 'flex',
    alignItems: 'center',
    '& .action': {
      cursor: 'pointer',
      padding: '0 5px',
      color: '#333535',
      '&:hover': {
        color: '#3f51b5'
      }
    }
  }
}));
export default TableResults;
