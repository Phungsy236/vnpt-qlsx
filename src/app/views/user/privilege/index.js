import { Container, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DialogConfirm from 'src/app/components/DialogConfirm';
import Page from 'src/app/components/Page';
import ToastMessage from 'src/app/components/ToastMessage';
import {
  ACTION_TABLE,
  HEADER_AUTH_KEY_NAME,
  MESSAGE_TYPE,
  PAGE_SIZE_LIST,
  STATUS_API,
  TYPE_DELETE
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { DefaultSearchParams } from 'src/app/models/DefaultSearchParams';
import {
  deleteMultiPrivilege,
  deletePrivilege,
  getListPrivilege,
  resetStatus
} from 'src/features/privilegeSlice';
import {
  closeDialogConfirm,
  showDialogConfirm,
  showToast
} from 'src/features/uiSlice';
import DetailsPrivilege from './privilege_detail/index';
import TableView from './TableView';
import Cookie from 'js-cookie';

const PrivilegeListView = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  // get data api
  const listPrivilege = useSelector(
    state => state.privilegeSlice.listPrivilege
  );
  const totalPrivilege = useSelector(
    state => state.privilegeSlice.totalPrivilege
  );
  const statusGetAll = useSelector(state => state.privilegeSlice.statusGetAll);
  const statusDeleteMulti = useSelector(
    state => state.privilegeSlice.statusDeleteMulti
  );
  const statusDeletePrivilege = useSelector(
    state => state.privilegeSlice.statusDeletePrivilege
  );

  const [
    isShowModalPrivilegeDetails,
    setIsShowModalPrivilegeDetails
  ] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const [deleteItem, setDeleteItem] = useState(null);

  const [sendData, setSendData] = useState({
    data: undefined,
    type: null
  });

  const [params, setParams] = useState(new DefaultSearchParams());

  useEffect(() => {
    if (!Cookie.get(HEADER_AUTH_KEY_NAME)) return;
    dispatch(getListPrivilege(params));
  }, [dispatch, params]);

  const getListPrivilegeWithParams = data => {
    const paramValue = Object.assign({}, params, data);
    setParams(paramValue);
  };

  // const clearSearch = () => {
  //   const paramValue = {
  //     _page: 1,
  //     _limit: PAGE_SIZE_LIST
  //   };
  //   setParams(paramValue);
  //   dispatch(getListPrivilege(paramValue));
  // };

  const showDetailsPrivilege = data => {
    if (!data) return;
    setSendData(data);
    setIsShowModalPrivilegeDetails(true);
  };
  const createNewPrivilege = () => {
    setSendData({ data: {}, type: ACTION_TABLE.CREATE });
    setIsShowModalPrivilegeDetails(true);
  };

  const [typeDelete, setTypeDelete] = useState(TYPE_DELETE.SINGLE);
  const handleDeleteMultiPrivilege = data => {
    setTypeDelete(TYPE_DELETE.MULTI);
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };

  const confirmDeleteMultiPrivilege = () => {
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deleteMultiPrivilege(deleteItem));
  };

  const handleDeletePrivilege = data => {
    setTypeDelete(TYPE_DELETE.SINGLE);
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };

  const confirmDeletePrivilege = () => {
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deletePrivilege(deleteItem));
  };

  const willShowToast =
    statusDeletePrivilege === STATUS_API.SUCCESS ||
    statusDeletePrivilege === STATUS_API.ERROR ||
    statusDeleteMulti === STATUS_API.SUCCESS ||
    statusDeleteMulti === STATUS_API.ERROR;
  useEffect(() => {
    if (willShowToast) dispatch(showToast());
  }, [statusDeletePrivilege, statusDeleteMulti]);
  const closeModalPrivilegeDetails = () => {
    setIsShowModalPrivilegeDetails(false);

    dispatch(resetStatus());
  };

  return (
    <Page className={classes.root} title="Privilege">
      <Container maxWidth={false}>
        <TableView
          actionDetailsPrivilegeRef={showDetailsPrivilege}
          actionDeletePrivilegeRef={handleDeletePrivilege}
          actionDeleteMultiPrivilegeRef={handleDeleteMultiPrivilege}
          listPrivilege={listPrivilege}
          totalPrivilege={totalPrivilege}
          isLoading={statusGetAll === STATUS_API.PENDING}
          getListPrivilegeRef={getListPrivilegeWithParams}
          createRef={createNewPrivilege}
        />
      </Container>
      {typeDelete === TYPE_DELETE.SINGLE ? (
        <DialogConfirm
          title={MESSAGE.CONFIRM_DELETE_STATION}
          textOk={MESSAGE.BTN_YES}
          textCancel={MESSAGE.BTN_CANCEL}
          callbackOk={() => confirmDeletePrivilege()}
          callbackCancel={() => dispatch(closeDialogConfirm())}
        />
      ) : (
        <DialogConfirm
          title={MESSAGE.CONFIRM_DELETE_STATION}
          textOk={MESSAGE.BTN_YES}
          textCancel={MESSAGE.BTN_CANCEL}
          callbackOk={() => confirmDeleteMultiPrivilege()}
          callbackCancel={() => dispatch(closeDialogConfirm())}
        />
      )}

      {isShowModalPrivilegeDetails && sendData && (
        <DetailsPrivilege
          open={isShowModalPrivilegeDetails}
          sendData={sendData}
          closeRef={closeModalPrivilegeDetails}
        />
      )}

      {(statusDeletePrivilege === STATUS_API.SUCCESS ||
        statusDeletePrivilege === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeletePrivilege === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_STATION_SUCCESS
                : MESSAGE.DELETE_STATION_FAIL
            }
            type={
              statusDeletePrivilege === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusDeleteMulti === STATUS_API.SUCCESS ||
        statusDeleteMulti === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_STATION_SUCCESS
                : MESSAGE.DELETE_STATION_FAIL
            }
            type={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </Page>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

export default PrivilegeListView;
