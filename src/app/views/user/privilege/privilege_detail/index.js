import {
  AppBar,
  Box,
  Card,
  CardContent,
  Container,
  Dialog,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  makeStyles,
  Select,
  Slide,
  TextField
} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { green } from '@material-ui/core/colors';
import { Formik } from 'formik';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  ACTION_TABLE,
  ACTIVE_STATUS,
  MESSAGE_TYPE,
  STATUS_API
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { Privilege } from 'src/app/models/Privilege';
import {
  createPrivilege,
  resetStatus,
  updatePrivilege
} from 'src/features/privilegeSlice';
import { showToast } from 'src/features/uiSlice';
import * as Yup from 'yup';
import CustomErrorMessage from 'src/app/components/CustomErrorMsg';
import LoadingInButton from 'src/app/components/LoadingInButton';
import ToastMessage from 'src/app/components/ToastMessage';
import ToolBarEdit from './ToolBar.js';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function DetailsPrivilege({ open, sendData, closeRef }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  console.log(sendData);
  const typeAction = sendData.type;
  const err = useSelector(state => state.privilegeSlice.err);

  const handleClose = isSaved => {
    if (!closeRef) return;
    closeRef();
  };
  const statusCreatePrivilege = useSelector(
    state => state.privilegeSlice.statusCreatePrivilege
  );
  const statusUpdatePrivilege = useSelector(
    state => state.privilegeSlice.statusUpdatePrivilege
  );
  const willLoading =
    statusCreatePrivilege === STATUS_API.SUCCESS ||
    statusCreatePrivilege === STATUS_API.ERROR ||
    statusUpdatePrivilege === STATUS_API.SUCCESS ||
    statusUpdatePrivilege === STATUS_API.ERROR;
  useEffect(() => {
    if (willLoading) dispatch(showToast());
  }, [statusCreatePrivilege, statusUpdatePrivilege]);

  const [isSubmitted, setSubmitted] = useState(false);
  function handleSubmit(value) {
    setSubmitted(true);
    let objSubmitted;
    if (typeAction === ACTION_TABLE.CREATE) {
      objSubmitted = {
        name: value.name,
        description: value.description,
        displayName: value.displayName,
        active: value.active
      };
      dispatch(createPrivilege(objSubmitted));
    } else {
      objSubmitted = {
        ...sendData.data,
        name: value.name,
        description: value.description,
        active: value.active
      };
      dispatch(updatePrivilege(objSubmitted));
    }
  }

  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={() => handleClose()}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <ToolBarEdit closeToolbarRef={handleClose} />
        </AppBar>

        <Container maxWidth="lg" style={{ paddingTop: 50 }}>
          <Grid container spacing={3}>
            <Grid
              item
              lg={12}
              md={12}
              xs={12}
              style={{
                paddingTop: '50px',
                pointerEvents: typeAction === ACTION_TABLE.PREVIEW ? 'none' : ''
              }}
            >
              <Card className={classes.shadowBox}>
                <CardContent>
                  <Formik
                    initialValues={
                      typeAction === ACTION_TABLE.CREATE
                        ? new Privilege()
                        : sendData.data
                    }
                    validationSchema={Yup.object().shape({
                      name: Yup.string()
                        .max(255)
                        .required('Tên không được để trống'),
                      displayName: Yup.string()
                        .max(50)
                        .required('Tên hiển thị không được để trống')
                    })}
                    onSubmit={handleSubmit}
                  >
                    {({
                      errors,
                      handleBlur,
                      handleChange,
                      handleSubmit,
                      touched,
                      values
                    }) => (
                      <form onSubmit={handleSubmit}>
                        <Grid container spacing={3}>
                          <Grid item md={12} xs={12}>
                            <InputLabel htmlFor="name">Tên Quyền *:</InputLabel>
                            <TextField
                              error={Boolean(touched.name && errors.name)}
                              fullWidth
                              helperText={touched.name && errors.name}
                              margin="normal"
                              name="name"
                              onBlur={handleBlur}
                              onChange={handleChange}
                              value={values.name}
                              variant="outlined"
                            />
                          </Grid>
                          <Grid
                            item
                            md={12}
                            xs={12}
                            // className={classes.wrapSelectDropdown}
                          >
                            <InputLabel htmlFor="type">Trạng thái :</InputLabel>
                            <FormControl
                              variant="outlined"
                              className={classes.formControl}
                            >
                              <Select
                                native
                                defaultValue={values.active}
                                onChange={handleChange}
                                inputProps={{
                                  name: 'active',
                                  id: 'active'
                                }}
                              >
                                <option value={ACTIVE_STATUS.ACTIVE}>
                                  Kích hoạt
                                </option>
                                <option value={ACTIVE_STATUS.INACTIVE}>
                                  Nháp
                                </option>
                              </Select>
                            </FormControl>
                          </Grid>
                          {/*=======================if preview render full information==================*/}
                          {/*{typeAction === ACTION_TABLE.PREVIEW && (*/}
                          {/*  <>*/}
                          {/*    <Grid item md={6} xs={12}>*/}
                          {/*      <InputLabel htmlFor="created_at">*/}
                          {/*        Tạo lúc :*/}
                          {/*      </InputLabel>*/}
                          {/*      <TextField*/}
                          {/*        fullWidth*/}
                          {/*        margin="normal"*/}
                          {/*        name="created"*/}
                          {/*        value={moment(values.created).format('LLL')}*/}
                          {/*        variant="outlined"*/}
                          {/*      />*/}
                          {/*    </Grid>*/}
                          {/*    {values.updated && (*/}
                          {/*      <Grid item md={6} xs={12}>*/}
                          {/*        <InputLabel htmlFor="created_at">*/}
                          {/*          Lần sửa gần nhất :*/}
                          {/*        </InputLabel>*/}
                          {/*        <TextField*/}
                          {/*          fullWidth*/}
                          {/*          margin="normal"*/}
                          {/*          name="created"*/}
                          {/*          value={moment(values.updated).format('LLL')}*/}
                          {/*          variant="outlined"*/}
                          {/*        />*/}
                          {/*      </Grid>*/}
                          {/*    )}*/}

                          {/*    <Grid item md={6} xs={12}>*/}
                          {/*      <InputLabel htmlFor="created_by">*/}
                          {/*        Tạo bởi :*/}
                          {/*      </InputLabel>*/}
                          {/*      <TextField*/}
                          {/*        fullWidth*/}
                          {/*        margin="normal"*/}
                          {/*        name="created_by"*/}
                          {/*        value={values.createdBy}*/}
                          {/*        variant="outlined"*/}
                          {/*      />*/}
                          {/*    </Grid>*/}
                          {/*    {values.updatedBy && (*/}
                          {/*      <Grid item md={6} xs={12}>*/}
                          {/*        <InputLabel htmlFor="created_by">*/}
                          {/*          Sửa gần nhất bởi:*/}
                          {/*        </InputLabel>*/}
                          {/*        <TextField*/}
                          {/*          fullWidth*/}
                          {/*          margin="normal"*/}
                          {/*          name="created_by"*/}
                          {/*          value={values.updatedBy}*/}
                          {/*          variant="outlined"*/}
                          {/*        />*/}
                          {/*      </Grid>*/}
                          {/*    )}*/}
                          {/*  </>*/}
                          {/*)}*/}
                          <Grid item md={12} xs={12}>
                            <InputLabel htmlFor="description">
                              Tên hiển thị trên hệ thống * :
                            </InputLabel>
                            <TextField
                              fullWidth
                              error={Boolean(
                                touched.displayName && errors.displayName
                              )}
                              helperText={
                                touched.displayName && errors.displayName
                              }
                              rows={5}
                              margin="normal"
                              name="displayName"
                              onBlur={handleBlur}
                              onChange={handleChange}
                              value={values.displayName}
                              variant="outlined"
                            />
                          </Grid>
                          <Grid item md={12} xs={12}>
                            <InputLabel htmlFor="description">
                              Mô tả :
                            </InputLabel>
                            <TextField
                              fullWidth
                              multiline
                              rows={5}
                              margin="normal"
                              name="description"
                              onBlur={handleBlur}
                              onChange={handleChange}
                              value={values.description}
                              variant="outlined"
                            />
                          </Grid>
                        </Grid>
                        <CustomErrorMessage content={err} />
                        <Box my={2} mt={5}>
                          <div className={classes.groupButtonSubmit}>
                            {Boolean(typeAction !== ACTION_TABLE.PREVIEW) && (
                              <div className="left-button">
                                <div className={classes.wrapper}>
                                  <Button
                                    className={classes.styleInputSearch}
                                    style={{ marginRight: '10px' }}
                                    color="primary"
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                  >
                                    {typeAction === ACTION_TABLE.CREATE
                                      ? 'Tạo mới'
                                      : 'Cập nhật'}
                                  </Button>
                                  {(statusUpdatePrivilege ===
                                    STATUS_API.PENDING ||
                                    statusUpdatePrivilege ===
                                      STATUS_API.PENDING) && (
                                    <LoadingInButton />
                                  )}
                                </div>
                                <Button
                                  size="large"
                                  variant="contained"
                                  onClick={handleClose}
                                >
                                  Thoát
                                </Button>
                              </div>
                            )}
                          </div>
                        </Box>
                      </form>
                    )}
                  </Formik>
                </CardContent>
                <Divider />
              </Card>
            </Grid>
          </Grid>
        </Container>
      </Dialog>
      {(statusCreatePrivilege === STATUS_API.SUCCESS ||
        statusCreatePrivilege === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusCreatePrivilege === STATUS_API.SUCCESS
                ? MESSAGE.CREATE_STATION_SUCCESS
                : MESSAGE.CREATE_STATION_FAIL
            }
            type={
              statusCreatePrivilege === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusUpdatePrivilege === STATUS_API.SUCCESS ||
        statusUpdatePrivilege === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusUpdatePrivilege === STATUS_API.SUCCESS
                ? MESSAGE.UPDATE_STATION_SUCCESS
                : MESSAGE.UPDATE_STATION_FAIL
            }
            type={
              statusUpdatePrivilege === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </div>
  );
}

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'fixed'
  },
  shadowBox: {
    boxShadow: '0 2px 5px rgba(0,0,0,.18)'
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  root: {
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  headerDivider: {
    backgroundColor: '#e8e7e7',
    padding: theme.spacing(1),
    marginBottom: '15px'
  },
  formControl: {
    marginTop: theme.spacing(2),
    width: '100%'
  },

  groupButtonSubmit: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '15px',

    '& .left-button': {
      display: 'flex'
    }
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  wrapper: {
    position: 'relative'
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  disableForm: {
    pointerEvents: 'none'
  },
  colorWhite: {
    color: '#fff'
  },
  tabHeader: {
    backgroundColor: 'aliceblue'
  }
}));

export default DetailsPrivilege;
