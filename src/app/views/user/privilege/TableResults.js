import {
  Checkbox,
  makeStyles,
  TableBody,
  TableCell,
  TableRow,
  Tooltip
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import moment from 'moment';
import PropTypes from 'prop-types';
import React from 'react';
import TableNullData from 'src/app/components/TableNullData';
import TextHightLight from 'src/app/components/TextHightLight';
import { ACTION_TABLE } from 'src/app/constant/config';

TableResults.propTypes = {
  listPrivilege: PropTypes.array.isRequired,
  listIdCheck: PropTypes.array.isRequired,
  setCheckAll: PropTypes.func.isRequired,
  setListIdCheck: PropTypes.func.isRequired,
  onEditPrivilege: PropTypes.func,
  actionDeletePrivilegeRef: PropTypes.func,
  isNullData: PropTypes.bool
};

function TableResults({
  listPrivilege,
  listIdCheck,
  setCheckAll,
  setListIdCheck,
  onEditPrivilege,
  actionDeletePrivilegeRef,
  isNullData
}) {
  const classes = useStyles();
  if (isNullData) return <TableNullData numberOfTableColumn={7} />;
  return (
    <TableBody>
      {listPrivilege?.map(privilege => (
        <TableRow hover key={privilege.id}>
          {/*<TableCell>*/}
          {/*  <Checkbox*/}
          {/*    checked={listIdCheck.includes(privilege.id)}*/}
          {/*    onChange={() => {*/}
          {/*      setCheckAll(true);*/}
          {/*      if (listIdCheck.includes(privilege.id)) {*/}
          {/*        setListIdCheck(*/}
          {/*          listIdCheck.filter(item => item !== privilege.id)*/}
          {/*        );*/}
          {/*      } else {*/}
          {/*        setListIdCheck(listIdCheck.concat(privilege.id));*/}
          {/*      }*/}
          {/*    }}*/}
          {/*    inputProps={{ 'aria-label': 'primary checkbox' }}*/}
          {/*  />*/}
          {/*</TableCell>*/}
          <TableCell
            onClick={() => onEditPrivilege(ACTION_TABLE.PREVIEW, privilege)}
          >
            <TextHightLight
              type={'blue'}
              content={privilege.displayName}
              isCursor={true}
            />
          </TableCell>
          <TableCell>{privilege.description}</TableCell>
          <TableCell>{moment(privilege.created).format('LLLL')}</TableCell>
          <TableCell>{privilege.createdBy}</TableCell>
          {/*<TableCell>*/}
          {/*  <div className={classes.groupAction}>*/}
          {/*    <div*/}
          {/*      className="action"*/}
          {/*      onClick={() => onEditPrivilege(ACTION_TABLE.PREVIEW, privilege)}*/}
          {/*    >*/}
          {/*      <Tooltip title="Chi tiết">*/}
          {/*        <VisibilityIcon />*/}
          {/*      </Tooltip>*/}
          {/*    </div>*/}

          {/*    <div*/}
          {/*      className="action"*/}
          {/*      onClick={() => onEditPrivilege(ACTION_TABLE.EDIT, privilege)}*/}
          {/*    >*/}
          {/*      <Tooltip title="Chỉnh sửa">*/}
          {/*        <EditIcon />*/}
          {/*      </Tooltip>*/}
          {/*    </div>*/}

          {/*    <div*/}
          {/*      className="action"*/}
          {/*      onClick={() => actionDeletePrivilegeRef(privilege)}*/}
          {/*    >*/}
          {/*      <Tooltip title="Xóa">*/}
          {/*        <DeleteIcon />*/}
          {/*      </Tooltip>*/}
          {/*    </div>*/}
          {/*  </div>*/}
          {/*</TableCell>*/}
        </TableRow>
      ))}
    </TableBody>
  );
}

const useStyles = makeStyles(theme => ({
  groupAction: {
    display: 'flex',
    alignItems: 'center',
    '& .action': {
      cursor: 'pointer',
      padding: '0 5px',
      color: '#333535',
      '&:hover': {
        color: '#3f51b5'
      }
    }
  }
}));
export default TableResults;
