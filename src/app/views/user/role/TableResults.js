import {
  Checkbox,
  Chip,
  makeStyles,
  TableBody,
  TableCell,
  TableRow,
  Tooltip
} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import moment from 'moment';
import PropTypes from 'prop-types';
import React from 'react';
import TableNullData from 'src/app/components/TableNullData';
import TextHightLight from 'src/app/components/TextHightLight';
import { ACTION_TABLE } from 'src/app/constant/config';

TableResults.propTypes = {
  listRole: PropTypes.array.isRequired,
  listIdCheck: PropTypes.array.isRequired,
  setCheckAll: PropTypes.func.isRequired,
  setListIdCheck: PropTypes.func.isRequired,
  onEditRole: PropTypes.func,
  actionDeleteRoleRef: PropTypes.func,
  isNullData: PropTypes.bool
};

function TableResults({
  listRole,
  listIdCheck,
  setCheckAll,
  setListIdCheck,
  onEditRole,
  actionDeleteRoleRef,
  permission,
  isNullData
}) {
  const classes = useStyles();
  if (isNullData) return <TableNullData numberOfTableColumn={7} />;
  return (
    <TableBody>
      {listRole?.map(role => (
        <TableRow hover key={role.id}>
          <TableCell>
            <Checkbox
              checked={listIdCheck.includes(role.id)}
              onChange={() => {
                setCheckAll(true);
                if (listIdCheck.includes(role.id)) {
                  setListIdCheck(listIdCheck.filter(item => item !== role.id));
                } else {
                  setListIdCheck(listIdCheck.concat(role.id));
                }
              }}
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
          </TableCell>
          <TableCell onClick={() => onEditRole(ACTION_TABLE.PREVIEW, role)}>
            <TextHightLight type={'blue'} content={role.name} isCursor={true} />
          </TableCell>
          <TableCell>{role.description}</TableCell>
          {/*<TableCell>*/}
          {/*  {role.privileges?.map(item => (*/}
          {/*    <Chip*/}
          {/*      color="primary"*/}
          {/*      title={item.description}*/}
          {/*      label={item.displayName}*/}
          {/*      size={'small'}*/}
          {/*      style={{ margin: 1 }}*/}
          {/*    />*/}
          {/*  )) || (*/}
          {/*    <Typography variant={'caption'} color={'secondary'}>*/}
          {/*      Chưa phân quyền*/}
          {/*    </Typography>*/}
          {/*  )}*/}
          {/*</TableCell>*/}
          <TableCell>{moment(role.created).format('LLLL')}</TableCell>
          <TableCell>{role.createdBy}</TableCell>
          <TableCell>
            <div className={classes.groupAction}>
              <div
                className="action"
                onClick={() => onEditRole(ACTION_TABLE.PREVIEW, role)}
              >
                <Tooltip title="Chi tiết">
                  <VisibilityIcon />
                </Tooltip>
              </div>
              {permission.update && (
                <div
                  className="action"
                  onClick={() => onEditRole(ACTION_TABLE.EDIT, role)}
                >
                  <Tooltip title="Chỉnh sửa">
                    <EditIcon />
                  </Tooltip>
                </div>
              )}
              {permission.delete && (
                <div
                  className="action"
                  onClick={() => actionDeleteRoleRef(role)}
                >
                  <Tooltip title="Xóa">
                    <DeleteIcon />
                  </Tooltip>
                </div>
              )}
            </div>
          </TableCell>
        </TableRow>
      ))}
    </TableBody>
  );
}

const useStyles = makeStyles(theme => ({
  groupAction: {
    display: 'flex',
    alignItems: 'center',
    '& .action': {
      cursor: 'pointer',
      padding: '0 5px',
      color: '#333535',
      '&:hover': {
        color: '#3f51b5'
      }
    }
  }
}));
export default TableResults;
