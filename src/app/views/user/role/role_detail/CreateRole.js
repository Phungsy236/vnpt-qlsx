import {
  AppBar,
  Box,
  Card,
  CardContent,
  Container,
  Dialog,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  makeStyles,
  Select,
  Slide,
  TextField
} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { green } from '@material-ui/core/colors';
import { Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AutoCompleteMulti from 'src/app/components/AutoCompleteMulti';
import ToggleBtn from 'src/app/components/ToggleBtn';
import {
  ACTION_TABLE,
  MESSAGE_TYPE,
  STATUS_API
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { DefaultSearchParams } from 'src/app/models/DefaultSearchParams';
import { Role } from 'src/app/models/Role';
import { getListPrivilege } from 'src/features/privilegeSlice';
import { createRole, resetStatus, updateRole } from 'src/features/roleSlice';
import { showToast } from 'src/features/uiSlice';
import * as Yup from 'yup';
import CustomErrorMessage from '../../../../components/CustomErrorMsg';
import LoadingInButton from '../../../../components/LoadingInButton';
import ToastMessage from '../../../../components/ToastMessage';
import ToolBarEdit from './ToolBar.js';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function FormRole({ open, sendData, closeRef }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const typeAction = sendData.type;
  const err = useSelector(state => state.roleSlice.err);

  const handleClose = isSaved => {
    if (!closeRef) return;
    closeRef();
  };
  const statusCreateRole = useSelector(
    state => state.roleSlice.statusCreateRole
  );
  const statusUpdateRole = useSelector(
    state => state.roleSlice.statusUpdateRole
  );
  const listPrivilege = useSelector(
    state => state.privilegeSlice.listPrivilege
  );
  const willLoading =
    statusCreateRole === STATUS_API.SUCCESS ||
    statusCreateRole === STATUS_API.ERROR ||
    statusUpdateRole === STATUS_API.SUCCESS ||
    statusUpdateRole === STATUS_API.ERROR;
  useEffect(() => {
    if (willLoading) dispatch(showToast());
  }, [dispatch, statusCreateRole, statusUpdateRole, willLoading]);

  useEffect(() => {
    if (!listPrivilege) dispatch(getListPrivilege(new DefaultSearchParams()));
  }, [listPrivilege, dispatch]);

  const [isSubmitted, setSubmitted] = useState(false);
  function handleSubmit(value) {
    setSubmitted(true);
    let objSubmitted;
    if (typeAction === ACTION_TABLE.CREATE) {
      objSubmitted = {
        ...new Role(),
        name: value.name,
        description: value.description,
        active: value.active
      };
      dispatch(createRole(objSubmitted));
    } else {
      objSubmitted = {
        ...sendData.data,
        name: value.name,
        description: value.description,
        active: value.active
      };
      dispatch(updateRole(objSubmitted));
    }
  }
  const [listPrivilegeChoose, setListPrivilege] = useState([]);

  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={() => handleClose()}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <ToolBarEdit closeToolbarRef={handleClose} />
        </AppBar>

        <Container maxWidth="lg">
          <Grid container spacing={3}>
            <Grid
              item
              lg={12}
              md={12}
              xs={12}
              style={{
                paddingTop: '50px',
                pointerEvents: typeAction === ACTION_TABLE.PREVIEW ? 'none' : ''
              }}
            >
              <Card className={classes.shadowBox}>
                <CardContent>
                  <Formik
                    initialValues={
                      typeAction === ACTION_TABLE.CREATE
                        ? new Role()
                        : sendData.data
                    }
                    validationSchema={Yup.object().shape({
                      name: Yup.string()
                        .max(255)
                        .required('Tên không được để trống')
                    })}
                    onSubmit={handleSubmit}
                  >
                    {({
                      errors,
                      handleBlur,
                      handleChange,
                      handleSubmit,
                      touched,
                      values,
                      setFieldValue
                    }) => (
                      <form onSubmit={handleSubmit}>
                        <Grid container spacing={3}>
                          <Grid item md={12} xs={12}>
                            <InputLabel htmlFor="name">
                              Tên Chức vụ *:
                            </InputLabel>
                            <TextField
                              error={Boolean(touched.name && errors.name)}
                              fullWidth
                              helperText={touched.name && errors.name}
                              margin="normal"
                              name="name"
                              onBlur={handleBlur}
                              onChange={handleChange}
                              value={values.name}
                              variant="outlined"
                            />
                          </Grid>
                          <Grid item md={6} xs={12}>
                            <InputLabel htmlFor="description">
                              Chức vụ này được quyền làm *:
                            </InputLabel>
                            <FormControl
                              variant="outlined"
                              className={classes.formControl}
                            >
                              {listPrivilege && (
                                <AutoCompleteMulti
                                  displayAttributeName={'name'}
                                  options={listPrivilege}
                                  placeHoldInput={'Nhập quyền hạn'}
                                  // defaultValue={[listPrivilege[2]]}
                                  handleSendValue={value => {
                                    setFieldValue('privileges', value);
                                    setListPrivilege(value);
                                  }}
                                />
                              )}
                            </FormControl>
                          </Grid>
                          <Grid item md={6} xs={12}>
                            <InputLabel htmlFor="type">Trạng thái :</InputLabel>
                            <FormControl
                              variant="outlined"
                              className={classes.formControl}
                            >
                              <Select
                                native
                                value={values.active}
                                onChange={handleChange}
                                inputProps={{
                                  name: 'active',
                                  id: 'active'
                                }}
                              >
                                <option value={true}>Kích hoạt</option>
                                <option value={false}>Nháp</option>
                              </Select>
                            </FormControl>
                          </Grid>

                          <Grid item md={12} xs={12}>
                            <InputLabel htmlFor="description">
                              Mô tả :
                            </InputLabel>
                            <TextField
                              fullWidth
                              multiline
                              rows={5}
                              margin="normal"
                              name="description"
                              onBlur={handleBlur}
                              onChange={handleChange}
                              value={values.description}
                              variant="outlined"
                            />
                          </Grid>
                        </Grid>

                        <CustomErrorMessage content={err} />
                        <Box my={2} mt={5}>
                          <div className={classes.groupButtonSubmit}>
                            {Boolean(typeAction !== ACTION_TABLE.PREVIEW) && (
                              <div className="left-button">
                                <div className={classes.wrapper}>
                                  <Button
                                    className={classes.styleInputSearch}
                                    style={{ marginRight: '10px' }}
                                    color="primary"
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                    disabled={listPrivilegeChoose.length === 0}
                                  >
                                    {typeAction === ACTION_TABLE.CREATE
                                      ? 'Tạo mới'
                                      : 'Cập nhật'}
                                  </Button>
                                  {(statusUpdateRole === STATUS_API.PENDING ||
                                    statusUpdateRole ===
                                      STATUS_API.PENDING) && (
                                    <LoadingInButton />
                                  )}
                                </div>
                                <Button
                                  size="large"
                                  variant="contained"
                                  onClick={handleClose}
                                >
                                  Thoát
                                </Button>
                              </div>
                            )}
                          </div>
                        </Box>
                      </form>
                    )}
                  </Formik>
                </CardContent>
                <Divider />
              </Card>
            </Grid>
          </Grid>
        </Container>
      </Dialog>
      {(statusCreateRole === STATUS_API.SUCCESS ||
        statusCreateRole === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusCreateRole === STATUS_API.SUCCESS
                ? MESSAGE.CREATE_STATION_SUCCESS
                : MESSAGE.CREATE_STATION_FAIL
            }
            type={
              statusCreateRole === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusUpdateRole === STATUS_API.SUCCESS ||
        statusUpdateRole === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusUpdateRole === STATUS_API.SUCCESS
                ? MESSAGE.UPDATE_STATION_SUCCESS
                : MESSAGE.UPDATE_STATION_FAIL
            }
            type={
              statusUpdateRole === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </div>
  );
}

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative'
  },
  shadowBox: {
    boxShadow: '0 2px 5px rgba(0,0,0,.18)'
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  root: {
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  headerDivider: {
    backgroundColor: '#e8e7e7',
    padding: theme.spacing(1),
    marginBottom: '15px'
  },
  formControl: {
    marginTop: theme.spacing(2),
    width: '100%'
  },

  groupButtonSubmit: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: '15px',

    '& .left-button': {
      display: 'flex'
    }
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  wrapper: {
    position: 'relative'
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  disableForm: {
    pointerEvents: 'none'
  },
  colorWhite: {
    color: '#fff'
  },
  tabHeader: {
    backgroundColor: 'aliceblue'
  }
}));

export default FormRole;
