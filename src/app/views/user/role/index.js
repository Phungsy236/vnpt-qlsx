import { Container, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DialogConfirm from 'src/app/components/DialogConfirm';
import Page from 'src/app/components/Page';
import ToastMessage from 'src/app/components/ToastMessage';
import {
  ACTION_TABLE,
  HEADER_AUTH_KEY_NAME,
  LIST_ROLE,
  MESSAGE_TYPE,
  PAGE_SIZE_LIST,
  STATUS_API,
  TYPE_DELETE
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { DefaultSearchParams } from 'src/app/models/DefaultSearchParams';
import { getAllPrivilege } from 'src/features/privilegeSlice';
import {
  deleteMultiRole,
  deleteRole,
  getListRole,
  resetStatus
} from 'src/features/roleSlice';
import {
  closeDialogConfirm,
  showDialogConfirm,
  showToast
} from 'src/features/uiSlice';
import DetailsRole from './role_detail/index';
import TableView from './TableView';
import Cookie from 'js-cookie';

const RoleListView = ({ authorities }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  // get data api
  const listRole = useSelector(state => state.roleSlice.listRole);
  const totalRole = useSelector(state => state.roleSlice.totalRole);
  const allPrivilege = useSelector(state => state.privilegeSlice.allPrivilege);
  const statusGetAll = useSelector(state => state.roleSlice.statusGetAll);
  const statusDeleteMulti = useSelector(
    state => state.roleSlice.statusDeleteMulti
  );
  const statusDeleteRole = useSelector(
    state => state.roleSlice.statusDeleteRole
  );

  const [isShowModalRoleDetails, setIsShowModalRoleDetails] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const [deleteItem, setDeleteItem] = useState(null);

  const [sendData, setSendData] = useState({
    data: undefined,
    type: null
  });

  const [params, setParams] = useState(new DefaultSearchParams());

  useEffect(() => {
    if (!Cookie.get(HEADER_AUTH_KEY_NAME)) return;
    dispatch(getListRole(params));
  }, [dispatch, params]);

  const getListRoleWithParams = data => {
    const paramValue = Object.assign({}, params, data);
    setParams(paramValue);
  };

  const showDetailsRole = data => {
    if (!data) return;
    setSendData(data);
    setIsShowModalRoleDetails(true);
  };
  const createNewRole = () => {
    setSendData({ data: {}, type: ACTION_TABLE.CREATE });
    setIsShowModalRoleDetails(true);
  };

  const [typeDelete, setTypeDelete] = useState(TYPE_DELETE.SINGLE);
  const handleDeleteMultiRole = data => {
    setTypeDelete(TYPE_DELETE.MULTI);
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };

  const confirmDeleteMultiRole = () => {
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deleteMultiRole(deleteItem));
  };

  const handleDeleteRole = data => {
    setTypeDelete(TYPE_DELETE.SINGLE);
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };

  const confirmDeleteRole = () => {
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deleteRole(deleteItem));
  };

  const willShowToast =
    statusDeleteRole === STATUS_API.SUCCESS ||
    statusDeleteRole === STATUS_API.ERROR ||
    statusDeleteMulti === STATUS_API.SUCCESS ||
    statusDeleteMulti === STATUS_API.ERROR;
  useEffect(() => {
    if (!allPrivilege) dispatch(getAllPrivilege());
  }, [dispatch]);

  useEffect(() => {
    if (willShowToast) dispatch(showToast());
  }, [statusDeleteRole, statusDeleteMulti]);
  const closeModalRoleDetails = () => {
    setIsShowModalRoleDetails(false);
    dispatch(resetStatus());
  };
  const permission = {
    create: authorities.includes(LIST_ROLE.Role_Create),
    delete: authorities.includes(LIST_ROLE.Role_Delete),
    update: authorities.includes(LIST_ROLE.Role_Update)
  };

  return (
    <Page className={classes.root} title="Role">
      <Container maxWidth={false}>
        <TableView
          actionDetailsRoleRef={showDetailsRole}
          actionDeleteRoleRef={handleDeleteRole}
          actionDeleteMultiRoleRef={handleDeleteMultiRole}
          listRole={listRole}
          totalRole={totalRole}
          isLoading={statusGetAll === STATUS_API.PENDING}
          getListRoleRef={getListRoleWithParams}
          createRef={createNewRole}
          permission={permission}
        />
      </Container>
      {typeDelete === TYPE_DELETE.SINGLE ? (
        <DialogConfirm
          title={MESSAGE.CONFIRM_DELETE_ROLE}
          textOk={MESSAGE.BTN_YES}
          textCancel={MESSAGE.BTN_CANCEL}
          callbackOk={() => confirmDeleteRole()}
          callbackCancel={() => dispatch(closeDialogConfirm())}
        />
      ) : (
        <DialogConfirm
          title={MESSAGE.CONFIRM_DELETE_ROLE}
          textOk={MESSAGE.BTN_YES}
          textCancel={MESSAGE.BTN_CANCEL}
          callbackOk={() => confirmDeleteMultiRole()}
          callbackCancel={() => dispatch(closeDialogConfirm())}
        />
      )}

      {isShowModalRoleDetails && sendData && (
        <DetailsRole
          allPrivilege={allPrivilege}
          open={isShowModalRoleDetails}
          sendData={sendData}
          closeRef={closeModalRoleDetails}
        />
      )}

      {(statusDeleteRole === STATUS_API.SUCCESS ||
        statusDeleteRole === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteRole === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_ROLE_SUCCESS
                : MESSAGE.DELETE_ROLE_FAIL
            }
            type={
              statusDeleteRole === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusDeleteMulti === STATUS_API.SUCCESS ||
        statusDeleteMulti === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_ROLE_SUCCESS
                : MESSAGE.DELETE_ROLE_FAIL
            }
            type={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </Page>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

export default RoleListView;
