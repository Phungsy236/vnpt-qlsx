import {
  Checkbox,
  makeStyles,
  TableBody,
  TableCell,
  TableRow,
  Tooltip
} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import moment from 'moment';
import PropTypes from 'prop-types';
import React from 'react';
import { useSelector } from 'react-redux';
import TableNullData from '../../components/TableNullData';
import TextHightLight from '../../components/TextHightLight';
import { ACTION_TABLE } from '../../constant/config';

TableResults.propTypes = {
  listLifeCycle: PropTypes.array.isRequired,
  listIdCheck: PropTypes.array.isRequired,
  setCheckAll: PropTypes.func.isRequired,
  setListIdCheck: PropTypes.func.isRequired,
  onEditLifeCycle: PropTypes.func,
  actionDeleteLifeCycleRef: PropTypes.func,
  isNullData: PropTypes.bool
};

function TableResults({
  listLifeCycle,
  listIdCheck,
  setCheckAll,
  setListIdCheck,
  onEditLifeCycle,
  permission,
  actionDeleteLifeCycleRef,
  isNullData
}) {
  const allStation = useSelector(state => state.stationSlice.allStation);
  const allProduct = useSelector(state => state.productSlice.allProduct);
  const classes = useStyles();
  if (isNullData) return <TableNullData numberOfTableColumn={7} />;
  return (
    <TableBody>
      {listLifeCycle?.map((lifeCycle, index) => (
        <TableRow hover key={lifeCycle.id}>
          <TableCell>
            <Checkbox
              checked={listIdCheck.includes(lifeCycle.id)}
              onChange={() => {
                setCheckAll(true);
                if (listIdCheck.includes(lifeCycle.id)) {
                  setListIdCheck(
                    listIdCheck.filter(item => item !== lifeCycle.id)
                  );
                } else {
                  setListIdCheck(listIdCheck.concat(lifeCycle.id));
                }
              }}
              inputProps={{ 'aria-label': 'primary checkbox' }}
            />
          </TableCell>
          <TableCell
            onClick={() => onEditLifeCycle(ACTION_TABLE.PREVIEW, lifeCycle)}
          >
            <TextHightLight
              type={'blue'}
              content={lifeCycle.name}
              isCursor={true}
            />
          </TableCell>
          <TableCell>
            {
              allStation?.filter(item => item.id === lifeCycle.stationId)[0]
                .name
            }
          </TableCell>{' '}
          <TableCell>
            {
              allProduct?.filter(item => item.id === lifeCycle.productId)[0]
                .name
            }
          </TableCell>
          <TableCell>{lifeCycle.value}</TableCell>
          <TableCell>
            <div className={classes.groupAction}>
              <div
                className="action"
                onClick={() => onEditLifeCycle(ACTION_TABLE.PREVIEW, lifeCycle)}
              >
                <Tooltip title="Chi tiết">
                  <VisibilityIcon />
                </Tooltip>
              </div>

              {permission.update && (
                <div
                  className="action"
                  onClick={() => onEditLifeCycle(ACTION_TABLE.EDIT, lifeCycle)}
                >
                  <Tooltip title="Chỉnh sửa">
                    <EditIcon />
                  </Tooltip>
                </div>
              )}

              {permission.delete && (
                <div
                  className="action"
                  onClick={() => actionDeleteLifeCycleRef(lifeCycle)}
                >
                  <Tooltip title="Xóa">
                    <DeleteIcon />
                  </Tooltip>
                </div>
              )}
            </div>
          </TableCell>
        </TableRow>
      ))}
    </TableBody>
  );
}

const useStyles = makeStyles(theme => ({
  groupAction: {
    display: 'flex',
    alignItems: 'center',
    '& .action': {
      cursor: 'pointer',
      padding: '0 5px',
      color: '#333535',
      '&:hover': {
        color: '#3f51b5'
      }
    }
  }
}));
export default TableResults;
