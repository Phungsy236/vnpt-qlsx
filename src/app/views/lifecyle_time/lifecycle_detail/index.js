import {
  AppBar,
  Box,
  Card,
  CardContent,
  Container,
  Dialog,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  makeStyles,
  Select,
  Slide,
  TextField
} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { green } from '@material-ui/core/colors';
import { Formik } from 'formik';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AutoCompleteMulti from 'src/app/components/AutoCompleteMulti';
import {
  ACTION_TABLE,
  ACTIVE_STATUS,
  MESSAGE_TYPE,
  STATUS_API
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { LifeCycle } from 'src/app/models/LifeCycle';
import {
  createLifeCycle,
  resetStatus,
  updateLifeCycle
} from 'src/features/lifeCycleSlice';
import { getAllProduct } from 'src/features/productSlice';
import { getAllStation } from 'src/features/stationSlice';
import { showToast } from 'src/features/uiSlice';
import * as Yup from 'yup';
import CustomErrorMessage from '../../../components/CustomErrorMsg';
import LoadingInButton from '../../../components/LoadingInButton';
import ToastMessage from '../../../components/ToastMessage';
import ToolBarEdit from './ToolBar.js';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function DetailsLifeCycle({ open, sendData, closeRef }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const typeAction = sendData.type;
  const err = useSelector(state => state.lifeCycleSlice.err);

  const handleClose = isSaved => {
    if (!closeRef) return;
    closeRef();
  };
  const statusCreateLifeCycle = useSelector(
    state => state.lifeCycleSlice.statusCreateLifeCycle
  );
  const statusUpdateLifeCycle = useSelector(
    state => state.lifeCycleSlice.statusUpdateLifeCycle
  );
  const allStation = useSelector(state => state.stationSlice.allStation);
  const allProduct = useSelector(state => state.productSlice.allProduct);
  const willLoading =
    statusCreateLifeCycle === STATUS_API.SUCCESS ||
    statusCreateLifeCycle === STATUS_API.ERROR ||
    statusUpdateLifeCycle === STATUS_API.SUCCESS ||
    statusUpdateLifeCycle === STATUS_API.ERROR;
  useEffect(() => {
    if (willLoading) dispatch(showToast());
  }, [statusCreateLifeCycle, statusUpdateLifeCycle]);

  const [isSubmitted, setSubmitted] = useState(false);
  function handleSubmit(value) {
    setSubmitted(true);
    let objSubmitted;
    if (typeAction === ACTION_TABLE.CREATE) {
      objSubmitted = {
        name: value.name,
        value: value.value,
        active: parseInt(value.active),
        stationId: value.stationId.id,
        productId: value.productId.id
      };
      dispatch(createLifeCycle(objSubmitted));
    } else {
      objSubmitted = {
        ...sendData.data,
        name: value.name,
        value: value.value,
        active: parseInt(value.active),
        stationId: value?.stationId.id || sendData.data.stationId,
        productId: value?.productId.id || sendData.data.productId
      };
      console.log(objSubmitted);
      dispatch(updateLifeCycle(objSubmitted));
    }
  }
  // const [stationId_choosed, setStation_id_choosed] = useState();
  // const [productId_choosed, setProduct_id_choosed] = useState();
  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={() => handleClose()}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <ToolBarEdit closeToolbarRef={handleClose} />
        </AppBar>

        <Container maxWidth="lg" style={{ paddingTop: 50 }}>
          <Grid container spacing={3}>
            <Grid
              item
              lg={12}
              md={12}
              xs={12}
              style={{
                paddingTop: '50px',
                pointerEvents: typeAction === ACTION_TABLE.PREVIEW ? 'none' : ''
              }}
            >
              <Card className={classes.shadowBox}>
                <CardContent>
                  <Formik
                    initialValues={
                      typeAction === ACTION_TABLE.CREATE
                        ? new LifeCycle()
                        : sendData.data
                    }
                    validationSchema={Yup.object().shape({
                      name: Yup.string()
                        .max(255)
                        .required('Tên không được để trống'),
                      value: Yup.number()
                        .required('Giá trị không được để trống')
                        .typeError('Phải là định dạng số ')
                      // stationId: Yup.number().typeError(
                      //   'Chưa chọn công đoạn sản xuất '
                      // ),
                      // .required('Chưa chọn công đoạn sản xuất'),
                      // productId: Yup.number().typeError('Chưa chọn sản phẩm')
                      // .required('Chưa chọn sản phẩm')
                    })}
                    onSubmit={handleSubmit}
                  >
                    {({
                      errors,
                      handleBlur,
                      handleChange,
                      handleSubmit,
                      setFieldValue,
                      setTouched,
                      setErrors,
                      touched,
                      values
                    }) => (
                      <form onSubmit={handleSubmit}>
                        <Grid container spacing={3}>
                          <Grid item md={6} xs={12}>
                            <InputLabel htmlFor="name">
                              Tên LifeCycle *:
                            </InputLabel>
                            <TextField
                              error={Boolean(touched.name && errors.name)}
                              fullWidth
                              helperText={touched.name && errors.name}
                              margin="normal"
                              name="name"
                              onBlur={handleBlur}
                              onChange={handleChange}
                              value={values.name}
                              variant="outlined"
                            />
                          </Grid>
                          <Grid item md={6} xs={12}>
                            <InputLabel htmlFor="value">Giá trị *:</InputLabel>
                            <TextField
                              error={Boolean(touched.value && errors.value)}
                              fullWidth
                              helperText={touched.value && errors.value}
                              margin="normal"
                              name="value"
                              onBlur={handleBlur}
                              onChange={handleChange}
                              value={values.value}
                              variant="outlined"
                            />
                          </Grid>
                          <Grid item md={12} xs={12}>
                            <InputLabel htmlFor="type">Trạng thái :</InputLabel>
                            <FormControl
                              variant="outlined"
                              className={classes.formControl}
                            >
                              <Select
                                native
                                defaultValue={values.active}
                                onChange={handleChange}
                                inputProps={{
                                  name: 'active',
                                  id: 'active'
                                }}
                              >
                                <option value={ACTIVE_STATUS.ACTIVE}>
                                  Kích hoạt
                                </option>
                                <option value={ACTIVE_STATUS.INACTIVE}>
                                  Nháp
                                </option>
                              </Select>
                            </FormControl>
                          </Grid>
                          <Grid item md={6} xs={12}>
                            <InputLabel htmlFor="stationId">
                              Chọn công đoạn sản xuất *:
                            </InputLabel>
                            <FormControl
                              variant="outlined"
                              className={classes.formControl}
                            >
                              {allStation && (
                                <AutoCompleteMulti
                                  isMulti={false}
                                  displayAttributeName={'name'}
                                  options={allStation}
                                  // isError={Boolean(
                                  //   touched.stationId && errors.stationId
                                  // )}
                                  // helperText={
                                  //   touched.stationId && errors.stationId
                                  // }
                                  placeHoldInput={'Nhập công đoạn sản xuất'}
                                  defaultValue={
                                    allStation?.filter(
                                      item => item.id === values.stationId
                                    )[0]
                                  }
                                  inputName={'stationId'}
                                  handleSendValue={value => {
                                    setFieldValue('stationId', value);
                                    // setStation_id_choosed(value);
                                  }}
                                />
                              )}
                            </FormControl>
                          </Grid>
                          <Grid item md={6} xs={12}>
                            <InputLabel htmlFor="productId">
                              Chọn sản phẩm *:
                            </InputLabel>
                            <FormControl
                              variant="outlined"
                              className={classes.formControl}
                            >
                              {allProduct && (
                                <AutoCompleteMulti
                                  isMulti={false}
                                  displayAttributeName={'name'}
                                  // isError={Boolean(
                                  //   touched.productId && errors.productId
                                  // )}
                                  // helperText={
                                  //   touched.productId && errors.productId
                                  // }
                                  // onBlurFormik={() =>
                                  //   setTouched('productId', true)
                                  // }
                                  options={allProduct}
                                  placeHoldInput={'Nhập sản phẩm'}
                                  defaultValue={
                                    allProduct?.filter(
                                      item => item.id === values.productId
                                    )[0]
                                  }
                                  handleSendValue={value => {
                                    setFieldValue('productId', value);
                                  }}
                                />
                              )}
                            </FormControl>
                          </Grid>
                          {/*=======================if preview render full information==================*/}
                          {/*{typeAction === ACTION_TABLE.PREVIEW && (*/}
                          {/*  <>*/}
                          {/*    <Grid item md={6} xs={12}>*/}
                          {/*      <InputLabel htmlFor="created_at">*/}
                          {/*        Tạo lúc :*/}
                          {/*      </InputLabel>*/}
                          {/*      <TextField*/}
                          {/*        fullWidth*/}
                          {/*        margin="normal"*/}
                          {/*        name="created"*/}
                          {/*        value={moment(values.created).format('LLL')}*/}
                          {/*        variant="outlined"*/}
                          {/*      />*/}
                          {/*    </Grid>*/}
                          {/*    {values.updated && (*/}
                          {/*      <Grid item md={6} xs={12}>*/}
                          {/*        <InputLabel htmlFor="created_at">*/}
                          {/*          Lần sửa gần nhất :*/}
                          {/*        </InputLabel>*/}
                          {/*        <TextField*/}
                          {/*          fullWidth*/}
                          {/*          margin="normal"*/}
                          {/*          name="created"*/}
                          {/*          value={moment(values.updated).format('LLL')}*/}
                          {/*          variant="outlined"*/}
                          {/*        />*/}
                          {/*      </Grid>*/}
                          {/*    )}*/}

                          {/*    <Grid item md={6} xs={12}>*/}
                          {/*      <InputLabel htmlFor="created_by">*/}
                          {/*        Tạo bởi :*/}
                          {/*      </InputLabel>*/}
                          {/*      <TextField*/}
                          {/*        fullWidth*/}
                          {/*        margin="normal"*/}
                          {/*        name="created_by"*/}
                          {/*        value={values.createdBy}*/}
                          {/*        variant="outlined"*/}
                          {/*      />*/}
                          {/*    </Grid>*/}
                          {/*    {values.updatedBy && (*/}
                          {/*      <Grid item md={6} xs={12}>*/}
                          {/*        <InputLabel htmlFor="created_by">*/}
                          {/*          Sửa gần nhất bởi:*/}
                          {/*        </InputLabel>*/}
                          {/*        <TextField*/}
                          {/*          fullWidth*/}
                          {/*          margin="normal"*/}
                          {/*          name="created_by"*/}
                          {/*          value={values.updatedBy}*/}
                          {/*          variant="outlined"*/}
                          {/*        />*/}
                          {/*      </Grid>*/}
                          {/*    )}*/}
                          {/*  </>*/}
                          {/*)}*/}
                        </Grid>
                        <CustomErrorMessage content={err} />
                        <Box my={2} mt={5}>
                          <div className={classes.groupButtonSubmit}>
                            {Boolean(typeAction !== ACTION_TABLE.PREVIEW) && (
                              <div className="left-button">
                                <div className={classes.wrapper}>
                                  <Button
                                    className={classes.styleInputSearch}
                                    style={{ marginRight: '10px' }}
                                    color="primary"
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                  >
                                    {typeAction === ACTION_TABLE.CREATE
                                      ? 'Tạo mới'
                                      : 'Cập nhật'}
                                  </Button>
                                  {(statusUpdateLifeCycle ===
                                    STATUS_API.PENDING ||
                                    statusUpdateLifeCycle ===
                                      STATUS_API.PENDING) && (
                                    <LoadingInButton />
                                  )}
                                </div>
                                <Button
                                  size="large"
                                  variant="contained"
                                  onClick={handleClose}
                                >
                                  Thoát
                                </Button>
                              </div>
                            )}
                          </div>
                        </Box>
                      </form>
                    )}
                  </Formik>
                </CardContent>
                <Divider />
              </Card>
            </Grid>
          </Grid>
        </Container>
      </Dialog>
      {(statusCreateLifeCycle === STATUS_API.SUCCESS ||
        statusCreateLifeCycle === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusCreateLifeCycle === STATUS_API.SUCCESS
                ? MESSAGE.CREATE_STATION_SUCCESS
                : MESSAGE.CREATE_STATION_FAIL
            }
            type={
              statusCreateLifeCycle === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusUpdateLifeCycle === STATUS_API.SUCCESS ||
        statusUpdateLifeCycle === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusUpdateLifeCycle === STATUS_API.SUCCESS
                ? MESSAGE.UPDATE_STATION_SUCCESS
                : MESSAGE.UPDATE_STATION_FAIL
            }
            type={
              statusUpdateLifeCycle === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </div>
  );
}

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'fixed'
  },
  shadowBox: {
    boxShadow: '0 2px 5px rgba(0,0,0,.18)'
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  root: {
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  headerDivider: {
    backgroundColor: '#e8e7e7',
    padding: theme.spacing(1),
    marginBottom: '15px'
  },
  formControl: {
    marginTop: theme.spacing(2),
    width: '100%'
  },

  groupButtonSubmit: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '15px',

    '& .left-button': {
      display: 'flex'
    }
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  wrapper: {
    position: 'relative'
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  disableForm: {
    pointerEvents: 'none'
  },
  colorWhite: {
    color: '#fff'
  },
  tabHeader: {
    backgroundColor: 'aliceblue'
  }
}));

export default DetailsLifeCycle;
