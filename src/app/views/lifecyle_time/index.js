import { Container, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DialogConfirm from 'src/app/components/DialogConfirm';
import Page from 'src/app/components/Page';
import ToastMessage from 'src/app/components/ToastMessage';
import {
  ACTION_TABLE,
  HEADER_AUTH_KEY_NAME,
  LIST_ROLE,
  MESSAGE_TYPE,
  PAGE_SIZE_LIST,
  STATUS_API,
  TYPE_DELETE
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { DefaultSearchParams } from 'src/app/models/DefaultSearchParams';
import {
  deleteMultiLifeCycle,
  deleteLifeCycle,
  getListLifeCycle,
  resetStatus
} from 'src/features/lifeCycleSlice';
import { getAllProduct } from 'src/features/productSlice';
import { getAllStation } from 'src/features/stationSlice';
import {
  closeDialogConfirm,
  showDialogConfirm,
  showToast
} from 'src/features/uiSlice';
import DetailsLifeCycle from './lifecycle_detail/index';
import TableView from './TableView';
import Cookie from 'js-cookie';

const LifeCycleListView = ({ authorities }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  // get data api
  const listLifeCycle = useSelector(
    state => state.lifeCycleSlice.listLifeCycle
  );
  const totalLifeCycle = useSelector(
    state => state.lifeCycleSlice.totalLifeCycle
  );
  const statusGetAll = useSelector(state => state.lifeCycleSlice.statusGetAll);
  const statusDeleteMulti = useSelector(
    state => state.lifeCycleSlice.statusDeleteMulti
  );
  const statusDeleteLifeCycle = useSelector(
    state => state.lifeCycleSlice.statusDeleteLifeCycle
  );
  const allStation = useSelector(state => state.stationSlice.allStation);
  const allProduct = useSelector(state => state.productSlice.allProduct);
  useEffect(() => {
    if (allStation && allStation.length > 0) return;
    else dispatch(getAllStation());
  }, [dispatch]);

  useEffect(() => {
    if (allStation && allProduct.length > 0) return;
    else dispatch(getAllProduct());
  }, [dispatch]);

  const [
    isShowModalLifeCycleDetails,
    setIsShowModalLifeCycleDetails
  ] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const [deleteItem, setDeleteItem] = useState(null);

  const [sendData, setSendData] = useState({
    data: undefined,
    type: null
  });

  const [params, setParams] = useState(new DefaultSearchParams());

  useEffect(() => {
    if (!Cookie.get(HEADER_AUTH_KEY_NAME)) return;
    dispatch(getListLifeCycle(params));
  }, [dispatch, params]);

  const getListLifeCycleWithParams = data => {
    const paramValue = Object.assign({}, params, data);
    setParams(paramValue);
  };

  const showDetailsLifeCycle = data => {
    if (!data) return;
    setSendData(data);
    setIsShowModalLifeCycleDetails(true);
  };
  const createNewLifeCycle = () => {
    setSendData({ data: {}, type: ACTION_TABLE.CREATE });
    setIsShowModalLifeCycleDetails(true);
  };

  const [typeDelete, setTypeDelete] = useState(TYPE_DELETE.SINGLE);
  const handleDeleteMultiLifeCycle = data => {
    setTypeDelete(TYPE_DELETE.MULTI);
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };

  const confirmDeleteMultiLifeCycle = () => {
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deleteMultiLifeCycle(deleteItem));
  };

  const handleDeleteLifeCycle = data => {
    setTypeDelete(TYPE_DELETE.SINGLE);
    if (!data) return;
    setDeleteItem(data);
    dispatch(showDialogConfirm());
  };

  const confirmDeleteLifeCycle = () => {
    if (!deleteItem) return;
    setIsDeleted(true);
    dispatch(deleteLifeCycle(deleteItem));
  };

  const willShowToast =
    statusDeleteLifeCycle === STATUS_API.SUCCESS ||
    statusDeleteLifeCycle === STATUS_API.ERROR ||
    statusDeleteMulti === STATUS_API.SUCCESS ||
    statusDeleteMulti === STATUS_API.ERROR;
  useEffect(() => {
    if (willShowToast) dispatch(showToast());
  }, [statusDeleteLifeCycle, statusDeleteMulti]);
  const closeModalLifeCycleDetails = () => {
    dispatch(resetStatus());
    setIsShowModalLifeCycleDetails(false);
  };
  const permission = {
    create: authorities.includes(LIST_ROLE.Cycle_Time_Create),
    delete: authorities.includes(LIST_ROLE.Cycle_Time_Delete),
    update: authorities.includes(LIST_ROLE.Cycle_Time_Update)
  };
  return (
    <Page className={classes.root} title="LifeCycle">
      <Container maxWidth={false}>
        <TableView
          actionDetailsLifeCycleRef={showDetailsLifeCycle}
          actionDeleteLifeCycleRef={handleDeleteLifeCycle}
          actionDeleteMultiLifeCycleRef={handleDeleteMultiLifeCycle}
          listLifeCycle={listLifeCycle}
          totalLifeCycle={totalLifeCycle}
          isLoading={statusGetAll === STATUS_API.PENDING}
          getListLifeCycleRef={getListLifeCycleWithParams}
          permission={permission}
          createRef={createNewLifeCycle}
        />
      </Container>
      {typeDelete === TYPE_DELETE.SINGLE ? (
        <DialogConfirm
          title={MESSAGE.CONFIRM_DELETE_STATION}
          textOk={MESSAGE.BTN_YES}
          textCancel={MESSAGE.BTN_CANCEL}
          callbackOk={() => confirmDeleteLifeCycle()}
          callbackCancel={() => dispatch(closeDialogConfirm())}
        />
      ) : (
        <DialogConfirm
          title={MESSAGE.CONFIRM_DELETE_STATION}
          textOk={MESSAGE.BTN_YES}
          textCancel={MESSAGE.BTN_CANCEL}
          callbackOk={() => confirmDeleteMultiLifeCycle()}
          callbackCancel={() => dispatch(closeDialogConfirm())}
        />
      )}

      {isShowModalLifeCycleDetails && sendData && (
        <DetailsLifeCycle
          open={isShowModalLifeCycleDetails}
          sendData={sendData}
          closeRef={closeModalLifeCycleDetails}
        />
      )}

      {(statusDeleteLifeCycle === STATUS_API.SUCCESS ||
        statusDeleteLifeCycle === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteLifeCycle === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_STATION_SUCCESS
                : MESSAGE.DELETE_STATION_FAIL
            }
            type={
              statusDeleteLifeCycle === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusDeleteMulti === STATUS_API.SUCCESS ||
        statusDeleteMulti === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_STATION_SUCCESS
                : MESSAGE.DELETE_STATION_FAIL
            }
            type={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </Page>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

export default LifeCycleListView;
