import {
  Checkbox,
  makeStyles,
  TableBody,
  TableCell,
  TableRow,
  Tooltip
} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import {
  Dvr,
  EmojiEmotions,
  GolfCourse,
  ImageSearch,
  InsertInvitation,
  Share
} from '@material-ui/icons';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllProduct } from 'src/features/productSlice';
import { getAllStation } from 'src/features/stationSlice';
import TableNullData from '../../components/TableNullData';
import TextHightLight from '../../components/TextHightLight';
import { ACTION_TABLE, MANUFACTURING_TYPE } from '../../constant/config';

TableResults.propTypes = {
  listManufacturing: PropTypes.array.isRequired,
  listIdCheck: PropTypes.array.isRequired,
  setCheckAll: PropTypes.func.isRequired,
  setListIdCheck: PropTypes.func.isRequired,
  onEditManufacturing: PropTypes.func,
  actionDeleteManufacturingRef: PropTypes.func,
  isNullData: PropTypes.bool
};

function TableResults({
  listManufacturing,
  listIdCheck,
  setCheckAll,
  allStation,
  allProduct,
  setListIdCheck,
  onEditManufacturing,
  actionDeleteManufacturingRef,
  isNullData,
  permission
}) {
  const classes = useStyles();
  if (isNullData) return <TableNullData numberOfTableColumn={7} />;

  return (
    <TableBody>
      {listManufacturing?.map(manufacturing => (
        <TableRow hover key={manufacturing.id}>
          <TableCell
            onClick={() =>
              onEditManufacturing(
                ACTION_TABLE.PREVIEW,
                manufacturing,
                MANUFACTURING_TYPE.VIEW_ALL
              )
            }
          >
            <TextHightLight
              type={'blue'}
              content={manufacturing.name}
              isCursor={true}
            />
          </TableCell>
          <TableCell>
            {' '}
            {
              allStation?.filter(item => item.id === manufacturing.stationId)[0]
                ?.name
            }
          </TableCell>

          <TableCell>
            {
              allProduct?.filter(item => item.id === manufacturing.productId)[0]
                ?.name
            }
          </TableCell>

          <TableCell>{moment(manufacturing.dateTime).format('LLLL')}</TableCell>
          <TableCell>
            <div className={classes.groupAction}>
              <IconButton
                className="action"
                onClick={() =>
                  onEditManufacturing(
                    ACTION_TABLE.PREVIEW,
                    manufacturing,
                    MANUFACTURING_TYPE.VIEW_ALL
                  )
                }
              >
                <Tooltip title="Chi tiết">
                  <VisibilityIcon />
                </Tooltip>
              </IconButton>

              {permission.update_indentify && (
                <IconButton
                  className="action"
                  onClick={() =>
                    onEditManufacturing(
                      ACTION_TABLE.EDIT,
                      manufacturing,
                      MANUFACTURING_TYPE.INDENTIFY
                    )
                  }
                >
                  <Tooltip title="Chỉnh sửa đinh danh">
                    <ImageSearch />
                  </Tooltip>
                </IconButton>
              )}
              {permission.update_strategy && (
                <IconButton
                  className="action"
                  onClick={() =>
                    onEditManufacturing(
                      ACTION_TABLE.EDIT,
                      manufacturing,
                      MANUFACTURING_TYPE.STRATEGY
                    )
                  }
                >
                  <Tooltip title="Chỉnh sửa kế hoạch">
                    <Share />
                  </Tooltip>
                </IconButton>
              )}
              {permission.update_result && (
                <IconButton
                  className="action"
                  onClick={() =>
                    onEditManufacturing(
                      ACTION_TABLE.EDIT,
                      manufacturing,
                      MANUFACTURING_TYPE.RESULT
                    )
                  }
                >
                  <Tooltip title="Chỉnh sửa kết quả">
                    <GolfCourse />
                  </Tooltip>
                </IconButton>
              )}
              {permission.delete && (
                <IconButton
                  className="action"
                  onClick={() => actionDeleteManufacturingRef(manufacturing)}
                >
                  <Tooltip title="Xóa">
                    <DeleteIcon />
                  </Tooltip>
                </IconButton>
              )}
            </div>
          </TableCell>
        </TableRow>
      ))}
    </TableBody>
  );
}

const useStyles = makeStyles(theme => ({
  groupAction: {
    display: 'flex',
    alignItems: 'center',
    '& .action': {
      cursor: 'pointer',
      padding: '0 5px',
      color: '#333535',
      '&:hover': {
        color: '#3f51b5'
      }
    }
  }
}));
export default TableResults;
