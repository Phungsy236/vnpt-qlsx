import { Container, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DialogConfirm from 'src/app/components/DialogConfirm';
import Page from 'src/app/components/Page';
import ToastMessage from 'src/app/components/ToastMessage';
import {
  ACTION_TABLE,
  HEADER_AUTH_KEY_NAME,
  LIST_ROLE,
  MANUFACTURING_TYPE,
  MESSAGE_TYPE,
  STATUS_API,
  TYPE_DELETE
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { DefaultSearchParams } from 'src/app/models/DefaultSearchParams';
import CreateForm from 'src/app/views/manufacturing_result/manufacturing_detail/CreateForm';
import DetailsManuFacturingIndentify from 'src/app/views/manufacturing_result/manufacturing_detail/indentify';
import DetailsManuFacturingResult from 'src/app/views/manufacturing_result/manufacturing_detail/result';
import DetailsManuFacturingStrategy from 'src/app/views/manufacturing_result/manufacturing_detail/strategy';
import {
  deleteManufacturing,
  getListManufacturing,
  resetStatus
} from 'src/features/manufacturingSlice';
import {
  closeDialogConfirm,
  showDialogConfirm,
  showToast
} from 'src/features/uiSlice';
import DetailsManuFacturing from './manufacturing_detail/index';
import DetailsManufacturing from './manufacturing_detail/index';
import TableView from './TableView';
import Cookie from 'js-cookie';

const ManufacturingListView = ({ authorities }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  // get data api
  const listManufacturing = useSelector(
    state => state.manufacturingSlice.listManufacturing
  );
  const totalManufacturing = useSelector(
    state => state.manufacturingSlice.totalManufacturing
  );
  const statusGetAll = useSelector(
    state => state.manufacturingSlice.statusGetAll
  );
  const statusDeleteMulti = useSelector(
    state => state.manufacturingSlice.statusDeleteMulti
  );
  const statusDeleteManufacturing = useSelector(
    state => state.manufacturingSlice.statusDeleteManufacturing
  );
  const [manufacturingType, setManuacturingType] = React.useState();
  const [
    isShowModalManufacturingDetails,
    setIsShowModalManufacturingDetails
  ] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);
  const [deleteItem, setDeleteItem] = useState(null);

  const [sendData, setSendData] = useState({
    data: undefined,
    type: null
  });

  const [params, setParams] = useState(new DefaultSearchParams());

  useEffect(() => {
    if (!Cookie.get(HEADER_AUTH_KEY_NAME)) return;
    dispatch(getListManufacturing(params));
  }, [dispatch, params]);

  const getListManufacturingWithParams = data => {
    const paramValue = Object.assign({}, params, data);
    setParams(paramValue);
  };
  const permission = {
    create_indentify: authorities.includes(LIST_ROLE.Indentify_Create),
    // create_result: authorities.includes(LIST_ROLE.Result_Create),
    // create_strategy: authorities.includes(LIST_ROLE.Strategy_Create),
    // delete: authorities.includes(LIST_ROLE.Manufacturing_Delete),
    update_indentify: authorities.includes(LIST_ROLE.Indentify_Update),
    update_result: authorities.includes(LIST_ROLE.Result_Update),
    update_strategy: authorities.includes(LIST_ROLE.Strategy_Update)
  };
  const showDetailsManufacturing = data => {
    if (!data) return;
    setSendData(data);
    setIsShowModalManufacturingDetails(true);
  };

  const createNewManufacturing = () => {
    setSendData({ data: {}, type: ACTION_TABLE.CREATE });
    setManuacturingType(MANUFACTURING_TYPE.CREATE);
    setIsShowModalManufacturingDetails(true);
  };

  // const [typeDelete, setTypeDelete] = useState(TYPE_DELETE.SINGLE);
  // const handleDeleteMultiManufacturing = data => {
  //   setTypeDelete(TYPE_DELETE.MULTI);
  //   if (!data) return;
  //   setDeleteItem(data);
  //   dispatch(showDialogConfirm());
  // };
  //
  // const confirmDeleteMultiManufacturing = () => {
  //   if (!deleteItem) return;
  //   setIsDeleted(true);
  //   dispatch(deleteMultiManufacturing(deleteItem));
  // };

  // const handleDeleteManufacturing = data => {
  //   setTypeDelete(TYPE_DELETE.SINGLE);
  //   if (!data) return;
  //   setDeleteItem(data);
  //   dispatch(showDialogConfirm());
  // };
  //
  // const confirmDeleteManufacturing = () => {
  //   if (!deleteItem) return;
  //   setIsDeleted(true);
  //   dispatch(deleteManufacturing(deleteItem));
  // };

  const willShowToast =
    statusDeleteManufacturing === STATUS_API.SUCCESS ||
    statusDeleteManufacturing === STATUS_API.ERROR ||
    statusDeleteMulti === STATUS_API.SUCCESS ||
    statusDeleteMulti === STATUS_API.ERROR;
  useEffect(() => {
    if (willShowToast) dispatch(showToast());
  }, [statusDeleteManufacturing, statusDeleteMulti]);
  const closeModalManufacturingDetails = () => {
    dispatch(resetStatus());
    setIsShowModalManufacturingDetails(false);
  };

  return (
    <Page className={classes.root} title="Manufacturing">
      <Container maxWidth={false}>
        <TableView
          actionDetailsManufacturingRef={showDetailsManufacturing}
          // actionDeleteManufacturingRef={handleDeleteManufacturing}
          // actionDeleteMultiManufacturingRef={handleDeleteMultiManufacturing}
          listManufacturing={listManufacturing}
          setTypeManufacturing={value => setManuacturingType(value)}
          totalManufacturing={totalManufacturing}
          isLoading={statusGetAll === STATUS_API.PENDING}
          getListManufacturingRef={getListManufacturingWithParams}
          createRef={createNewManufacturing}
          permission={permission}
        />
      </Container>
      {/*{typeDelete === TYPE_DELETE.SINGLE ? (*/}
      {/*  <DialogConfirm*/}
      {/*    title={MESSAGE.CONFIRM_DELETE_STATION}*/}
      {/*    textOk={MESSAGE.BTN_YES}*/}
      {/*    textCancel={MESSAGE.BTN_CANCEL}*/}
      {/*    callbackOk={() => confirmDeleteManufacturing()}*/}
      {/*    callbackCancel={() => dispatch(closeDialogConfirm())}*/}
      {/*  />*/}
      {/*) : (*/}
      {/*  <DialogConfirm*/}
      {/*    title={MESSAGE.CONFIRM_DELETE_STATION}*/}
      {/*    textOk={MESSAGE.BTN_YES}*/}
      {/*    textCancel={MESSAGE.BTN_CANCEL}*/}
      {/*    callbackOk={() => confirmDeleteMultiManufacturing()}*/}
      {/*    callbackCancel={() => dispatch(closeDialogConfirm())}*/}
      {/*  />*/}
      {/*)}*/}
      {isShowModalManufacturingDetails &&
        sendData &&
        manufacturingType === MANUFACTURING_TYPE.INDENTIFY && (
          <DetailsManuFacturingIndentify
            open={isShowModalManufacturingDetails}
            sendData={sendData}
            closeRef={closeModalManufacturingDetails}
          />
        )}
      {isShowModalManufacturingDetails &&
        sendData &&
        manufacturingType === MANUFACTURING_TYPE.STRATEGY && (
          <DetailsManuFacturingStrategy
            open={isShowModalManufacturingDetails}
            sendData={sendData}
            closeRef={closeModalManufacturingDetails}
          />
        )}{' '}
      {isShowModalManufacturingDetails &&
        sendData &&
        manufacturingType === MANUFACTURING_TYPE.RESULT && (
          <DetailsManuFacturingResult
            open={isShowModalManufacturingDetails}
            sendData={sendData}
            closeRef={closeModalManufacturingDetails}
          />
        )}
      {isShowModalManufacturingDetails &&
        sendData &&
        manufacturingType === MANUFACTURING_TYPE.VIEW_ALL && (
          <DetailsManuFacturing
            open={isShowModalManufacturingDetails}
            sendData={sendData}
            closeRef={closeModalManufacturingDetails}
          />
        )}
      {isShowModalManufacturingDetails &&
        sendData &&
        manufacturingType === MANUFACTURING_TYPE.CREATE && (
          <CreateForm
            open={isShowModalManufacturingDetails}
            sendData={sendData}
            closeRef={closeModalManufacturingDetails}
          />
        )}
      {(statusDeleteManufacturing === STATUS_API.SUCCESS ||
        statusDeleteManufacturing === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteManufacturing === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_STATION_SUCCESS
                : MESSAGE.DELETE_STATION_FAIL
            }
            type={
              statusDeleteManufacturing === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusDeleteMulti === STATUS_API.SUCCESS ||
        statusDeleteMulti === STATUS_API.ERROR) &&
        isDeleted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE.DELETE_STATION_SUCCESS
                : MESSAGE.DELETE_STATION_FAIL
            }
            type={
              statusDeleteMulti === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </Page>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

export default ManufacturingListView;
