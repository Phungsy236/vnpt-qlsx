import {
  Box,
  Button,
  Card,
  CardContent,
  Checkbox,
  Fab,
  Hidden,
  makeStyles,
  Table,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  useTheme,
  Zoom
} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { Trash2 } from 'react-feather';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useDispatch, useSelector } from 'react-redux';
import {
  ACTION_TABLE,
  MANUFACTURING_TYPE,
  PAGE_SIZE_LIST,
  STATUS_API
} from 'src/app/constant/config';
import { parseObjToSearchQuery } from 'src/app/utils/apiService';
import { sortManufacturingByName } from 'src/features/manufacturingSlice';
import { getAllProduct } from 'src/features/productSlice';
import { getAllStation } from 'src/features/stationSlice';
import HeaderTableCustom from '../../components/HeaderTableCustom';
import LoadingTableResult from '../../components/LoadingTableResult';
import TableResults from './TableResults';

const TableView = ({
  className,
  listManufacturing,
  setTypeManufacturing,
  isLoading,
  getListManufacturingRef,
  actionDetailsManufacturingRef,
  totalManufacturing,
  actionDeleteManufacturingRef,
  actionDeleteMultiManufacturingRef,
  handleShowDeleteIcon,
  createRef,
  permission,
  ...rest
}) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [limit, setLimit] = useState(PAGE_SIZE_LIST);
  const [page, setPage] = useState(0);
  const [params, setParams] = useState({
    page: page,
    size: limit,
    query: ''
  });
  const theme = useTheme();
  const transitionDuration = {
    enter: theme.transitions.duration.enteringScreen,
    exit: theme.transitions.duration.leavingScreen
  };
  const [queryValue, setQueryValue] = useState({});
  let isNullData = !listManufacturing || listManufacturing.length === 0;
  const statusDeleteMulti = useSelector(
    state => state.manufacturingSlice.statusDeleteMulti
  );
  const handleLimitChange = event => {
    setLimit(event.target.value);
    if (!getListManufacturingRef) return;
    const newparams = Object.assign({}, params, {
      size: event.target.value,
      page: 0
    });
    setParams(newparams);
    getListManufacturingRef(newparams);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
    if (!getListManufacturingRef) return;
    const newparams = Object.assign({}, params, { page: newPage });
    setParams(newparams);
    getListManufacturingRef(newparams);
  };
  function handleSearchByName(data, propertyName) {
    let temp = { ...queryValue };
    temp[propertyName] = data;
    setQueryValue(temp);
    let newParams = Object.assign({}, params, {
      page: 0,
      query: parseObjToSearchQuery(temp)
    });
    setParams(newParams);
    return getListManufacturingRef(newParams);
  }

  const handleSort = type => {
    return !isNullData && dispatch(sortManufacturingByName(type));
  };
  const onEditManufacturing = (type, manufacturing, manufType) => {
    if (!actionDetailsManufacturingRef) return;
    const sendData = { type: type, data: manufacturing };
    actionDetailsManufacturingRef(sendData);
    setTypeManufacturing(manufType);
  };
  const onEditManufacturingViaId = (type, id) => {
    if (!actionDetailsManufacturingRef) return;
    const sendData = {
      type: type,
      data: listManufacturing.filter(item => item.id === id[0])[0]
    };
    actionDetailsManufacturingRef(sendData);
  };
  const [listIdCheck, setListIdCheck] = useState([]);
  const [isCheckAll, setCheckAll] = useState(false);
  const [isShowDeleteIcon, setIsShowDeleteIcon] = useState(false);
  function handleDeleteMulti() {
    actionDeleteMultiManufacturingRef(listIdCheck);
  }
  const allStation = useSelector(state => state.stationSlice.allStation);
  const allProduct = useSelector(state => state.productSlice.allProduct);
  useEffect(() => {
    if (allStation && allStation.length > 0) return;
    else dispatch(getAllStation());
  }, [dispatch]);

  useEffect(() => {
    if (allStation && allProduct.length > 0) return;
    else dispatch(getAllProduct());
  }, [dispatch]);
  // if delete success remove listIdcheck and uncheckALl
  useEffect(() => {
    if (statusDeleteMulti === STATUS_API.SUCCESS) {
      setCheckAll(false);
      setListIdCheck([]);
    }
  }, [statusDeleteMulti]);
  useEffect(() => {
    if (listIdCheck.length >= 2) {
      setIsShowDeleteIcon(true);
    } else {
      setCheckAll(false);
      setIsShowDeleteIcon(false);
    }
  }, [listIdCheck]);

  return (
    <div>
      <Box>
        <Card>
          <CardContent>
            <Box className={classes.toolbar}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-around',
                  width: '30%'
                }}
              >
                {permission.view && (
                  <Zoom
                    key={'preview'}
                    in={listIdCheck.length === 1}
                    timeout={transitionDuration}
                    style={{
                      transitionDelay: `${
                        isShowDeleteIcon ? transitionDuration.exit : 0
                      }ms`
                    }}
                    unmountOnExit
                  >
                    <Box
                      onClick={() =>
                        onEditManufacturingViaId(
                          ACTION_TABLE.PREVIEW,
                          listIdCheck,
                          MANUFACTURING_TYPE.VIEW_ALL
                        )
                      }
                    >
                      <Fab
                        aria-label={'delete'}
                        size={'small'}
                        style={{ color: 'white', backgroundColor: '#3f51b5' }}
                      >
                        <VisibilityIcon />
                      </Fab>
                      <Hidden mdDown>
                        <Typography variant={'caption'} color={'primary'}>
                          {' '}
                          Chi tiết
                        </Typography>
                      </Hidden>
                    </Box>
                  </Zoom>
                )}
                {permission.update && (
                  <Zoom
                    key={'edit'}
                    in={listIdCheck.length === 1}
                    timeout={transitionDuration}
                    style={{
                      transitionDelay: `${
                        isShowDeleteIcon ? transitionDuration.exit : 0
                      }ms`
                    }}
                    unmountOnExit
                  >
                    <Box
                      onClick={() =>
                        onEditManufacturingViaId(ACTION_TABLE.EDIT, listIdCheck)
                      }
                    >
                      <Fab
                        aria-label={'delete'}
                        size={'small'}
                        style={{ color: 'white', backgroundColor: 'orange' }}
                      >
                        <EditIcon />
                      </Fab>
                      <Hidden mdDown>
                        <Typography variant={'caption'} color={'primary'}>
                          {' '}
                          Cập nhật
                        </Typography>
                      </Hidden>
                    </Box>
                  </Zoom>
                )}
                {permission.delete && (
                  <Zoom
                    key={'delete'}
                    in={listIdCheck.length !== 0}
                    timeout={transitionDuration}
                    style={{
                      transitionDelay: `${
                        isShowDeleteIcon ? transitionDuration.exit : 0
                      }ms`
                    }}
                    unmountOnExit
                  >
                    <Box onClick={handleDeleteMulti}>
                      <Fab
                        aria-label={'delete'}
                        size={'small'}
                        style={{ color: 'white', backgroundColor: '#f80759' }}
                      >
                        <Trash2 />
                      </Fab>
                      <Hidden mdDown>
                        <Typography variant={'caption'} color={'primary'}>
                          {' '}
                          Xóa
                        </Typography>
                      </Hidden>
                    </Box>
                  </Zoom>
                )}
              </div>
              <Fab
                variant={'extended'}
                color="secondary"
                aria-label="create"
                size={'small'}
                disabled={!permission.create_indentify}
                onClick={() => createRef()}
              >
                <EditIcon
                  style={{ color: 'white', marginRight: theme.spacing(1) }}
                />
                <Typography variant={'button'} style={{ color: 'white' }}>
                  {' '}
                  Tạo mới
                </Typography>
              </Fab>
            </Box>
          </CardContent>
        </Card>
      </Box>

      <Box mt={3}>
        <Card className={clsx(classes.root, className)} {...rest}>
          <PerfectScrollbar>
            <Box minWidth={1050}>
              <Table>
                <TableHead>
                  <TableRow>
                    {/*<TableCell>*/}
                    {/*  <Checkbox*/}
                    {/*    checked={isCheckAll}*/}
                    {/*    onChange={() => {*/}
                    {/*      if (isNullData) return;*/}
                    {/*      setCheckAll(!isCheckAll);*/}
                    {/*      if (!isCheckAll)*/}
                    {/*        setListIdCheck(*/}
                    {/*          listManufacturing.map(item => item.id)*/}
                    {/*        );*/}
                    {/*      else setListIdCheck([]);*/}
                    {/*    }}*/}
                    {/*    inputProps={{ 'aria-label': 'primary checkbox' }}*/}
                    {/*  />*/}
                    {/*</TableCell>*/}
                    <HeaderTableCustom
                      title={'Tên kết quả sản xuất'}
                      searchName={'manufacturingName'}
                      callbackSearch={data =>
                        handleSearchByName(data.trim(), 'name')
                      } // data is Object
                      callbackSort={handleSort}
                      isEnableSearch={true}
                      isEnableSort={true}
                    />
                    <HeaderTableCustom
                      title={'Công đoạn'}
                      typeSearch={'select'}
                      searchName={'stationId'}
                      placeHolder={'station'}
                      optionSearch={allStation}
                      callbackSearch={data =>
                        handleSearchByName(data, 'stationId')
                      }
                      isEnableSearch={true}
                      isEnableSort={false}
                    />
                    <HeaderTableCustom
                      title={'Sản phẩm'}
                      typeSearch={'select'}
                      placeHolder={'product'}
                      searchName={'productId'}
                      optionSearch={allProduct}
                      callbackSearch={data =>
                        handleSearchByName(data, 'productId')
                      }
                      isEnableSearch={true}
                      isEnableSort={false}
                    />
                    <HeaderTableCustom
                      title={'Thời gian'}
                      searchName={'date_time'}
                      callbackSearch={data =>
                        handleSearchByName(data.trim(), 'createdBy')
                      } // data is Object
                      callbackSort={handleSort}
                      isEnableSearch={true}
                      isEnableSort={false}
                    />
                    <TableCell>Thao tác</TableCell>
                  </TableRow>
                </TableHead>
                {isLoading ? (
                  <LoadingTableResult numberOfTableColumn={7} />
                ) : (
                  <TableResults
                    permission={permission}
                    allProduct={allProduct}
                    allStation={allStation}
                    // setTypeManufacturing={setTypeManufacturing}
                    setCheckAll={value => setCheckAll(value)}
                    listManufacturing={listManufacturing}
                    setListIdCheck={value => setListIdCheck(value)}
                    listIdCheck={listIdCheck}
                    onEditManufacturing={onEditManufacturing}
                    actionDeleteManufacturingRef={actionDeleteManufacturingRef}
                    isNullData={isNullData}
                  />
                )}
              </Table>
            </Box>
          </PerfectScrollbar>
          <TablePagination
            component="div"
            count={totalManufacturing}
            onChangePage={handlePageChange}
            onChangeRowsPerPage={handleLimitChange}
            page={page}
            rowsPerPage={limit}
            rowsPerPageOptions={isNullData ? [] : [5, 10, 25]}
          />
        </Card>
      </Box>
    </div>
  );
};

TableView.propTypes = {
  className: PropTypes.string,
  listManufacturing: PropTypes.array.isRequired
};

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative'
  },

  minWithColumn: {
    minWidth: '150px'
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    minWidth: '50%'
  }
}));

export default TableView;
