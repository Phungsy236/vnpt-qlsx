import {
  AppBar,
  Box,
  Card,
  CardContent,
  Container,
  Dialog,
  Divider,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  makeStyles,
  Select,
  Slide,
  TextField
} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { green } from '@material-ui/core/colors';
import Typography from '@material-ui/core/Typography';
import { Formik } from 'formik';
import moment from 'moment/moment';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AutoCompleteCreateAble from 'src/app/components/AutoCompleteCreateAble';
import AutoCompleteMulti from 'src/app/components/AutoCompleteMulti';
import { DefaultSearchParams } from 'src/app/models/DefaultSearchParams';
import { parseObjToSearchQuery } from 'src/app/utils/apiService';
import { getListLifeCycle } from 'src/features/lifeCycleSlice';
import { getAllProduct } from 'src/features/productSlice';
import { getAllStation } from 'src/features/stationSlice';
import * as Yup from 'yup';
import {
  createManufacturing,
  resetStatus,
  updateManufacturing
} from 'src/features/manufacturingSlice';
import { showToast } from 'src/features/uiSlice';
import CustomErrorMessage from '../../../components/CustomErrorMsg';
import LoadingInButton from '../../../components/LoadingInButton';
import ToastMessage from '../../../components/ToastMessage';
import {
  ACTION_TABLE,
  ACTIVE_STATUS,
  MESSAGE_TYPE,
  SHIFT,
  STATUS_API
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { Manufacturing } from 'src/app/models/Manufacturing';
import ToolBarEdit from './ToolBar.js';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function CreateManufacturing({ open, sendData, closeRef }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const typeAction = sendData.type;
  const err = useSelector(state => state.manufacturingSlice.err);

  const handleClose = isSaved => {
    if (!closeRef) return;
    closeRef();
  };
  const statusCreateManuFacturing = useSelector(
    state => state.manufacturingSlice.statusCreateManuFacturing
  );
  const statusUpdateManuFacturing = useSelector(
    state => state.manufacturingSlice.statusUpdateManuFacturing
  );
  useEffect(() => {
    if (allStation && allStation.length > 0) return;
    else dispatch(getAllStation());
  }, [dispatch]);

  useEffect(() => {
    if (allStation && allProduct.length > 0) return;
    else dispatch(getAllProduct());
  }, [dispatch]);

  const allStation = useSelector(state => state.stationSlice.allStation);
  const allProduct = useSelector(state => state.productSlice.allProduct);

  const [cycleTimeMaterial, setCycleTimeMaterial] = useState({
    stationId: null,
    productId: null
  });
  useEffect(() => {
    if (cycleTimeMaterial.productId && cycleTimeMaterial.stationId) {
      let param = {
        ...new DefaultSearchParams(),
        query: parseObjToSearchQuery(cycleTimeMaterial)
      };
      dispatch(getListLifeCycle(param));
    }
  }, [cycleTimeMaterial]);
  const listLifeCycle = useSelector(
    state => state.lifeCycleSlice.listLifeCycle
  );
  const willLoading =
    statusCreateManuFacturing === STATUS_API.SUCCESS ||
    statusCreateManuFacturing === STATUS_API.ERROR ||
    statusUpdateManuFacturing === STATUS_API.SUCCESS ||
    statusUpdateManuFacturing === STATUS_API.ERROR;
  useEffect(() => {
    if (willLoading) dispatch(showToast());
  }, [statusCreateManuFacturing, statusUpdateManuFacturing]);

  const [isSubmitted, setSubmitted] = useState(false);
  function handleSubmit(value) {
    console.log(value);
    setSubmitted(true);
    let objSubmitted;
    if (typeAction === ACTION_TABLE.CREATE) {
      objSubmitted = {
        ...new Manufacturing(),
        name: value.name,
        dateTime: moment(value.dateTime).valueOf(),
        shift: value.shift,
        cycleTime: value.cycleTime,
        stationId: value.stationId,
        productId: value.productId
      };
      dispatch(createManufacturing(objSubmitted));
    } else {
      objSubmitted = {
        ...sendData.data,
        name: value.name,
        dateTime: value.dateTime,
        shift: value.shift,
        cycleTime: value.cycleTime,
        productId: value.productId,
        stationId: value.stationId
      };
      dispatch(updateManufacturing(objSubmitted));
    }
  }

  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={() => handleClose()}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <ToolBarEdit closeToolbarRef={handleClose} />
        </AppBar>

        <Container maxWidth="lg">
          <Grid container spacing={3}>
            <Grid
              item
              lg={12}
              md={12}
              xs={12}
              style={{
                paddingTop: '50px',
                pointerEvents: typeAction === ACTION_TABLE.PREVIEW ? 'none' : ''
              }}
            >
              <Card className={classes.shadowBox}>
                <CardContent>
                  <Formik
                    initialValues={
                      typeAction === ACTION_TABLE.CREATE
                        ? new Manufacturing()
                        : sendData.data
                    }
                    validationSchema={Yup.object().shape({
                      name: Yup.string()
                        .max(255)
                        .required('Tên không được để trống'),
                      // dateTime: Yup.date().required(
                      //   'Ngày giờ không được để trống'
                      // ),
                      // productId: Yup.number().required('Chưa chọn sản phầm'),
                      // stationId: Yup.number().required(
                      //   'Chưa chọn công đoạn sản xuất'
                      // ),
                      // shift: Yup.string().required(
                      //   'Chưa chọn ca sản xuất sản xuất'
                      // ),
                      wo: Yup.string().required('Chưa nhập mã lệnh sản xuất')
                      // cycleTime: Yup.number()
                    })}
                    onSubmit={handleSubmit}
                  >
                    {({
                      errors,
                      handleBlur,
                      handleChange,
                      handleSubmit,
                      setFieldValue,
                      touched,
                      values
                    }) => (
                      <form onSubmit={handleSubmit}>
                        <Grid container spacing={3}>
                          <Grid item md={12} xs={12}>
                            <InputLabel
                              htmlFor="name"
                              className={classes.inputLabel}
                            >
                              Tên bản ghi kết quả sản xuất *:
                            </InputLabel>
                            <TextField
                              error={Boolean(touched.name && errors.name)}
                              fullWidth
                              helperText={touched.name && errors.name}
                              margin="normal"
                              name="name"
                              placeholder={`Kết quả sản xuất ngày ${moment(
                                new Date()
                              ).format('LL')}`}
                              onBlur={handleBlur}
                              onChange={handleChange}
                              value={values.name}
                              variant="outlined"
                            />
                          </Grid>
                        </Grid>
                        <Grid container spacing={3}>
                          <Grid item md={4} xs={12}>
                            <InputLabel
                              htmlFor="name"
                              className={classes.inputLabel}
                            >
                              Mã lệnh sản xuất *:
                            </InputLabel>
                            <TextField
                              error={Boolean(touched.wo && errors.wo)}
                              fullWidth
                              helperText={touched.wo && errors.wo}
                              margin="normal"
                              placeholder={'Wo2Y3D'}
                              name="wo"
                              onBlur={handleBlur}
                              onChange={handleChange}
                              value={values.wo}
                              variant="outlined"
                            />
                          </Grid>
                          <Grid item md={4} xs={12}>
                            <InputLabel
                              htmlFor="name"
                              className={classes.inputLabel}
                            >
                              Ca sản xuất *:
                            </InputLabel>
                            <FormControl
                              variant="outlined"
                              className={classes.formControl}
                            >
                              <Select
                                native
                                defaultValue={values.shift}
                                onChange={event => {
                                  setFieldValue('shift', event.target.value);
                                }}
                                inputProps={{
                                  name: 'shift',
                                  id: 'shift'
                                }}
                              >
                                {SHIFT.map((item, index) => (
                                  <option value={item} key={index}>
                                    {item}
                                  </option>
                                ))}
                              </Select>
                            </FormControl>
                          </Grid>
                          <Grid item md={4} xs={12}>
                            <InputLabel
                              htmlFor="dateTime"
                              className={classes.inputLabel}
                            >
                              Thời gian bắt đầu sản xuất *:
                            </InputLabel>
                            <FormControl
                              variant="outlined"
                              className={classes.formControl}
                            >
                              <TextField
                                id="dateTime"
                                error={Boolean(
                                  touched.dateTime && errors.dateTime
                                )}
                                helperText={touched.dateTime && errors.dateTime}
                                type="datetime-local"
                                name={'dateTime'}
                                variant={'outlined'}
                                onChange={event => {
                                  setFieldValue('dateTime', event.target.value);
                                }}
                                defaultValue={
                                  moment(new Date()).format(
                                    'YYYY-MM-DD[T]HH:mm'
                                  )
                                  // .split('+')[0]
                                }
                                className={classes.textField}
                                InputLabelProps={{
                                  shrink: true
                                }}
                                InputProps={{
                                  name: 'dateTime',
                                  id: 'dateTime'
                                }}
                              />
                            </FormControl>
                          </Grid>

                          <Grid container spacing={3} style={{ padding: 10 }}>
                            <Grid item md={4} xs={12}>
                              <InputLabel
                                htmlFor="stationId"
                                className={classes.inputLabel}
                              >
                                Chọn công đoạn sản xuất *:
                              </InputLabel>
                              <FormControl
                                variant="outlined"
                                className={classes.formControl}
                              >
                                {allStation && (
                                  <AutoCompleteMulti
                                    isMulti={false}
                                    displayAttributeName={'name'}
                                    options={allStation}
                                    placeHoldInput={'Nhập công đoạn sản xuất'}
                                    defaultValue={
                                      allStation?.filter(
                                        item => item.id === values.stationId
                                      )[0]
                                    }
                                    inputName={'stationId'}
                                    handleSendValue={value => {
                                      setCycleTimeMaterial({
                                        ...cycleTimeMaterial,
                                        stationId: value.id
                                      });
                                      setFieldValue('stationId', value.id);
                                      // setStation_id_choosed(value);
                                    }}
                                  />
                                )}
                              </FormControl>
                            </Grid>
                            <Grid item md={4} xs={12}>
                              <InputLabel
                                htmlFor="productId"
                                className={classes.inputLabel}
                              >
                                Tên sản phẩm *:
                              </InputLabel>
                              <FormControl
                                variant="outlined"
                                className={classes.formControl}
                              >
                                {allProduct && (
                                  <AutoCompleteMulti
                                    isMulti={false}
                                    displayAttributeName={'name'}
                                    options={allProduct}
                                    placeHoldInput={'Nhập sản phẩm'}
                                    inputName={'productId'}
                                    defaultValue={
                                      allProduct?.filter(
                                        item => item.id === values.productId
                                      )[0]
                                    }
                                    handleSendValue={value => {
                                      setCycleTimeMaterial({
                                        ...cycleTimeMaterial,
                                        productId: value.id
                                      });
                                      setFieldValue('productId', value.id);
                                    }}
                                  />
                                )}
                              </FormControl>
                            </Grid>
                            <Grid item md={4} xs={12}>
                              <InputLabel
                                htmlFor="name"
                                className={classes.inputLabel}
                              >
                                Thời gian tạo sản phẩm
                                <Box>
                                  (
                                  <Typography variant={'caption'}>
                                    Chọn công đoạn sản xuất và sản phẩm trước
                                  </Typography>{' '}
                                  ) *:
                                </Box>
                              </InputLabel>
                              <FormControl
                                variant="outlined"
                                className={classes.formControl}
                              >
                                <AutoCompleteCreateAble
                                  options={listLifeCycle}
                                  getter={value => {
                                    console.log(value);
                                    setFieldValue('cycleTime', value);
                                  }}
                                  isDisable={
                                    !cycleTimeMaterial.stationId ||
                                    !cycleTimeMaterial.productId
                                  }
                                />
                              </FormControl>
                            </Grid>
                          </Grid>
                        </Grid>

                        <CustomErrorMessage content={err} />
                        <br />

                        <Box my={2} mt={5}>
                          <div className={classes.groupButtonSubmit}>
                            {Boolean(typeAction !== ACTION_TABLE.PREVIEW) && (
                              <div className="left-button">
                                <div className={classes.wrapper}>
                                  <Button
                                    className={classes.styleInputSearch}
                                    style={{ marginRight: '10px' }}
                                    color="primary"
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                  >
                                    {typeAction === ACTION_TABLE.CREATE
                                      ? 'Tạo mới'
                                      : 'Cập nhật'}
                                  </Button>
                                  {(statusUpdateManuFacturing ===
                                    STATUS_API.PENDING ||
                                    statusUpdateManuFacturing ===
                                      STATUS_API.PENDING) && (
                                    <LoadingInButton />
                                  )}
                                </div>
                                <Button
                                  size="large"
                                  variant="contained"
                                  onClick={handleClose}
                                >
                                  Thoát
                                </Button>
                              </div>
                            )}
                          </div>
                        </Box>
                      </form>
                    )}
                  </Formik>
                </CardContent>
                <Divider />
              </Card>
            </Grid>
          </Grid>
        </Container>
      </Dialog>
      {(statusCreateManuFacturing === STATUS_API.SUCCESS ||
        statusCreateManuFacturing === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusCreateManuFacturing === STATUS_API.SUCCESS
                ? MESSAGE.CREATE_STATION_SUCCESS
                : MESSAGE.CREATE_STATION_FAIL
            }
            type={
              statusCreateManuFacturing === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusUpdateManuFacturing === STATUS_API.SUCCESS ||
        statusUpdateManuFacturing === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusUpdateManuFacturing === STATUS_API.SUCCESS
                ? MESSAGE.UPDATE_STATION_SUCCESS
                : MESSAGE.UPDATE_STATION_FAIL
            }
            type={
              statusUpdateManuFacturing === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </div>
  );
}

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative'
  },
  shadowBox: {
    boxShadow: '0 2px 5px rgba(0,0,0,.18)'
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  inputLabel: {
    height: 30
  },
  root: {
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  headerDivider: {
    backgroundColor: '#e8e7e7',
    padding: theme.spacing(1),
    marginBottom: '15px'
  },
  heading: {
    fontSize: theme.typography.pxToRem(17),
    fontWeight: 500,
    flexBasis: '70%',
    flexShrink: 0
  },
  formControl: {
    marginTop: theme.spacing(2),
    width: '100%'
  },
  inputAutoGen: {
    backgroundColor: '#cffbfe'
  },
  groupButtonSubmit: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '15px',

    '& .left-button': {
      display: 'flex'
    }
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  wrapper: {
    position: 'relative'
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  disableForm: {
    pointerEvents: 'none'
  },
  colorWhite: {
    color: '#fff'
  },
  tabHeader: {
    backgroundColor: 'aliceblue'
  }
}));

export default CreateManufacturing;
