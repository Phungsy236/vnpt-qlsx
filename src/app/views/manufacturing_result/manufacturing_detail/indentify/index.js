import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  AppBar,
  Box,
  Card,
  CardContent,
  Container,
  Dialog,
  Divider,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  makeStyles,
  Slide,
  TextField
} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { green } from '@material-ui/core/colors';
import Typography from '@material-ui/core/Typography';
import { Formik } from 'formik';
import moment from 'moment/moment';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AutoCompleteMulti from 'src/app/components/AutoCompleteMulti';
import { getAllProduct } from 'src/features/productSlice';
import { getAllStation } from 'src/features/stationSlice';
import * as Yup from 'yup';
import {
  createManufacturing,
  resetStatus,
  updateManufacturing
} from 'src/features/manufacturingSlice';
import { showToast } from 'src/features/uiSlice';
import CustomErrorMessage from '../../../../components/CustomErrorMsg';
import LoadingInButton from '../../../../components/LoadingInButton';
import ToastMessage from '../../../../components/ToastMessage';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  ACTION_TABLE,
  MESSAGE_TYPE,
  STATUS_API
} from 'src/app/constant/config';
import { MESSAGE } from 'src/app/constant/message';
import { Manufacturing } from 'src/app/models/Manufacturing';
import ToolBarEdit from './ToolBar.js';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function DetailsManuFacturingIndentify({ open, sendData, closeRef }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const typeAction = sendData.type;
  const err = useSelector(state => state.manufacturingSlice.err);

  const handleClose = isSaved => {
    if (!closeRef) return;
    closeRef();
  };
  const statusCreateManuFacturing = useSelector(
    state => state.manufacturingSlice.statusCreateManuFacturing
  );
  const statusUpdateManuFacturing = useSelector(
    state => state.manufacturingSlice.statusUpdateManuFacturing
  );
  useEffect(() => {
    if (allStation && allStation.length > 0) return;
    else dispatch(getAllStation());
  }, [dispatch]);

  useEffect(() => {
    if (allStation && allProduct.length > 0) return;
    else dispatch(getAllProduct());
  }, [dispatch]);
  const allStation = useSelector(state => state.stationSlice.allStation);
  const allProduct = useSelector(state => state.productSlice.allProduct);
  const [expanded, setExpanded] = React.useState(false);
  const handleTabChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
  const willLoading =
    statusUpdateManuFacturing === STATUS_API.SUCCESS ||
    statusUpdateManuFacturing === STATUS_API.ERROR;
  useEffect(() => {
    if (willLoading) dispatch(showToast());
  }, [statusCreateManuFacturing, statusUpdateManuFacturing]);

  const [isSubmitted, setSubmitted] = useState(false);
  function handleSubmit(value) {
    setSubmitted(true);
    let objSubmitted;
    objSubmitted = {
      ...sendData.data,
      name: value.name,
      date_time: value.date_time,
      active: value.active
    };
    dispatch(updateManufacturing(objSubmitted));
  }

  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={() => handleClose()}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <ToolBarEdit
            closeToolbarRef={handleClose}
            title={sendData.data.name}
          />
        </AppBar>

        <Container maxWidth="lg">
          <Grid container spacing={3}>
            <Grid
              item
              lg={12}
              md={12}
              xs={12}
              style={{
                paddingTop: '50px',
                pointerEvents: ''
              }}
            >
              <Card className={classes.shadowBox}>
                <CardContent>
                  <Formik
                    initialValues={
                      typeAction === ACTION_TABLE.CREATE
                        ? new Manufacturing()
                        : sendData.data
                    }
                    validationSchema={Yup.object().shape({
                      name: Yup.string()
                        .max(255)
                        .required('Tên không được để trống'),
                      date_time: Yup.date().required(
                        'Ngày giờ không được để trống'
                      ),
                      product_id: Yup.number().required('Chưa chọn sản phầm'),
                      station_id: Yup.number().required(
                        'Chưa chọn công đoạn sản xuất'
                      ),
                      shift: Yup.string(),
                      wo: Yup.string()
                    })}
                    onSubmit={handleSubmit}
                  >
                    {({
                      errors,
                      handleBlur,
                      handleChange,
                      handleSubmit,
                      setFieldValue,
                      setTouched,
                      touched,
                      values
                    }) => (
                      <form onSubmit={handleSubmit}>
                        <Grid container spacing={3}>
                          <Grid item md={12} xs={12}>
                            <InputLabel
                              htmlFor="name"
                              className={classes.inputLabel}
                            >
                              Tên :
                            </InputLabel>
                            <TextField
                              error={Boolean(touched.name && errors.name)}
                              fullWidth
                              helperText={touched.name && errors.name}
                              margin="normal"
                              name="name"
                              onBlur={handleBlur}
                              onChange={handleChange}
                              value={values.name}
                              variant="outlined"
                            />
                          </Grid>
                        </Grid>

                        <br />
                        <InputLabel
                          htmlFor="name"
                          className={classes.inputLabel}
                        >
                          Thông tin chi tiết:
                        </InputLabel>
                        <br />
                        <Accordion
                          expanded={expanded === 'panel1'}
                          onChange={handleTabChange('panel1')}
                        >
                          <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1bh-content"
                            id="panel1bh-header"
                          >
                            <Typography className={classes.heading}>
                              Định danh kết quả sản xuất
                            </Typography>
                          </AccordionSummary>
                          <AccordionDetails>
                            <Grid container spacing={3}>
                              <Grid item md={4} xs={12}>
                                <InputLabel
                                  htmlFor="name"
                                  className={classes.inputLabel}
                                >
                                  Mã lệnh sản xuất :
                                </InputLabel>
                                <TextField
                                  error={Boolean(touched.wo && errors.wo)}
                                  fullWidth
                                  helperText={touched.wo && errors.wo}
                                  margin="normal"
                                  name="wo"
                                  onBlur={handleBlur}
                                  onChange={handleChange}
                                  value={values.wo}
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid item md={4} xs={12}>
                                <InputLabel
                                  htmlFor="name"
                                  className={classes.inputLabel}
                                >
                                  Tên ca sản xuất:
                                </InputLabel>
                                <TextField
                                  error={Boolean(touched.shift && errors.shift)}
                                  fullWidth
                                  helperText={touched.shift && errors.shift}
                                  margin="normal"
                                  name="shift"
                                  onBlur={handleBlur}
                                  onChange={handleChange}
                                  value={values.shift}
                                  variant="outlined"
                                />
                              </Grid>
                              <Grid item md={4} xs={12}>
                                <InputLabel
                                  htmlFor="name"
                                  className={classes.inputLabel}
                                >
                                  Thời gian sản xuất * :
                                </InputLabel>
                                <FormControl
                                  variant="outlined"
                                  className={classes.formControl}
                                >
                                  <TextField
                                    id="datetime-local"
                                    error={Boolean(
                                      touched.date_time && errors.date_time
                                    )}
                                    helperText={
                                      touched.date_time && errors.date_time
                                    }
                                    type="datetime-local"
                                    name={'date_time'}
                                    variant={'outlined'}
                                    defaultValue={
                                      moment(new Date())
                                        .format()
                                        .split('+')[0]
                                    }
                                    className={classes.textField}
                                    InputLabelProps={{
                                      shrink: true
                                    }}
                                  />
                                </FormControl>
                              </Grid>

                              <Grid
                                container
                                spacing={3}
                                style={{ padding: 10 }}
                              >
                                <Grid item md={4} xs={12}>
                                  <InputLabel htmlFor="stationId">
                                    Chọn công đoạn sản xuất *:
                                  </InputLabel>
                                  <FormControl
                                    variant="outlined"
                                    className={classes.formControl}
                                  >
                                    {allStation && (
                                      <AutoCompleteMulti
                                        isMulti={false}
                                        displayAttributeName={'name'}
                                        options={allStation}
                                        isError={Boolean(
                                          touched.stationId && errors.stationId
                                        )}
                                        helperText={
                                          touched.stationId && errors.stationId
                                        }
                                        placeHoldInput={
                                          'Nhập công đoạn sản xuất'
                                        }
                                        defaultValue={
                                          allStation?.filter(
                                            item => item.id === values.stationId
                                          )[0]
                                        }
                                        inputName={'station_id'}
                                        handleSendValue={value => {
                                          setFieldValue('station_id', value);
                                          // setStation_id_choosed(value);
                                        }}
                                      />
                                    )}
                                  </FormControl>
                                </Grid>
                                <Grid item md={4} xs={12}>
                                  <InputLabel htmlFor="productId">
                                    Tên sản phẩm *:
                                  </InputLabel>
                                  <FormControl
                                    variant="outlined"
                                    className={classes.formControl}
                                  >
                                    {allProduct && (
                                      <AutoCompleteMulti
                                        isMulti={false}
                                        displayAttributeName={'name'}
                                        isError={Boolean(
                                          touched.productId && errors.productId
                                        )}
                                        helperText={
                                          touched.productId && errors.productId
                                        }
                                        onBlurFormik={() =>
                                          setTouched('product_id', true)
                                        }
                                        options={allProduct}
                                        placeHoldInput={'Nhập sản phẩm'}
                                        defaultValue={
                                          allProduct?.filter(
                                            item => item.id === values.productId
                                          )[0]
                                        }
                                        handleSendValue={value => {
                                          setFieldValue('product_id', value);
                                        }}
                                      />
                                    )}
                                  </FormControl>
                                </Grid>
                                <Grid item md={4} xs={12}>
                                  <InputLabel
                                    htmlFor="name"
                                    // className={classes.inputLabel}
                                  >
                                    Thời gian tạo sản phẩm:
                                  </InputLabel>
                                  <TextField
                                    InputProps={{
                                      endAdornment: (
                                        <InputAdornment position="start">
                                          Giờ
                                        </InputAdornment>
                                      )
                                    }}
                                    error={Boolean(
                                      touched.cycle_time && errors.cycle_time
                                    )}
                                    fullWidth
                                    helperText={
                                      touched.cycle_time && errors.cycle_time
                                    }
                                    margin="normal"
                                    name="cycle_time"
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values.cycle_time}
                                    variant="outlined"
                                  />
                                </Grid>
                              </Grid>
                            </Grid>
                          </AccordionDetails>
                        </Accordion>
                        {/*<Accordion*/}
                        {/*  expanded={expanded === 'panel2'}*/}
                        {/*  onChange={handleTabChange('panel2')}*/}
                        {/*>*/}
                        {/*  <AccordionSummary*/}
                        {/*    expandIcon={<ExpandMoreIcon />}*/}
                        {/*    aria-controls="panel1bh-content"*/}
                        {/*    id="panel1bh-header"*/}
                        {/*  >*/}
                        {/*    <Typography className={classes.heading}>*/}
                        {/*      Kế hoạch sản xuất*/}
                        {/*    </Typography>*/}
                        {/*    /!*<Typography className={classes.secondaryHeading}>*!/*/}
                        {/*    /!*  Số lượng công nhân sản xuất trực tiếp , gián tiếp*!/*/}
                        {/*    /!*  , Tổng số lượng , Số nhân công kế hoạch , thực tế*!/*/}
                        {/*    /!*  , ...*!/*/}
                        {/*    /!*</Typography>*!/*/}
                        {/*  </AccordionSummary>*/}
                        {/*  <AccordionDetails>*/}
                        {/*    <Grid container spacing={3}>*/}
                        {/*      <Grid item md={4} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Số lượng công nhân sản xuất trực tiếp:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.man_asm && errors.man_asm*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={touched.man_asm && errors.man_asm}*/}
                        {/*          margin="normal"*/}
                        {/*          name="man_asm"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.man_asm}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={4} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Số lượng công nhân hỗ trợ sản xuất trực tiếp:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.man_sub_asm && errors.man_sub_asm*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.man_sub_asm && errors.man_sub_asm*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="man_sub_asm"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.man_sub_asm}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={4} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Số lượng công nhân sản xuất gián tiếp:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.man_indirect && errors.man_indirect*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.man_indirect && errors.man_indirect*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="man_indirect"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.man_indirect}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={4} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Số lượng công nhân sản xuất sửa chữa:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.man_repair && errors.man_repair*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.man_repair && errors.man_repair*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="man_repair"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.man_repair}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={4} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Số lượng công nhân nghỉ:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.man_off && errors.man_off*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={touched.man_off && errors.man_off}*/}
                        {/*          margin="normal"*/}
                        {/*          name="man_of"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.man_off}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={4} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Tổng số lượng công nhân:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          fullWidth*/}
                        {/*          style={{ backgroundColor: '#ebfdfe' }}*/}
                        {/*          margin="normal"*/}
                        {/*          name="man_total"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={*/}
                        {/*            values.man_asm +*/}
                        {/*            values.man_sub_asm +*/}
                        {/*            values.man_indirect +*/}
                        {/*            values.man_repair +*/}
                        {/*            values.man_off*/}
                        {/*          }*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={4} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Tổng số lượng nhân lực trực tiếp tạo sản phẩm:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.man_h_total && errors.man_h_total*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.man_h_total && errors.man_h_total*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="man_h_total"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.man_h_total}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={4} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Tổng sản lượng cho lệnh sản xuất:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.wo_qty && errors.wo_qty*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={touched.wo_qty && errors.wo_qty}*/}
                        {/*          margin="normal"*/}
                        {/*          name="wo_qty"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.wo_qty}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={4} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Sản lượng theo kế hoạch:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.planning_qty && errors.planning_qty*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.planning_qty && errors.planning_qty*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="planning_qty"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.planning_qty}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={4} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Nhân công kế hoạch:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.planning_man && errors.planning_man*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.planning_man && errors.planning_man*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="planning_man"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.planning_man}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={4} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Nhân lực kế hoạch:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.planning_man_h &&*/}
                        {/*              errors.planning_man_h*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.planning_man_h &&*/}
                        {/*            errors.planning_man_h*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="planning_man_h"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.planning_man_h}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*    </Grid>*/}
                        {/*  </AccordionDetails>*/}
                        {/*</Accordion>*/}
                        {/*<Accordion*/}
                        {/*  expanded={expanded === 'panel3'}*/}
                        {/*  onChange={handleTabChange('panel3')}*/}
                        {/*>*/}
                        {/*  <AccordionSummary*/}
                        {/*    expandIcon={<ExpandMoreIcon />}*/}
                        {/*    aria-controls="panel1bh-content"*/}
                        {/*    id="panel1bh-header"*/}
                        {/*  >*/}
                        {/*    <Typography className={classes.heading}>*/}
                        {/*      Kết quả sản xuất*/}
                        {/*    </Typography>*/}
                        {/*    /!*<Typography className={classes.secondaryHeading}>*!/*/}
                        {/*    /!*  Sản lượng đã hoàn thành cho lệnh sản xuất , tỉ lệ*!/*/}
                        {/*    /!*  hoàn thành lệnh , Sản lượng thực tế , Thời gian*!/*/}
                        {/*    /!*  chậm trễ , ...*!/*/}
                        {/*    /!*</Typography>*!/*/}
                        {/*  </AccordionSummary>*/}
                        {/*  <AccordionDetails>*/}
                        {/*    <Grid container spacing={3}>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Sản lượng đã hoàn thành cho lệnh sản xuất :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.wo_complete && errors.wo_complete*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.wo_complete && errors.wo_complete*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="wo_complete"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.wo_complete}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Tỉ lệ hoàn thành lệnh sản xuất :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          fullWidth*/}
                        {/*          InputProps={{*/}
                        {/*            endAdornment: (*/}
                        {/*              <InputAdornment position="start">*/}
                        {/*                %*/}
                        {/*              </InputAdornment>*/}
                        {/*            )*/}
                        {/*          }}*/}
                        {/*          helperText={touched.wo_rate && errors.wo_rate}*/}
                        {/*          margin="normal"*/}
                        {/*          name="wo_rate"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.wo_rate}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Sản lượng thực tế :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.actual_qty && errors.actual_qty*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.actual_qty && errors.actual_qty*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="actual_qty"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.actual_qty}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Nhân lực thực tế sử dụng :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.actual_man_h && errors.actual_man_h*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.actual_man_h && errors.actual_man_h*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="actual_man_h"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.actual_man_h}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Thời gian dừng sản xuất do dây chuyền SMT :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.down_smt && errors.down_smt*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.down_smt && errors.down_smt*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="down_smt"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.down_smt}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Lí do dừng line SMT :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.reason_smt && errors.reason_smt*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.reason_smt && errors.reason_smt*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="reason_smt"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.reason_smt}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Thời gian dừng sản xuất do lỗi trạm :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.down_station && errors.down_station*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.down_station && errors.down_station*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="down_station"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.down_station}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Lí do dừng trạm :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.reason_station &&*/}
                        {/*              errors.reason_station*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.reason_station &&*/}
                        {/*            errors.reason_station*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="reason_station"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.reason_station}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Thời gian dừng sản xuất do bảo trì , bảo dưỡng*/}
                        {/*          :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.down_maintain &&*/}
                        {/*              errors.down_maintain*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.down_maintain &&*/}
                        {/*            errors.down_maintain*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="down_maintain"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.down_maintain}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Lí do bảo trì bảo dưỡng:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.reason_maintain &&*/}
                        {/*              errors.reason_maintain*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.reason_maintain &&*/}
                        {/*            errors.reason_maintain*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="reason_maintain"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.reason_maintain}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Thời gian dừng sản xuất do vấn đề vật tư:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.down_material &&*/}
                        {/*              errors.down_material*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.down_material &&*/}
                        {/*            errors.down_material*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="down_material"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.down_material}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Lí do dừng do vật tư :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.reason_material &&*/}
                        {/*              errors.reason_material*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.reason_material &&*/}
                        {/*            errors.reason_material*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="reason_material"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.reason_material}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Thời gian dừng sản xuất do phân bổ , sắp xếp*/}
                        {/*          nhân lực :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.down_man && errors.down_man*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.down_man && errors.down_man*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="down_man"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.down_man}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Lí do dừng nhân công:*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.reason_man && errors.reason_man*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.reason_man && errors.reason_man*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="reason_man"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.reason_man}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Tổng thời gian dừng sản xuất :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          fullWidth*/}
                        {/*          helperText={*/}
                        {/*            touched.down_total && errors.down_total*/}
                        {/*          }*/}
                        {/*          style={{ backgroundColor: '#ebfdfe' }}*/}
                        {/*          margin="normal"*/}
                        {/*          name="name"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={*/}
                        {/*            values.down_smt +*/}
                        {/*            values.down_station +*/}
                        {/*            values.down_maintain +*/}
                        {/*            values.down_material +*/}
                        {/*            values.down_man*/}
                        {/*          }*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Tỉ lệ sản phẩm tốt :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.quality_rate && errors.quality_rate*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          InputProps={{*/}
                        {/*            endAdornment: (*/}
                        {/*              <InputAdornment position="start">*/}
                        {/*                %*/}
                        {/*              </InputAdornment>*/}
                        {/*            )*/}
                        {/*          }}*/}
                        {/*          helperText={*/}
                        {/*            touched.quality_rate && errors.quality_rate*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="quality_rate"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.quality_rate}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Tỉ lệ sản phẩm tốt mục tiêu :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          error={Boolean(*/}
                        {/*            touched.quality_rate_target &&*/}
                        {/*              errors.quality_rate_target*/}
                        {/*          )}*/}
                        {/*          fullWidth*/}
                        {/*          InputProps={{*/}
                        {/*            endAdornment: (*/}
                        {/*              <InputAdornment position="start">*/}
                        {/*                %*/}
                        {/*              </InputAdornment>*/}
                        {/*            )*/}
                        {/*          }}*/}
                        {/*          helperText={*/}
                        {/*            touched.quality_rate_target &&*/}
                        {/*            errors.quality_rate_target*/}
                        {/*          }*/}
                        {/*          margin="normal"*/}
                        {/*          name="quality_rate_target"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.quality_rate_target}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Tỉ lệ sử dụng nguồn lực :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          fullWidth*/}
                        {/*          style={{ backgroundColor: '#ebfdfe' }}*/}
                        {/*          margin="normal"*/}
                        {/*          InputProps={{*/}
                        {/*            endAdornment: (*/}
                        {/*              <InputAdornment position="start">*/}
                        {/*                %*/}
                        {/*              </InputAdornment>*/}
                        {/*            )*/}
                        {/*          }}*/}
                        {/*          name="availability"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={*/}
                        {/*            (values.man_h_total - values.down_total) /*/}
                        {/*            values.man_h_total*/}
                        {/*          }*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Hiệu suất sử dụng nguồn lực :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          fullWidth*/}
                        {/*          style={{ backgroundColor: '#ebfdfe' }}*/}
                        {/*          margin="normal"*/}
                        {/*          InputProps={{*/}
                        {/*            endAdornment: (*/}
                        {/*              <InputAdornment position="start">*/}
                        {/*                %*/}
                        {/*              </InputAdornment>*/}
                        {/*            )*/}
                        {/*          }}*/}
                        {/*          name="performance"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.name}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Tổng hiệu quả sử dụng nguồn lực :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          style={{ backgroundColor: '#ebfdfe' }}*/}
                        {/*          fullWidth*/}
                        {/*          margin="normal"*/}
                        {/*          name="name"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.name}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*      <Grid item md={3} xs={12}>*/}
                        {/*        <InputLabel*/}
                        {/*          htmlFor="name"*/}
                        {/*          className={classes.inputLabel}*/}
                        {/*        >*/}
                        {/*          Tổng số lượng nhân lực thực tế theo tính toán*/}
                        {/*          :*/}
                        {/*        </InputLabel>*/}
                        {/*        <TextField*/}
                        {/*          fullWidth*/}
                        {/*          style={{ backgroundColor: '#ebfdfe' }}*/}
                        {/*          margin="normal"*/}
                        {/*          name="actual_man_h_lt"*/}
                        {/*          onBlur={handleBlur}*/}
                        {/*          onChange={handleChange}*/}
                        {/*          value={values.cycle_time * values.actual_qty}*/}
                        {/*          variant="outlined"*/}
                        {/*        />*/}
                        {/*      </Grid>*/}
                        {/*    </Grid>*/}
                        {/*  </AccordionDetails>*/}
                        {/*</Accordion>*/}

                        <CustomErrorMessage content={err} />
                        <Box my={2} mt={5}>
                          <div className={classes.groupButtonSubmit}>
                            {Boolean(typeAction !== ACTION_TABLE.PREVIEW) && (
                              <div className="left-button">
                                <div className={classes.wrapper}>
                                  <Button
                                    className={classes.styleInputSearch}
                                    style={{ marginRight: '10px' }}
                                    color="primary"
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                  >
                                    {typeAction === ACTION_TABLE.CREATE
                                      ? 'Tạo mới'
                                      : 'Cập nhật'}
                                  </Button>
                                  {(statusUpdateManuFacturing ===
                                    STATUS_API.PENDING ||
                                    statusUpdateManuFacturing ===
                                      STATUS_API.PENDING) && (
                                    <LoadingInButton />
                                  )}
                                </div>
                                <Button
                                  size="large"
                                  variant="contained"
                                  onClick={handleClose}
                                >
                                  Thoát
                                </Button>
                              </div>
                            )}
                          </div>
                        </Box>
                      </form>
                    )}
                  </Formik>
                </CardContent>
                <Divider />
              </Card>
            </Grid>
          </Grid>
        </Container>
      </Dialog>
      {(statusCreateManuFacturing === STATUS_API.SUCCESS ||
        statusCreateManuFacturing === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusCreateManuFacturing === STATUS_API.SUCCESS
                ? MESSAGE.CREATE_STATION_SUCCESS
                : MESSAGE.CREATE_STATION_FAIL
            }
            type={
              statusCreateManuFacturing === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
      {(statusUpdateManuFacturing === STATUS_API.SUCCESS ||
        statusUpdateManuFacturing === STATUS_API.ERROR) &&
        isSubmitted && (
          <ToastMessage
            callBack={() => dispatch(resetStatus())}
            message={
              statusUpdateManuFacturing === STATUS_API.SUCCESS
                ? MESSAGE.UPDATE_STATION_SUCCESS
                : MESSAGE.UPDATE_STATION_FAIL
            }
            type={
              statusUpdateManuFacturing === STATUS_API.SUCCESS
                ? MESSAGE_TYPE.success
                : MESSAGE_TYPE.error
            }
          />
        )}
    </div>
  );
}

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative'
  },
  shadowBox: {
    boxShadow: '0 2px 5px rgba(0,0,0,.18)'
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  inputLabel: {
    height: 30
  },
  root: {
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  headerDivider: {
    backgroundColor: '#e8e7e7',
    padding: theme.spacing(1),
    marginBottom: '15px'
  },
  heading: {
    fontSize: theme.typography.pxToRem(17),
    fontWeight: 500,
    flexBasis: '70%',
    flexShrink: 0
  },
  formControl: {
    marginTop: theme.spacing(2),
    width: '100%'
  },
  inputAutoGen: {
    backgroundColor: '#cffbfe'
  },
  groupButtonSubmit: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: '15px',

    '& .left-button': {
      display: 'flex'
    }
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  wrapper: {
    position: 'relative'
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  },
  disableForm: {
    pointerEvents: 'none'
  },
  colorWhite: {
    color: '#fff'
  },
  tabHeader: {
    backgroundColor: 'aliceblue'
  }
}));

export default DetailsManuFacturingIndentify;
