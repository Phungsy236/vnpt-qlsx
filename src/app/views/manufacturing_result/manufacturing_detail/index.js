import {
  AppBar,
  Card,
  Container,
  Dialog,
  Grid,
  makeStyles,
  Slide
} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import VerticalLinearStepper from 'src/app/components/HistoryStepper';
import ManufacturingDetail from 'src/app/components/ManufacturingDetail';
import { MANUFACTURING_TYPE } from 'src/app/constant/config';
import theme from 'src/app/theme';
import PopupLogs from 'src/app/views/manufacturing_result/manufacturing_detail/PopupLogs';
import { getFullManufacturing } from 'src/features/manufacturingSlice';
import { getAllProduct } from 'src/features/productSlice';
import { getAllStation } from 'src/features/stationSlice';
import ToolBarEdit from './ToolBar.js';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function DetailsManuFacturing({ open, sendData, closeRef }) {
  const classes = useStyles();
  const dispatch = useDispatch();

  const handleClose = isSaved => {
    if (!closeRef) return;
    closeRef();
  };

  useEffect(() => {
    if (allStation && allStation.length > 0) return;
    else dispatch(getAllStation());
  }, [dispatch]);

  useEffect(() => {
    if (allStation && allProduct.length > 0) return;
    else dispatch(getAllProduct());
  }, [dispatch]);

  const allStation = useSelector(state => state.stationSlice.allStation);
  const allProduct = useSelector(state => state.productSlice.allProduct);

  // const statusGetFullDetail = useSelector(
  //   state => state.manufacturingSlice.statusGetFullDetail
  // );
  const logs = useSelector(state => state.manufacturingSlice.logs);
  useEffect(() => {
    dispatch(getFullManufacturing(sendData.data.id));
  }, [dispatch]);
  const [isShowDetailLog, setShowDetailLog] = useState(false);
  const [logDetail, setLogDetail] = useState(null);

  function handleViewLog(data) {
    setLogDetail(data);
    setShowDetailLog(true);
  }
  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={() => handleClose()}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <ToolBarEdit
            closeToolbarRef={handleClose}
            title={sendData.data.name}
          />
        </AppBar>

        <Container maxWidth="lg">
          <Grid container spacing={3}>
            <Grid
              item
              lg={12}
              md={12}
              xs={12}
              style={{
                paddingTop: '100px',
                pointerEvents: ''
              }}
            >
              <ManufacturingDetail
                data={sendData.data}
                allStation={allStation}
                allProduct={allProduct}
              />
              {logs && Boolean(logs?.length) && (
                <Card className={classes.historyWrapper}>
                  <Typography variant={'h4'}> Lịch sử</Typography>
                  <VerticalLinearStepper
                    listItem={logs}
                    actionDetailLog={handleViewLog}
                  />
                </Card>
              )}
            </Grid>
          </Grid>
          {isShowDetailLog && (
            <PopupLogs
              open={isShowDetailLog}
              data={{
                logData: logDetail,
                allProduct: allProduct,
                allStation: allStation
              }}
              closeRef={() => setShowDetailLog(false)}
            />
          )}
        </Container>
      </Dialog>
    </div>
  );
}

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'fixed'
  },
  shadowBox: {
    boxShadow: '0 2px 5px rgba(0,0,0,.18)'
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  root: {
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  headerDivider: {
    backgroundColor: '#e8e7e7',
    padding: theme.spacing(1),
    marginBottom: '15px'
  },
  heading: {
    fontSize: theme.typography.pxToRem(17),
    fontWeight: 500,
    flexBasis: '70%',
    flexShrink: 0
  },
  historyWrapper: {
    marginTop: theme.spacing(2),
    padding: 10
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  wrapper: {
    position: 'relative'
  },
  colorWhite: {
    color: '#fff'
  },
  tabHeader: {
    backgroundColor: 'aliceblue'
  }
}));

export default DetailsManuFacturing;
