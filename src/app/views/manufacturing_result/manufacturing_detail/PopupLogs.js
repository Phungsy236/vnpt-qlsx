import {
  AppBar,
  Container,
  Dialog,
  makeStyles,
  Slide
} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';
import PropTypes from 'prop-types';
import ManufacturingDetail from 'src/app/components/ManufacturingDetail';
import ToolBarEdit from 'src/app/views/manufacturing_result/manufacturing_detail/ToolBar';

PopupLogs.propTypes = {};
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="left" ref={ref} {...props} />;
});
function PopupLogs({ open, closeRef, data }) {
  const classes = useStyles();
  const handleClose = () => {
    if (!closeRef) return;
    closeRef();
  };
  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={() => handleClose()}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={() => handleClose()}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              {`Lịch sử ${data.logData.name}`}
            </Typography>
          </Toolbar>
        </AppBar>
        <Container maxWidth="lg">
          <Grid container spacing={3}>
            <Grid
              item
              lg={12}
              md={12}
              xs={12}
              style={{
                paddingTop: '100px',
                pointerEvents: ''
              }}
            >
              <ManufacturingDetail
                data={data.logData}
                allStation={data.allStation}
                allProduct={data.allProduct}
              />
            </Grid>
          </Grid>
        </Container>
      </Dialog>
    </div>
  );
}
const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'fixed'
  },
  shadowBox: {
    boxShadow: '0 2px 5px rgba(0,0,0,.18)'
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  root: {
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  headerDivider: {
    backgroundColor: '#e8e7e7',
    padding: theme.spacing(1),
    marginBottom: '15px'
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  wrapper: {
    position: 'relative'
  },
  colorWhite: {
    color: '#fff'
  },
  tabHeader: {
    backgroundColor: 'aliceblue'
  }
}));
export default PopupLogs;
