import { Container, Grid, makeStyles } from '@material-ui/core';
import React from 'react';
import Page from 'src/app/components/Page';
import TotalProduct from 'src/app/views/dashboard/TotalProduct';
import LatestManufactory from './LatestManufactory';
import LatestProducts from './LatestProducts';
import TotalUser from './TotalCustomers';
import TotalManuafactory from './TotalManuafactory';
import TotalStation from './TotalStation';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const Dashboard = () => {
  const classes = useStyles();

  return (
    <Page className={classes.root} title="Dashboard">
      <Container maxWidth={false}>
        <Grid container spacing={3}>
          <Grid item lg={3} sm={3} xl={3} xs={12}>
            <TotalUser />
          </Grid>
          <Grid item lg={3} sm={3} xl={3} xs={12}>
            <TotalStation />
          </Grid>
          <Grid item lg={3} sm={3} xl={3} xs={12}>
            <TotalProduct />
          </Grid>
          <Grid item lg={3} sm={3} xl={3} xs={12}>
            <TotalManuafactory />
          </Grid>

          {/*============++Table ======================*/}
          <Grid item lg={4} md={6} xl={3} xs={12}>
            <LatestProducts />
          </Grid>
          <Grid item lg={8} md={12} xl={9} xs={12}>
            <LatestManufactory />
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
};

export default Dashboard;
