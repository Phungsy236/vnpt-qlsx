import {
  Avatar,
  Card,
  CardContent,
  colors,
  Grid,
  makeStyles,
  Typography
} from '@material-ui/core';
import PeopleIcon from '@material-ui/icons/PeopleOutlined';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { getTotalUser } from 'src/features/userSlice';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    cursor: 'pointer'
  },
  avatar: {
    backgroundColor: colors.green[600],
    height: 56,
    width: 56
  },
  differenceIcon: {
    color: colors.green[600]
  },
  differenceValue: {
    color: colors.green[600],
    marginRight: theme.spacing(1)
  }
}));

const TotalUser = ({ className, ...rest }) => {
  const classes = useStyles();
  let navigate = useNavigate();
  const dispatch = useDispatch();
  const totalUser = useSelector(state => state.userSlice.totalUser);
  const statusGetTotal = useSelector(state => state.userSlice.statusGetTotal);
  useEffect(() => {
    if (!totalUser) dispatch(getTotalUser());
  }, [dispatch]);
  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
      onClick={() => navigate('/vnpt/user/all')}
    >
      <CardContent>
        <Grid container justify="space-between" spacing={3}>
          <Grid item>
            <Typography color="textSecondary" gutterBottom variant="h6">
              Số người dùng
            </Typography>
            <Typography color="textPrimary" variant="h3">
              {totalUser} Người Dùng
            </Typography>
          </Grid>
          <Grid item>
            <Avatar className={classes.avatar}>
              <PeopleIcon />
            </Avatar>
          </Grid>
        </Grid>
        {/*<Box mt={2} display="flex" alignItems="center">*/}
        {/*  <ArrowUpwardIcon className={classes.differenceIcon} />*/}
        {/*  <Typography className={classes.differenceValue} variant="body2">*/}
        {/*    16%*/}
        {/*  </Typography>*/}
        {/*  <Typography color="textSecondary" variant="caption">*/}
        {/*    Since last month*/}
        {/*  </Typography>*/}
        {/*</Box>*/}
      </CardContent>
    </Card>
  );
};

TotalUser.propTypes = {
  className: PropTypes.string
};

export default TotalUser;
