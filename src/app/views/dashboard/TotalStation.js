import {
  Avatar,
  Card,
  CardContent,
  colors,
  Grid,
  makeStyles,
  Typography
} from '@material-ui/core';
import { TimelineOutlined } from '@material-ui/icons';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { Loader } from 'react-feather';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { STATUS_API } from 'src/app/constant/config';
import { getTotalStation } from 'src/features/stationSlice';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    cursor: 'pointer'
  },
  avatar: {
    backgroundColor: colors.red[600],
    height: 56,
    width: 56
  },
  differenceIcon: {
    color: colors.green[600]
  },
  differenceValue: {
    color: colors.green[600],
    marginRight: theme.spacing(1)
  }
}));

const TotalStation = ({ className, ...rest }) => {
  const classes = useStyles();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const totalStation = useSelector(state => state.stationSlice.totalStation);
  const statusGetTotal = useSelector(
    state => state.productSlice.statusGetTotal
  );
  useEffect(() => {
    if (!totalStation) dispatch(getTotalStation());
  }, [dispatch]);

  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
      onClick={() => navigate('/vnpt/station')}
    >
      <CardContent>
        <Grid container justify="space-between" spacing={3}>
          <Grid item>
            <Typography color="textSecondary" gutterBottom variant="h6">
              Số công đoạn sản xuất
            </Typography>
            <Typography color="textPrimary" variant="h3">
              {statusGetTotal === STATUS_API.PENDING ? (
                <Loader />
              ) : (
                totalStation
              )}{' '}
              Công Đoạn
            </Typography>
          </Grid>
          <Grid item>
            <Avatar className={classes.avatar}>
              <TimelineOutlined />
            </Avatar>
          </Grid>
        </Grid>
        {/*<Box mt={2} display="flex" alignItems="center">*/}
        {/*  <ArrowUpward className={classes.differenceIcon} />*/}
        {/*  <Typography className={classes.differenceValue} variant="body2">*/}
        {/*    12%*/}
        {/*  </Typography>*/}
        {/*  <Typography color="textSecondary" variant="caption">*/}
        {/*    So với tháng trước*/}
        {/*  </Typography>*/}
        {/*</Box>*/}
      </CardContent>
    </Card>
  );
};

TotalStation.propTypes = {
  className: PropTypes.string
};

export default TotalStation;
