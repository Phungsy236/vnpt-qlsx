import {
  Avatar,
  Card,
  CardContent,
  colors,
  Grid,
  makeStyles,
  Typography
} from '@material-ui/core';
import { MemoryOutlined } from '@material-ui/icons';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { Loader } from 'react-feather';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { STATUS_API } from 'src/app/constant/config';
import { getTotalProduct } from 'src/features/productSlice';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    cursor: 'pointer'
  },
  avatar: {
    backgroundColor: colors.blue[600],
    height: 56,
    width: 56
  },
  differenceIcon: {
    color: colors.green[600]
  },
  differenceValue: {
    color: colors.green[600],
    marginRight: theme.spacing(1)
  }
}));

const TotalProduct = ({ className, ...rest }) => {
  const classes = useStyles();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const totalProduct = useSelector(state => state.productSlice.totalProduct);
  const statusGetTotal = useSelector(
    state => state.productSlice.statusGetTotal
  );
  useEffect(() => {
    if (!totalProduct) dispatch(getTotalProduct());
  }, [dispatch]);
  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
      onClick={() => navigate('/vnpt/product')}
    >
      <CardContent>
        <Grid container justify="space-between" spacing={3}>
          <Grid item>
            <Typography color="textSecondary" gutterBottom variant="h6">
              Số sản phẩm
            </Typography>
            <Typography color="textPrimary" variant="h3">
              {statusGetTotal === STATUS_API.PENDING ? (
                <Loader />
              ) : (
                totalProduct
              )}{' '}
              Sản phẩm
            </Typography>
          </Grid>
          <Grid item>
            <Avatar className={classes.avatar}>
              <MemoryOutlined />
            </Avatar>
          </Grid>
        </Grid>
        {/*<Box mt={2} display="flex" alignItems="center">*/}
        {/*  <ArrowUpward className={classes.differenceIcon} />*/}
        {/*  <Typography className={classes.differenceValue} variant="body2">*/}
        {/*    12%*/}
        {/*  </Typography>*/}
        {/*  <Typography color="textSecondary" variant="caption">*/}
        {/*    So với tháng trước*/}
        {/*  </Typography>*/}
        {/*</Box>*/}
      </CardContent>
    </Card>
  );
};

TotalProduct.propTypes = {
  className: PropTypes.string
};

export default TotalProduct;
