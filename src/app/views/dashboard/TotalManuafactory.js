import {
  Avatar,
  Card,
  CardContent,
  colors,
  Grid,
  makeStyles,
  Typography
} from '@material-ui/core';
import { LibraryBooksOutlined } from '@material-ui/icons';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { Loader } from 'react-feather';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { STATUS_API } from 'src/app/constant/config';
import { getTotalManufacturing } from 'src/features/manufacturingSlice';

const useStyles = makeStyles(() => ({
  root: {
    height: '100%',
    cursor: 'pointer'
  },
  avatar: {
    backgroundColor: colors.orange[600],
    height: 56,
    width: 56
  }
}));

const TotalManuafactory = ({ className, ...rest }) => {
  const classes = useStyles();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const totalManufacturing = useSelector(
    state => state.manufacturingSlice.totalManufacturing
  );
  const statusGetTotal = useSelector(
    state => state.manufacturingSlice.statusGetTotal
  );
  useEffect(() => {
    if (!totalManufacturing) dispatch(getTotalManufacturing());
  }, [dispatch]);
  return (
    <Card
      className={clsx(classes.root, className)}
      {...rest}
      onClick={() => navigate('/vnpt/manufacturing')}
    >
      <CardContent>
        <Grid container justify="space-between" spacing={3}>
          <Grid item>
            <Typography color="textSecondary" gutterBottom variant="h6">
              Số bản ghi
            </Typography>
            <Typography color="textPrimary" variant="h3">
              {statusGetTotal === STATUS_API.PENDING ? (
                <Loader />
              ) : (
                totalManufacturing
              )}{' '}
              Kết quả
            </Typography>
          </Grid>
          <Grid item>
            <Avatar className={classes.avatar}>
              <LibraryBooksOutlined />
            </Avatar>
          </Grid>
        </Grid>
        {/*<Box mt={3}>*/}
        {/*  <LinearProgress value={75.5} variant="determinate" />*/}
        {/*</Box>*/}
      </CardContent>
    </Card>
  );
};

TotalManuafactory.propTypes = {
  className: PropTypes.string
};

export default TotalManuafactory;
