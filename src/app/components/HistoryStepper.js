import { PropTypes } from '@material-ui/core';
import { Alarm } from '@material-ui/icons';
import moment from 'moment/moment';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Typography from '@material-ui/core/Typography';
const useStyles = makeStyles(theme => ({
  root: {
    width: '100%'
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  actionsContainer: {
    marginBottom: theme.spacing(2)
  },
  resetContainer: {
    padding: theme.spacing(3)
  },
  wrapper: {
    cursor: 'pointer'
  }
}));

export default function VerticalLinearStepper({ listItem, actionDetailLog }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Stepper
        orientation="vertical"
        activeStep={true}
        className={classes.wrapper}
      >
        {listItem.map(item => (
          <Step
            key={item.id}
            active={true}
            className={classes.wrapper}
            onClick={() => actionDetailLog(item)}
          >
            <StepLabel StepIconComponent={Alarm}>{item.name}</StepLabel>
            <StepContent>
              <Typography>{moment(item.updated).format('LLL')}</Typography>
            </StepContent>
          </Step>
        ))}
      </Stepper>
    </div>
  );
}
