import React from 'react';
import { CircularProgress, makeStyles } from '@material-ui/core';
import { green } from '@material-ui/core/colors';

LoadingInButton.propTypes = {};

function LoadingInButton(props) {
  const classes = useStyle();
  return <CircularProgress size={24} className={classes.buttonProgress} />;
}
const useStyle = makeStyles({
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  }
});
export default LoadingInButton;
