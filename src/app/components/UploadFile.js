import {
  Box,
  Card,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  InputLabel,
  Slide,
  TextField
} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { CloudUploadOutlined, Error } from '@material-ui/icons';
import { Skeleton } from '@material-ui/lab';
import React, { useRef } from 'react';
import CustomErrorMsg from 'src/app/components/CustomErrorMsg';
import { MSG_TIMEOUT_REQUEST, TIMEOUT_DEFAULT } from 'src/app/constant/config';
import theme from 'src/app/theme';
import PropTypes from 'prop-types';
import Axios from 'axios';

UploadFile.propTypes = {
  defaultImg: PropTypes.string,
  getter: PropTypes.func
};
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
function UploadFile({ defaultImg, getter }) {
  const [url, setUrl] = React.useState(
    defaultImg || '/static/images/default-img.png'
  );

  const [file, setFile] = React.useState();
  const [previewUrl, setPreviewUrl] = React.useState('');
  const [inputValue, setInputValue] = React.useState('');
  const [typeUpload, setTypeUpload] = React.useState();
  const [isOpenModal, setOpenModal] = React.useState(false);
  const [errorLinkUrl, setErrorLinkUrl] = React.useState('');
  const [showLoading, setLoading] = React.useState(false);
  const [errorUpload, setErrorUpload] = React.useState('');
  const inputUpload = useRef();
  const inputUrl = useRef();

  const handleDisplayPreviewLocal = () => {
    let reader = new FileReader();
    setTypeUpload(2);
    let file = inputUpload.current.files[0];
    if (file) {
      setFile(file);
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        setInputValue('');
        setPreviewUrl(reader.result);
      };
    }
  };
  function handleDisplayPreviewUrl(event) {
    setTypeUpload(1);
    setErrorUpload('');
    setErrorLinkUrl('');
    setLoading(true);
    setInputValue(event.target.value);
    setPreviewUrl(event.target.value);
  }
  function handleSave() {
    if (typeUpload === 2) {
      let formData = new FormData();
      formData.append('file', file);
      Axios({
        //TODO replace later
        url: 'https://hiento.xyz/backend/storage',
        method: 'POST',
        header: {
          'content-type': 'form-data'
        },
        timeout: TIMEOUT_DEFAULT,
        data: formData
      })
        .then(res => {
          console.log(res.data);
          // getter(res.data)
        })
        .catch(err => {
          if (err.code === 'ECONNABORTED') setErrorUpload(MSG_TIMEOUT_REQUEST);
        });
    } else {
      setOpenModal(false);
      setUrl(previewUrl);
      // getter(previewUrl);
    }
  }
  return (
    <div>
      <Card
        style={{ padding: theme.spacing(2), height: 'auto', maxWidth: 300 }}
      >
        <img alt="" src={url} style={{ width: '100%' }} />
      </Card>
      <Box
        mt={2}
        display={'flex'}
        justifyContent={'center'}
        style={{ maxWidth: 300 }}
      >
        <Button
          variant={'outlined'}
          size={'small'}
          onClick={() => {
            setOpenModal(!isOpenModal);
          }}
        >
          Chọn File
        </Button>
      </Box>
      {/*<p>Current react URL state: {this.state.url}</p>*/}

      {/*====================================+Dialog pickup ==============================*/}
      <Dialog
        fullWidth={true}
        open={isOpenModal}
        TransitionComponent={Transition}
        onClose={() => setOpenModal(false)}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          Chọn file upload
        </DialogTitle>
        <DialogContent>
          <InputLabel>Dán đường dẫn tới file : </InputLabel>
          <br />
          <TextField
            ref={inputUrl}
            fullWidth
            value={inputValue}
            error={errorLinkUrl.length > 0 && errorLinkUrl}
            helperText={errorLinkUrl}
            name={'url'}
            variant={'outlined'}
            placeholder={'Dán đường dẫn ...'}
            onChange={handleDisplayPreviewUrl}
            size={'small'}
          />
          <Box mt={2} display={'flex'} alignItems={'center'}>
            <InputLabel> Hoặc tải lên từ thiết bị </InputLabel>
            <input
              ref={inputUpload}
              accept="image/*"
              style={{ display: 'none' }}
              onChange={handleDisplayPreviewLocal}
              id="icon-button-file"
              type="file"
            />
            <label htmlFor="icon-button-file">
              <IconButton
                color="primary"
                aria-label="upload picture"
                component="span"
              >
                <CloudUploadOutlined />
              </IconButton>
            </label>
          </Box>
          {previewUrl && (
            <Card
              style={{
                display: 'flex',
                position: 'relative',
                justifyContent: 'center',
                padding: 10,
                width: '100%',
                minHeight: 300
              }}
            >
              {showLoading && (
                <Skeleton
                  animation="wave"
                  variant={'rect'}
                  width={'100%'}
                  height={'100%'}
                  style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    zIndex: 1000,
                    backgroundColor: 'lightgray'
                  }}
                >
                  <div
                    style={{
                      width: '100%',
                      height: '100%'
                    }}
                  />
                </Skeleton>
              )}

              <img
                src={previewUrl}
                alt={'preview'}
                onLoad={() => {
                  console.log('done');
                  setLoading(false);
                }}
                onError={() => {
                  setLoading(false);
                  setErrorLinkUrl('Đường dẫn không đúng');
                }}
                style={{
                  width: '100%',
                  height: 'auto'
                }}
              />
            </Card>
          )}
          <CustomErrorMsg content={errorUpload} />
        </DialogContent>

        <DialogActions>
          <Button onClick={handleSave} variant={'contained'} color="primary">
            Lưu
          </Button>
          <Button
            onClick={() => setOpenModal(false)}
            variant={'outlined'}
            color="primary"
          >
            Hủy
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default UploadFile;
