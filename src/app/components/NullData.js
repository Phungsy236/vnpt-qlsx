import React from 'react';
import SignalCellularConnectedNoInternet0BarIcon from '@material-ui/icons/SignalCellularConnectedNoInternet0Bar';
import Typography from '@material-ui/core/Typography';

function NullData({ icon, title = 'Chưa có dữ liệu' }) {
  return (
    <div
      style={{ width: '100%', padding: 20, textAlign: 'center', fontSize: 28 }}
    >
      {icon || (
        <SignalCellularConnectedNoInternet0BarIcon style={{ fontSize: 50 }} />
      )}
      <Typography variant={'caption'}>{title}</Typography>
    </div>
  );
}

export default NullData;
