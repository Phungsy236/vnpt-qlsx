import Button from '@material-ui/core/Button';
import { DoneAll } from '@material-ui/icons';
import React from 'react';
import PropTypes from 'prop-types';

ToggleBtn.propTypes = {
  title: PropTypes.string.isRequired,
  isChoose: PropTypes.bool,
  value: PropTypes.any,
  getBtnValue: PropTypes.func.isRequired
};

function ToggleBtn({ title, isChoose = false, value, getBtnValue }) {
  const [clicked, setClicked] = React.useState(isChoose);
  function handleClick() {
    setClicked(!clicked);
    getBtnValue(value);
  }
  return (
    <Button
      variant={clicked ? 'contained' : 'outlined'}
      color={clicked ? 'primary' : 'default'}
      size={'small'}
      onClick={handleClick}
      startIcon={clicked ? <DoneAll style={{ color: 'white' }} /> : ''}
      style={{ margin: 4 }}
    >
      {title}
    </Button>
  );
}

export default ToggleBtn;
