import React, { useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete, {
  createFilterOptions
} from '@material-ui/lab/Autocomplete';

const filter = createFilterOptions();

export default function AutoCompleteCreateAble({
  options = [],
  getter,
  isDisable = false
}) {
  const [value, setValue] = React.useState(null);
  const [error, setError] = React.useState(false);
  useEffect(() => {
    setValue([]);
  }, [options]);

  return (
    <Autocomplete
      value={value}
      disabled={isDisable}
      onChange={(event, newValue) => {
        getter(newValue?.value);
        if (newValue && newValue.hasOwnProperty('value')) {
          if (isNaN(newValue.value)) {
            setError(true);
            getter(null);
          } else {
            setError(false);
          }
        }

        if (typeof newValue === 'string') {
          setValue({
            name: newValue
          });
        } else if (newValue && newValue.inputValue) {
          // Create a new value from the user input
          setValue({
            name: newValue.inputValue
          });
        } else {
          setValue(newValue);
        }
      }}
      filterOptions={(options, params) => {
        const filtered = filter(options, params);

        // Suggest the creation of a new value
        if (params.inputValue !== '') {
          filtered.push({
            value: params.inputValue,
            name: `Add "${params.inputValue}"`
          });
        }

        return filtered;
      }}
      selectOnFocus
      clearOnBlur
      handleHomeEndKeys
      id="auto-complete-createable"
      options={options}
      getOptionLabel={option => {
        // Value selected with enter, right from the input
        if (typeof option === 'string') {
          return option;
        }
        // Add "xxx" option created dynamically
        if (option.value) {
          return option.value.toString();
        }
        // Regular option
        return option.value?.toString();
      }}
      renderOption={option => option.name}
      // style={{ width: 300 }}
      freeSolo
      renderInput={params => (
        <TextField
          {...params}
          variant="outlined"
          fullWidth
          error={error}
          helperText={error && 'Thời gian ước tính phải là dạng số'}
        />
      )}
    />
  );
}
