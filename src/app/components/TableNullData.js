import React from 'react';
import PropTypes from 'prop-types';
import { TableCell, TableRow } from '@material-ui/core';
import NullData from './NullData';

TableNullData.propTypes = {
  numberOfTableColumn: PropTypes.number.isRequired
};

function TableNullData({ numberOfTableColumn }) {
  return (
    <TableRow>
      <TableCell colSpan={numberOfTableColumn} style={{ textAlign: 'center' }}>
        <NullData />
      </TableCell>
    </TableRow>
  );
}

export default TableNullData;
