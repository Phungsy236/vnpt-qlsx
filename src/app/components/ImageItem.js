import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Box,
  Card,
  Divider,
  Grid,
  makeStyles,
  Typography
} from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import { CameraAlt, Room } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column'
  },
  statsItem: {
    alignItems: 'center',
    display: 'flex'
  },
  statsIcon: {
    marginRight: theme.spacing(1)
  },
  imgStyle: {
    cursor: 'pointer',
    width: '100%',
    height: '230px'
  },
  borderRounded: {
    // marginRight: 15,
    textAlign: 'center',
    paddingTop: 7,
    width: '60px',
    height: '60px',
    border: '3px solid #72c8e5',
    borderRadius: '100%'
  },
  groupInforImage: {
    display: 'flex',
    paddingLeft: 7,
    justifyContent: 'space-between',
    alignItems: 'center'
  }
}));

const ImageItem = ({ className, actionDetailsImgRef, image, ...rest }) => {
  const classes = useStyles();
  // function handleView() {
  //   if (!actionDetailsImgRef) return;
  //   actionDetailsImgRef(image);
  // }
  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      <Box display="flex" justifyContent="center" mb={1}>
        <img
          src={image.url}
          alt={image.file_name}
          className={classes.imgStyle}
        />
      </Box>

      <Box flexGrow={1} />
      <Divider />
      <Grid container className={classes.groupInforImage}>
        <Grid item xs={8}>
          <Grid container justify="space-between" spacing={2}>
            <Grid className={classes.statsItem} item>
              <AccessTimeIcon className={classes.statsIcon} color="primary" />
              <Typography
                color="textSecondary"
                display="inline"
                variant="caption"
              >
                {image.file_name}
              </Typography>
            </Grid>
          </Grid>{' '}
          <Grid container justify="space-between" spacing={2}>
            <Grid className={classes.statsItem} item>
              <CameraAlt className={classes.statsIcon} color="primary" />
              <Typography
                color="textSecondary"
                display="inline"
                variant="caption"
              >
                {'Kênh 1'}
              </Typography>
            </Grid>
          </Grid>{' '}
          <Grid container justify="space-between" spacing={2}>
            <Grid className={classes.statsItem} item>
              <Room color={'primary'} className={classes.statsIcon} />
              <Typography
                color="textSecondary"
                display="inline"
                variant="caption"
              >
                {'Hồ Mễ Trì'}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid
          item
          xs={4}
          spacing={1}
          style={{ display: 'flex', justifyContent: 'center' }}
        >
          <div className={classes.borderRounded}>
            <Typography variant={'h5'}>30</Typography>
            <Typography variant={'caption'}>Km/h</Typography>
          </div>
        </Grid>
      </Grid>
    </Card>
  );
};

ImageItem.propTypes = {
  actionDetailsImgRef: PropTypes.func,
  image: PropTypes.shape({
    url: PropTypes.string,
    file_name: PropTypes.string
  }).isRequired
};

export default ImageItem;
