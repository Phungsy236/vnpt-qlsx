import Typography from '@material-ui/core/Typography';
import React from 'react';
import PropTypes from 'prop-types';

CustomErrorMessage.propTypes = {
  content: PropTypes.string.isRequired
};

function CustomErrorMessage({ content }) {
  return (
    <Typography
      style={{
        width: '100%',
        textAlign: 'center',
        fontWeight: 700,
        margin: 20,
        color: '#f05053',
        fontSize: 18
      }}
    >
      {content}
    </Typography>
  );
}

export default CustomErrorMessage;
