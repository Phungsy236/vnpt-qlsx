import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Editor } from '@tinymce/tinymce-react';
import axios from 'axios';


MyEditor.propTypes = {
  content: PropTypes.string,
  getter: PropTypes.func.isRequired
};

function MyEditor({ content , getter }) {
  function handleEditorChange(e) {
    getter( e.target.getContent())
    // console.log('Content was updated:', e.target.getContent());
  }
  // fix unable focus to input
  useEffect(() => {
    const handler = e => {
      if (e.target.id === 'removeTabindex')
        e.target.removeAttribute('tabindex');
      else return;
    };
    document.addEventListener('focusin', handler);
    return () => document.removeEventListener('focusin', handler);
  }, []);

  function handleUploadImage(blobInfo, success, failure) {
    let formData = new FormData();
    formData.append('file', blobInfo.blob(), blobInfo.filename());
    let config = {
      method: 'POST',
      url: `${process.env.REACT_APP_LOCAL_ENTRY}/storage/`,
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      data: formData
    };
    axios(config)
      .then(function(response) {
        success(response.data.data.url)
        console.log(response.data.data.url);
      })
      .catch(function(error) {
        failure(error)
        alert(error);
      });
  }

  return (
    <Editor
      initialValue={content}
      apiKey={process.env.REACT_APP_TINY_MCE_KEY}
      init={{
        height: 500,
        width: '100%',
        menubar: "false",
        selector: 'textarea',
        images_upload_handler: handleUploadImage,
        file_picker_types: 'image',
        block_unsupported_drop: true,
        content_style:"body img { max-width:100% }",
        plugins: [
          'advlist autolink link image imagetool lists  charmap print preview hr anchor pagebreak',
          'searchreplace visualblocks visualchars code fullscreen  media nonbreaking importcss',
          'save table contextmenu directionality emoticons paste textcolor'
        ],
        toolbar:
          'insertfile | undo | redo | styleselect | bold |italic \
              alignleft | aligncenter | alignright | alignjustify | bullist | numlist | outdent | indent | \
              link | image | table | print | preview | media | fullpage | forecolor | backcolor | emoticons'
      }}
      onChange={handleEditorChange}
    />
  );
}

export default MyEditor;
