import { TextField } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

AutoCompleteMulti.propTypes = {
  options: PropTypes.array,
  defaultValue: PropTypes.array, // default value is object same with options
  displayAttributeName: PropTypes.string.isRequired,
  handleSendValue: PropTypes.func,
  placeHoldInput: PropTypes.string,
  size: PropTypes.oneOf(['small , medium'])
};

function AutoCompleteMulti({
  options,
  defaultValue,
  handleSendValue,
  isMulti,
  displayAttributeName,
  placeHoldInput,
  isError,
  helperText,
  inputName,
  minWidth,
  size = 'medium',
  onBlurFormik
}) {
  return (
    <Autocomplete
      multiple={isMulti}
      size={size}
      id="tags-outlined"
      options={options}
      getOptionLabel={option => option[displayAttributeName]}
      defaultValue={defaultValue}
      filterSelectedOptions
      onBlur={() => {
        if (!onBlurFormik) return;
        onBlurFormik();
      }}
      onChange={(event, value) => {
        handleSendValue(value);
      }}
      style={{ width: '100%' }}
      renderInput={params => (
        <TextField
          style={{ minWidth: minWidth ? minWidth : '' }}
          error={isError}
          helperText={helperText}
          {...params}
          name={inputName}
          variant="outlined"
          placeholder={placeHoldInput || ''}
        />
      )}
    />
  );
}

export default AutoCompleteMulti;
