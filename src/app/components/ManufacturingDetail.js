import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Card,
  CardContent,
  Divider,
  Grid,
  makeStyles,
  Tooltip
} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { ErrorOutline } from '@material-ui/icons';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import moment from 'moment/moment';
import React from 'react';
import CustomErrorMessage from 'src/app/components/CustomErrorMsg';
import { RowInfor } from 'src/app/components/RowInfo';

ManufacturingDetail.propTypes = {};

function ManufacturingDetail({ data, allProduct, allStation }) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState('panel1');
  const handleTabChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
  return (
    <Card className={classes.shadowBox}>
      <CardContent>
        <Accordion
          expanded={expanded === 'panel1'}
          onChange={handleTabChange('panel1')}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography className={classes.heading}>
              Định danh kết quả sản xuất
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Grid container spacing={1}>
              <Grid item md={6} xs={12}>
                <RowInfor title={'Tên'} content={data.name} />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor title={'Ca sản xuất '} content={data.shift} />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Thời gian bắt đầu sản xuất '}
                  content={'ngày ' + moment(data.dateTime).format('LLL')}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor title={'Mã lệnh sản xuất '} content={data.wo} />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Công đoạn '}
                  content={
                    allStation?.filter(item => item.id === data.stationId)[0]
                      ?.name
                  }
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Sản phẩm'}
                  content={
                    allProduct?.filter(item => item.id === data.productId)[0]
                      .name
                  }
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Thời gian dự tính'}
                  content={data.cycleTime + ' Giờ'}
                />
              </Grid>
            </Grid>
          </AccordionDetails>
        </Accordion>
        <Accordion
          expanded={expanded === 'panel2'}
          onChange={handleTabChange('panel2')}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography className={classes.heading}>
              Kế hoạch sản xuất
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Grid container spacing={1}>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Số lượng công nhân sản xuất trực tiếp'}
                  content={data.manAsm}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Số lượng công nhân hỗ trợ sản xuất trực tiếp '}
                  content={data.manSubAsm}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Số lượng công nhân sản xuất gián tiếp'}
                  content={data.manIndirect}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Số lượng công nhân sản xuất sửa chữa'}
                  content={data.manRepair}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  isSummary={true}
                  title={'Tổng số lượng công nhân'}
                  content={data.manTotal}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={
                    'Tổng số lượng nhân lực trực tiếp satn xuất tạo sản phẩm'
                  }
                  content={data.manHTotal}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Tổng sản lượng cho lệnh sản xuất'}
                  content={data.woQty}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Sản lượng theo kế hoạch'}
                  content={data.planningQty}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Nhân công theo kế hoạch'}
                  content={data.planningMan}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Nhân lực theo kế hoạch'}
                  content={data.planningManH}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Tổng sản lượng cho lệnh sản xuất'}
                  content={data.woQty}
                />
              </Grid>
            </Grid>
          </AccordionDetails>
        </Accordion>
        <Accordion
          expanded={expanded === 'panel3'}
          onChange={handleTabChange('panel3')}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography className={classes.heading}>
              Kết quả sản xuất
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Grid container spacing={3}>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Sản lượng đã hoàn thành cho lệnh sản xuất'}
                  content={data.woComplete}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Tỷ lệ hoàn thành lệnh sản xuất'}
                  content={data.woRate}
                  endTitle={'%'}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Sản lượng thực tế'}
                  content={data.actualQty}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Nhân lực thực tế sử dụng'}
                  content={data.actualManH}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Tổng số lượng nhân lực thực tế theo tính toán'}
                  content={data.actualManHLt}
                  isSummary={true}
                />
              </Grid>
              {data.downSmt && (
                <Grid item md={6} xs={12}>
                  <RowInfor
                    title={'Thời gian dừng sản xuất do dây chuyền SMT'}
                    content={data.downSmt + ' Giờ'}
                    explain={
                      <Tooltip title={data.reasonSmt}>
                        <ErrorOutline color={'primary'} />
                      </Tooltip>
                    }
                  />
                </Grid>
              )}

              {data.downStation && (
                <Grid item md={6} xs={12}>
                  <RowInfor
                    title={'Thời gian dừng sản xuất do lỗi trạm'}
                    content={data.downStation + ' Giờ'}
                    explain={
                      <Tooltip title={data.reasonStation}>
                        <ErrorOutline color={'primary'} />
                      </Tooltip>
                    }
                  />
                </Grid>
              )}

              {data.downMaintain && (
                <Grid item md={6} xs={12}>
                  <RowInfor
                    title={'Thời gian dừng sản xuất do bảo trì , bảo dưỡng'}
                    content={data.downMaintain + ' Giờ'}
                    explain={
                      <Tooltip title={data.reasonMaintain}>
                        <ErrorOutline color={'primary'} />
                      </Tooltip>
                    }
                  />
                </Grid>
              )}
              {data.downMaterial && (
                <Grid item md={6} xs={12}>
                  <RowInfor
                    title={'Thời gian dừng sản xuất do vấn đề vật tư'}
                    content={data.downMaterial + ' Giờ'}
                    explain={
                      <Tooltip title={data.reasonMaterial}>
                        <ErrorOutline color={'primary'} />
                      </Tooltip>
                    }
                  />
                </Grid>
              )}

              {data.downMan && (
                <Grid item md={6} xs={12}>
                  <RowInfor
                    title={'Thời gian dừng sản xuất do nhân công'}
                    content={data.downMan + ' Giờ'}
                    explain={
                      <Tooltip title={data.reasonMan}>
                        <ErrorOutline color={'primary'} />
                      </Tooltip>
                    }
                  />
                </Grid>
              )}
              {data.downTotal && (
                <Grid item md={6} xs={12}>
                  <RowInfor
                    title={'Tổng thời gian dừng sản xuất'}
                    content={data.downMan + ' Giờ'}
                    isSummary={true}
                  />
                </Grid>
              )}
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Tỉ lệ sản phẩm tốt'}
                  content={data.qualityRate || 0 + '%'}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Tỉ lệ sản phẩm tốt mục tiêu'}
                  content={data.qualityRateTarget || 0 + '%'}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Tỉ lệ sử dụng nguồn lực'}
                  content={data.availability || 0 + '%'}
                  isSummary={true}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Hiệu suất sử dụng nguồn lực'}
                  content={data.performance || 0 + '%'}
                  isSummary={true}
                />
              </Grid>
              <Grid item md={6} xs={12}>
                <RowInfor
                  title={'Tổng thể hiệu quả sử dụng nguồn lực'}
                  content={data.ooe || 0 + '%'}
                  isSummary={true}
                />
              </Grid>
            </Grid>
          </AccordionDetails>
        </Accordion>
      </CardContent>
      <Divider />
    </Card>
  );
}
const useStyles = makeStyles(theme => ({
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  root: {
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  },
  headerDivider: {
    backgroundColor: '#e8e7e7',
    padding: theme.spacing(1),
    marginBottom: '15px'
  },
  heading: {
    fontSize: theme.typography.pxToRem(17),
    fontWeight: 500,
    flexBasis: '70%',
    flexShrink: 0
  },
  historyWrapper: {
    marginTop: theme.spacing(2),
    padding: 10
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  wrapper: {
    position: 'relative'
  },
  colorWhite: {
    color: '#fff'
  },
  tabHeader: {
    backgroundColor: 'aliceblue'
  }
}));
export default ManufacturingDetail;
