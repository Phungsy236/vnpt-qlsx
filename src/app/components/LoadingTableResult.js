import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import { TableCell, TableRow } from '@material-ui/core';

function NullData({ numberOfTableColumn }) {
  const classes = useStyles();
  return (
    <TableRow>
      <TableCell colSpan={numberOfTableColumn} style={{ textAlign: 'center' }}>
        <CircularProgress className={classes.spiner} color="inherit" />
      </TableCell>
    </TableRow>
  );
}
const useStyles = makeStyles(theme => ({
  spiner: {
    width: '90px !important',
    height: '90px !important'
  }
}));
export default NullData;
