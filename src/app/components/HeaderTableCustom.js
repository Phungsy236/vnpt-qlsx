import React, { useState } from 'react';
import PropTypes from 'prop-types';
import AutoCompleteMulti from 'src/app/components/AutoCompleteMulti';
import { SORT_TYPE } from '../constant/config';
import { Box, Divider, InputBase, Paper, TableCell } from '@material-ui/core';
import theme from '../theme';
import Typography from '@material-ui/core/Typography';
import { Sort } from '@material-ui/icons';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

HeaderTableCustom.propTypes = {
  title: PropTypes.string.isRequired,
  searchName: PropTypes.string,
  callbackSort: PropTypes.func,
  callbackSearch: PropTypes.func,
  isEnableSearch: PropTypes.bool,
  typeSearch: PropTypes.oneOf(['input', 'date', 'select']),
  optionSearch: PropTypes.array
};

function HeaderTableCustom({
  title,
  searchName,
  typeSearch = 'input',
  callbackSort,
  callbackSearch,
  optionSearch,
  isEnableSearch = false,
  isEnableSort = false,
  placeHolder
}) {
  const [searchText, setSearchText] = useState('');
  const [sortType, setSortType] = useState(SORT_TYPE.ASC);
  const onEnterSearchInput = event => {
    if (!callbackSearch) return;
    if (event.keyCode === 13) {
      callbackSearch(searchText);
    }
  };
  return (
    <TableCell style={{ padding: theme.spacing(2) }}>
      <Box
        style={{
          display: 'flex',
          justifyContent: 'space-evenly',
          marginBottom: 5
        }}
      >
        <Typography noWrap={true} style={{ width: '85%' }}>
          {title}
        </Typography>
        <Sort
          onClick={() => {
            if (sortType === SORT_TYPE.DESC && callbackSort) {
              callbackSort(SORT_TYPE.ASC);
              setSortType(SORT_TYPE.ASC);
            } else if (sortType !== SORT_TYPE.DESC && callbackSort) {
              callbackSort(SORT_TYPE.DESC);
              setSortType(SORT_TYPE.DESC);
            }
          }}
          style={{
            cursor: 'pointer',
            transform: `${
              sortType === SORT_TYPE.ASC ? 'rotate(180deg)' : 'rotate(0deg)'
            }`,
            transition: '0.3',
            color: isEnableSort ? '#0066B3' : 'gray',
            pointerEvents: isEnableSort ? '' : 'none'
          }}
          color={'primary'}
        />
      </Box>
      <Paper
        component="div"
        style={{
          padding: '2px 4px',
          display: 'flex',
          alignItems: 'center',
          pointerEvents: isEnableSearch ? '' : 'none'
        }}
      >
        {typeSearch === 'input' && (
          <InputBase
            style={{ marginLeft: theme.spacing(1), flex: 1 }}
            name={searchName}
            onChange={event => setSearchText(event.target.value)}
            onKeyDown={onEnterSearchInput}
            type={'search'}
            placeholder="search..."
            inputProps={{ 'aria-label': 'search google maps' }}
          />
        )}
        {typeSearch === 'select' && (
          <AutoCompleteMulti
            minWidth={200}
            displayAttributeName={'name'}
            size={'small'}
            handleSendValue={value => setSearchText(value?.id || '')}
            options={optionSearch}
            isMulti={false}
            placeHoldInput={placeHolder || 'Search...'}
          />
        )}

        <Divider style={{ height: 28, margin: 4 }} orientation="vertical" />
        <IconButton
          style={{ padding: 10, color: isEnableSearch ? '#0066B3' : 'gray' }}
          aria-label="search"
          onClick={() => callbackSearch(searchText)}
        >
          <SearchIcon />
        </IconButton>
      </Paper>
      <Divider orientation="vertical" flexItem />
    </TableCell>
  );
}

export default HeaderTableCustom;
