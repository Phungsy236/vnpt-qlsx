import React from 'react';
import PropTypes from 'prop-types';

TextHightLight.propTypes = {
  content: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['blue', 'red', 'green']).isRequired,
  isCursor: PropTypes.bool
};

function TextHightLight({ content = 'text', type, isCursor = false }) {
  let color = '';
  switch (type) {
    case 'blue':
      color = '#0066B3';
      break;
    case 'red':
      color = '#ff0040';
      break;
    case 'green':
      color = '#33CC00';
      break;
    default:
      color = '3f51b5';
  }
  return (
    <b style={{ color: color, cursor: isCursor ? 'pointer' : '' }}>{content}</b>
  );
}
export default TextHightLight;
