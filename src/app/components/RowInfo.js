import { Box, makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import theme from 'src/app/theme';

const useStyles = makeStyles(theme => ({
  wrapper: {
    width: '100%',
    [theme.breakpoints.down('md')]: {
      padding: 0
    },
    [theme.breakpoints.up('md')]: {
      padding: '5px 20px'
    },

    display: 'flex'
    // alignItems: 'center'
    // border: isSumary ? '0.1 px solid lightGray' : ''
  },

  titlePrimary: {},
  secondPrimary: {}
}));

export const RowInfor = ({ title, content, isSummary = false, explain }) => {
  const classes = useStyles();

  return (
    <Box
      className={classes.wrapper}
      style={{ border: isSummary ? '.1px solid lightGray' : '' }}
    >
      <Typography
        variant={'h6'}
        style={{ width: '60%', marginRight: 10 }}
        color={isSummary ? 'textSecondary' : 'primary'}
      >
        {title}:
      </Typography>
      <Typography
        style={{ width: '30%', marginRight: 10 }}
        variant={'caption'}
        color={isSummary ? 'textSecondary' : 'primary'}
      >
        {content}
      </Typography>

      <Box style={{ alignSelf: 'flex-end' }}>{explain}</Box>
    </Box>
  );
};
