import { configureStore } from '@reduxjs/toolkit';
import lifeCycleSlice from 'src/features/lifeCycleSlice';
import manufacturingSlice from 'src/features/manufacturingSlice';
import privilegeSlice from 'src/features/privilegeSlice';
import roleSlice from 'src/features/roleSlice';
import authSlice from 'src/features/authSlice';
import uiSlice from 'src/features/uiSlice';
import userSlice from 'src/features/userSlice';
import stationSlice from 'src/features/stationSlice';
import productSlice from 'src/features/productSlice';

export default configureStore({
  reducer: {
    authSlice: authSlice,
    uiSlice: uiSlice,
    stationSlice: stationSlice,
    productSlice: productSlice,
    userSlice: userSlice,
    roleSlice: roleSlice,
    privilegeSlice: privilegeSlice,
    manufacturingSlice: manufacturingSlice,
    lifeCycleSlice: lifeCycleSlice
  }
});
