import moment from 'moment';

export class BaseInfor {
  constructor() {
    this.name = '';
    this.description = '';
    this.created = moment(new Date()).valueOf();
    this.updated = moment(new Date()).valueOf();
    this.createdBy = '';
    this.updatedBy = '';
    this.active = 0;
  }
}
