import { PAGE_SIZE_LIST } from 'src/app/constant/config';

export class DefaultSearchParams {
  constructor(page, size, sort, query) {
    this.query = query || '';
    this.page = page || 0;
    this.size = size || PAGE_SIZE_LIST;
    this.sort = sort || 'created,desc';
  }
}
