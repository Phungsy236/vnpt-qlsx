import { BaseInfor } from 'src/app/models/BaseInfor';
export class LifeCycle extends BaseInfor {
  constructor() {
    super();
    this.value = null;
    this.stationId = null;
    this.productId = null;
  }
}
