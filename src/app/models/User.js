import { BaseInfor } from './BaseInfor';

export class User extends BaseInfor {
  constructor() {
    super();
    this.email = '';
    this.password = '';
    this.address = '';
    this.phone = '';
    this.fullName = '';
    this.activation_token = '';
    this.activation_token_created = '';
    this.jwt_token = '';
    this.avatar = '';
  }
}
