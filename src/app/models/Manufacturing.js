import moment from 'moment/moment';
import { SHIFT } from 'src/app/constant/config';
import { BaseInfor } from 'src/app/models/BaseInfor';

export class Manufacturing extends BaseInfor {
  constructor() {
    super();
    this.active = 1;
    // Định danh kqsx
    this.name = '';
    this.wo = '';
    this.shift = SHIFT[0];
    this.cycleTime = 0;
    this.stationId = null;
    this.productId = null;
    this.dateTime = moment(new Date()).valueOf();

    // ke hoạch
    this.manAsm = 0;
    this.manSubAsm = 0;
    this.manIndirect = 0;
    this.manRepair = 0;
    this.manOff = 0;
    this.manHTotal = 0;
    this.manTotal =
      this.manAsm +
      this.manSubAsm +
      this.manIndirect +
      this.manRepair +
      this.manOff;
    this.woQty = 0;
    this.planningQty = 0;
    this.planningManH = 0;
    this.planningMan = 0;

    /*=====> this.mantotal = sumAll (man)*/

    // ket quả
    this.woComplete = 0;
    this.woRate = 0; /**/
    this.actualQty = 0;
    this.actualManH = 0;
    this.actualManHLt = 0; /* actualQty * cycleTime*/
    // group down
    this.downSmt = 0;
    this.reasonSmt = '';
    this.downStation = 0;
    this.reasonStation = '';
    this.downMaintain = 0;
    this.reasonMaintain = '';
    this.downMaterial = 0;
    this.reasonMaterial = '';
    this.downMan = 0;
    this.reasonMan = '';
    this.downTotal = 0; /**/
    this.performance = 0;
    this.qualityRate = 0;
    // danh gia
    this.availability = 0;
    this.ooe = 0; /**/

    this.qualityRateTarget = 0; /**/
  }
}
