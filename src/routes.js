import React from 'react';
import { Navigate } from 'react-router-dom';
import { LIST_ROLE } from 'src/app/constant/config';
import DashboardLayout from 'src/app/layouts/DashboardLayout';
import MainLayout from 'src/app/layouts/MainLayout';
import LoadingPage from 'src/app/views/errors/LoadingPage';
import LifeCycleListView from 'src/app/views/lifecyle_time';
import LoginView from 'src/app/views/login/LoginView';
import ManufacturingListView from 'src/app/views/manufacturing_result';
import PrivilegeListView from 'src/app/views/user/privilege';
import RoleListView from 'src/app/views/user/role';
import UserListView from 'src/app/views/user/user';
import Account from 'src/app/views/user/user_account';
import NotFoundView from './app/views/errors/NotFoundView';
import DraftPageView from './app/views/errors/DraftPageView';
import StationListView from './app/views/station';
import ProductListView from './app/views/product';
import Dashboard from './app/views/dashboard';

const routes = authorities => {
  if (authorities)
    return [
      {
        path: 'vnpt',
        element: <DashboardLayout />,
        children: [
          { path: 'dashboard', element: <Dashboard /> },
          {
            path: 'station',
            element: authorities?.includes(LIST_ROLE.Station_View) ? (
              <StationListView authorities={authorities} />
            ) : (
              <NotFoundView />
            )
          },
          {
            path: 'product',
            element: authorities?.includes(LIST_ROLE.Product_View) ? (
              <ProductListView authorities={authorities} />
            ) : (
              <NotFoundView />
            )
          },
          {
            path: 'lifecycle_time',
            element: authorities?.includes(LIST_ROLE.Cycle_Time_View) ? (
              <LifeCycleListView authorities={authorities} />
            ) : (
              <NotFoundView />
            )
          },
          {
            path: 'manufacturing',
            element: authorities?.some(item =>
              [
                LIST_ROLE.Indentify_View,
                LIST_ROLE.Result_View,
                LIST_ROLE.Strategy_View
              ].includes(item)
            ) ? (
              <ManufacturingListView authorities={authorities} />
            ) : (
              <NotFoundView />
            )
          },
          {
            path: 'user',
            children: [
              {
                path: '/all',
                element: authorities?.includes(LIST_ROLE.User_View) ? (
                  <UserListView authorities={authorities} />
                ) : (
                  <NotFoundView />
                )
              },
              { path: '/account', element: <Account /> },
              {
                path: '/privilege',
                element: authorities?.includes(LIST_ROLE.Privilege_View) ? (
                  <PrivilegeListView authorities={authorities} />
                ) : (
                  <NotFoundView />
                )
              },
              {
                path: '/role',
                element: authorities?.includes(LIST_ROLE.Role_View) ? (
                  <RoleListView authorities={authorities} />
                ) : (
                  <NotFoundView />
                )
              }
            ]
          },
          { path: '*', element: <Navigate to="/vnpt/dashboard" /> }
        ]
      },
      {
        path: '/',
        element: <MainLayout />,
        children: [
          { path: 'login', element: <LoginView /> },
          // { path: 'register', element: <RegisterView /> },
          { path: '404', element: <NotFoundView /> },
          { path: 'waiting-confirm', element: <DraftPageView /> },
          { path: '/', element: <Navigate to="/vnpt/dashboard" /> },
          { path: '*', element: <Navigate to="/404" /> }
        ]
      }
    ];
  else
    return [
      { path: '*', element: <LoadingPage /> },
      { path: '/login', element: <LoginView /> }
    ];
};

export default routes;
