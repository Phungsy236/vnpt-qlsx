import { createSlice } from '@reduxjs/toolkit';
import {
  HTTP_GETTYPE,
  PAGE_SIZE_LIST,
  SORT_TYPE,
  STATUS_API
} from 'src/app/constant/config';
import { error } from 'src/app/constant/error_message';
import { mapStringToObjectProperties } from 'src/app/utils/apiService';
import AxiosAdapter from './AxiosAdapter';
import { ArrUtil } from '../app/utils/commomService';
import { Station } from '../app/models/Station';

export const getListStation = AxiosAdapter.GetHttp(
  'StationSlice/getAll',
  '/station/search',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const getAllStation = AxiosAdapter.GetHttp(
  'StationSlice/getAllNoPagination',
  '/station/search?query=&page=0&size=10000',
  HTTP_GETTYPE.ALL
);
export const getTotalStation = AxiosAdapter.GetHttp(
  'StationSlice/getTotal',
  '/station/search?query=&page=0&size=1',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const getDetailStation = AxiosAdapter.GetHttp(
  'stationSlice/GetDetailStation',
  '/station/',
  HTTP_GETTYPE.DETAIL
);
export const deleteMultiStation = AxiosAdapter.HttpPost(
  'stationSlice/DeleteMulti',
  '/station/batch-delete',
  false
);
export const createStation = AxiosAdapter.HttpPost(
  'stationSlice/CreateStation',
  '/station'
);
export const updateStation = AxiosAdapter.HttpPut(
  'stationSlice/UpdateStation',
  '/station/'
);
export const deleteStation = AxiosAdapter.HttpDelete(
  'stationSlice/DeleteStation',
  '/station/'
);

export const stationSlice = createSlice({
  name: 'stationSlice',
  initialState: {
    listStation: null,
    allStation: null,
    totalStation: 0,
    errorGetList: null,
    statusGetAll: null,

    err: null,
    stationDetail: null,
    statusGetDetail: null,
    statusDeleteMulti: null,
    statusCreateStation: null,
    statusUpdateStation: null,
    statusDeleteStation: null,
    statusGetTotal: null
  },

  reducers: {
    resetStatus: state => {
      state.statusGetAll = null;
      state.statusGetDetail = null;
      state.statusCreateStation = null;
      state.statusUpdateStation = null;
      state.statusDeleteStation = null;
      state.statusDeleteMulti = null;
      state.statusGetTotal = null;
    },
    sortStationByName: (state, action) => {
      if (action.payload === SORT_TYPE.DESC) {
        state.listStation = ArrUtil.sortDescByKey(state.listStation, 'name');
      } else {
        state.listStation = ArrUtil.sortAscByKey(state.listStation, 'name');
      }
    }
    //  every reducer must export to user
  },
  extraReducers: {
    [getListStation.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.listStation = null;
      state.totalStation = 0;
    },
    [getListStation.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.listStation = action.payload.data;
      state.totalStation = parseInt(action.payload.total);
    },
    [getListStation.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [getAllStation.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.allStation = null;
    },
    [getAllStation.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.allStation = action.payload;
    },
    [getAllStation.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    [getTotalStation.pending]: state => {
      state.statusGetTotal = STATUS_API.PENDING;
      state.err = null;
    },
    [getTotalStation.fulfilled]: (state, action) => {
      state.statusGetTotal = STATUS_API.SUCCESS;
      state.totalStation = parseInt(action.payload.total);
    },
    [getTotalStation.rejected]: (state, action) => {
      state.statusGetTotal = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================get detail=================================*/
    [getDetailStation.pending]: state => {
      state.statusGetDetail = STATUS_API.PENDING;
      state.err = null;
      state.stationDetail = null;
    },
    [getDetailStation.fulfilled]: (state, action) => {
      state.statusGetDetail = STATUS_API.SUCCESS;
      state.stationDetail = action.payload;
    },
    [getDetailStation.rejected]: (state, action) => {
      state.statusGetDetail = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================create=================================*/

    [createStation.pending]: state => {
      state.statusCreateStation = STATUS_API.PENDING;
      state.err = null;
    },
    [createStation.fulfilled]: (state, action) => {
      state.statusCreateStation = STATUS_API.SUCCESS;
      if (!state.listStation) state.listStation = [];
      state.totalStation += 1;
      if (state.listStation.length < PAGE_SIZE_LIST)
        state.listStation.unshift(action.payload);
      else {
        state.listStation.pop();
        state.listStation.unshift(action.payload);
      }
    },
    [createStation.rejected]: (state, action) => {
      state.statusCreateStation = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================update=================================*/

    [updateStation.pending]: state => {
      state.statusUpdateStation = STATUS_API.PENDING;
      state.err = null;
    },
    [updateStation.fulfilled]: (state, action) => {
      state.statusUpdateStation = STATUS_API.SUCCESS;
      state.listStation = state.listStation.map(item => {
        if (item.id === action.payload.id) {
          return action.payload;
        } else return item;
      });
    },
    [updateStation.rejected]: (state, action) => {
      state.statusUpdateStation = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    /*============================delete=================================*/
    [deleteStation.pending]: state => {
      state.statusDeleteStation = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteStation.fulfilled]: (state, action) => {
      state.statusDeleteStation = STATUS_API.SUCCESS;
      state.totalStation -= 1;
      let deletedId = state.listStation.findIndex(
        item => item.id === action.payload
      );
      state.listStation.splice(deletedId, 1);
    },
    [deleteStation.rejected]: (state, action) => {
      state.statusDeleteStation = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [deleteMultiStation.pending]: state => {
      state.statusDeleteMulti = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteMultiStation.fulfilled]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.SUCCESS;
      state.totalStation -= action.payload.length;
      let listDeleted = action.payload;
      for (const id of listDeleted) {
        let index = state.listStation.findIndex(item => item.id === id);
        state.listStation.splice(index, 1);
      }
    },
    [deleteMultiStation.rejected]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    }
  }
});
export const { resetStatus, sortStationByName } = stationSlice.actions;

export default stationSlice.reducer;
