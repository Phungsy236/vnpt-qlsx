import { createSlice } from '@reduxjs/toolkit';
import {
  HTTP_GETTYPE,
  PAGE_SIZE_LIST,
  SORT_TYPE,
  STATUS_API
} from 'src/app/constant/config';
import { error } from 'src/app/constant/error_message';
import { mapStringToObjectProperties } from 'src/app/utils/apiService';
import AxiosAdapter from './AxiosAdapter';
import { ArrUtil } from '../app/utils/commomService';

export const getListPrivilege = AxiosAdapter.GetHttp(
  'PrivilegeSlice/getAll',
  '/privileges/search',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const getAllPrivilege = AxiosAdapter.GetHttp(
  'PrivilegeSlice/getAllNoPagination',
  '/privileges/search?query=&page=0&size=10000',
  HTTP_GETTYPE.ALL
);
export const getTotalPrivilege = AxiosAdapter.GetHttp(
  'PrivilegeSlice/getTotal',
  '/privileges/search?query=&page=0&size=1',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const getDetailPrivilege = AxiosAdapter.GetHttp(
  'privilegeSlice/GetDetailPrivilege',
  '/privileges/',
  HTTP_GETTYPE.DETAIL
);
export const deleteMultiPrivilege = AxiosAdapter.HttpPost(
  'privilegeSlice/DeleteMulti',
  '/privileges/batch-delete',
  false
);
export const createPrivilege = AxiosAdapter.HttpPost(
  'privilegeSlice/CreatePrivilege',
  '/privileges'
);
export const updatePrivilege = AxiosAdapter.HttpPut(
  'privilegeSlice/UpdatePrivilege',
  '/privileges/'
);
export const deletePrivilege = AxiosAdapter.HttpDelete(
  'privilegeSlice/DeletePrivilege',
  '/privileges/'
);

export const privilegeSlice = createSlice({
  name: 'privilegeSlice',
  initialState: {
    listPrivilege: null,
    allPrivilege: null,
    totalPrivilege: 0,
    errorGetList: null,
    statusGetAll: null,

    err: null,
    privilegeDetail: null,
    statusGetDetail: null,
    statusDeleteMulti: null,
    statusCreatePrivilege: null,
    statusUpdatePrivilege: null,
    statusDeletePrivilege: null,
    statusGetTotal: null
  },

  reducers: {
    resetStatus: state => {
      state.statusGetAll = null;
      state.statusGetDetail = null;
      state.statusCreatePrivilege = null;
      state.statusUpdatePrivilege = null;
      state.statusDeletePrivilege = null;
      state.statusDeleteMulti = null;
      state.statusGetTotal = null;
    },
    sortPrivilegeByName: (state, action) => {
      if (action.payload === SORT_TYPE.DESC) {
        state.listPrivilege = ArrUtil.sortDescByKey(
          state.listPrivilege,
          'name'
        );
      } else {
        state.listPrivilege = ArrUtil.sortAscByKey(state.listPrivilege, 'name');
      }
    }
    //  every reducer must export to user
  },
  extraReducers: {
    [getListPrivilege.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.listPrivilege = null;
      state.totalPrivilege = 0;
    },
    [getListPrivilege.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.listPrivilege = action.payload.data;
      state.totalPrivilege = parseInt(action.payload.total);
    },
    [getListPrivilege.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [getAllPrivilege.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.allPrivilege = null;
    },
    [getAllPrivilege.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.allPrivilege = action.payload;
    },
    [getAllPrivilege.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    [getTotalPrivilege.pending]: state => {
      state.statusGetTotal = STATUS_API.PENDING;
      state.err = null;
    },
    [getTotalPrivilege.fulfilled]: (state, action) => {
      state.statusGetTotal = STATUS_API.SUCCESS;
      state.totalPrivilege = parseInt(action.payload.total);
    },
    [getTotalPrivilege.rejected]: (state, action) => {
      state.statusGetTotal = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================get detail=================================*/
    [getDetailPrivilege.pending]: state => {
      state.statusGetDetail = STATUS_API.PENDING;
      state.err = null;
      state.privilegeDetail = null;
    },
    [getDetailPrivilege.fulfilled]: (state, action) => {
      state.statusGetDetail = STATUS_API.SUCCESS;
      state.privilegeDetail = action.payload;
    },
    [getDetailPrivilege.rejected]: (state, action) => {
      state.statusGetDetail = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================create=================================*/

    [createPrivilege.pending]: state => {
      state.statusCreatePrivilege = STATUS_API.PENDING;
      state.err = null;
    },
    [createPrivilege.fulfilled]: (state, action) => {
      state.statusCreatePrivilege = STATUS_API.SUCCESS;
      state.totalPrivilege += 1;
      if (!state.listPrivilege) state.listPrivilege = [];
      if (state.listPrivilege.length < PAGE_SIZE_LIST)
        state.listPrivilege.unshift(action.payload);
      else {
        state.listPrivilege.pop();
        state.listPrivilege.unshift(action.payload);
      }
    },
    [createPrivilege.rejected]: (state, action) => {
      state.statusCreatePrivilege = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================update=================================*/

    [updatePrivilege.pending]: state => {
      state.statusUpdatePrivilege = STATUS_API.PENDING;
      state.err = null;
    },
    [updatePrivilege.fulfilled]: (state, action) => {
      state.statusUpdatePrivilege = STATUS_API.SUCCESS;
      state.listPrivilege = state.listPrivilege.map(item => {
        if (item.id === action.payload.id) {
          return action.payload;
        } else return item;
      });
    },
    [updatePrivilege.rejected]: (state, action) => {
      state.statusUpdatePrivilege = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    /*============================delete=================================*/
    [deletePrivilege.pending]: state => {
      state.statusDeletePrivilege = STATUS_API.PENDING;
      state.err = null;
    },
    [deletePrivilege.fulfilled]: (state, action) => {
      state.statusDeletePrivilege = STATUS_API.SUCCESS;
      state.totalPrivilege -= 1;
      let deletedId = state.listPrivilege.findIndex(
        item => item.id === action.payload
      );
      state.listPrivilege.splice(deletedId, 1);
    },
    [deletePrivilege.rejected]: (state, action) => {
      state.statusDeletePrivilege = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [deleteMultiPrivilege.pending]: state => {
      state.statusDeleteMulti = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteMultiPrivilege.fulfilled]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.SUCCESS;
      state.totalPrivilege -= action.payload.length;
      let listDeleted = action.payload;
      for (const id of listDeleted) {
        let index = state.listPrivilege.findIndex(item => item.id === id);
        state.listPrivilege.splice(index, 1);
      }
    },
    [deleteMultiPrivilege.rejected]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    }
  }
});
export const { resetStatus, sortPrivilegeByName } = privilegeSlice.actions;

export default privilegeSlice.reducer;
