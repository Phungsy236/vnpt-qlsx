import { createSlice } from '@reduxjs/toolkit';
import {
  HTTP_GETTYPE,
  PAGE_SIZE_LIST,
  SORT_TYPE,
  STATUS_API
} from 'src/app/constant/config';
import { error } from 'src/app/constant/error_message';
import { mapStringToObjectProperties } from 'src/app/utils/apiService';
import AxiosAdapter from './AxiosAdapter';
import { ArrUtil } from '../app/utils/commomService';
import { Manufacturing } from '../app/models/Manufacturing';

export const getListManufacturing = AxiosAdapter.GetHttp(
  'ManufacturingSlice/getAll',
  '/manufacturing-result/search',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const getAllManufacturing = AxiosAdapter.GetHttp(
  'ManufacturingSlice/getAllNoPagination',
  '/manufacturing-result/search?query=&page=0&size=10000',
  HTTP_GETTYPE.ALL
);
export const getFullManufacturing = AxiosAdapter.GetHttp(
  'ManufacturingSlice/getFullDetail',
  '/manufacturing-result/get-full/',
  HTTP_GETTYPE.DETAIL
);
export const getTotalManufacturing = AxiosAdapter.GetHttp(
  'ManufacturingSlice/getTotal',
  '/manufacturing-result/search?query=&page=0&size=1',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const getDetailManufacturing = AxiosAdapter.GetHttp(
  'manufacturingSlice/GetDetailManufacturing',
  '/manufacturing-result/',
  HTTP_GETTYPE.DETAIL
);
export const deleteMultiManufacturing = AxiosAdapter.HttpPost(
  'manufacturingSlice/DeleteMulti',
  '/manufacturing-result/batch-delete',
  false
);
export const createManufacturing = AxiosAdapter.HttpPost(
  'manufacturingSlice/CreateManufacturing',
  '/manufacturing-result'
);
export const updateManufacturing = AxiosAdapter.HttpPut(
  'manufacturingSlice/UpdateManufacturing',
  '/manufacturing-result/'
);
export const deleteManufacturing = AxiosAdapter.HttpDelete(
  'manufacturingSlice/DeleteManufacturing',
  '/manufacturing-result/'
);

export const manufacturingSlice = createSlice({
  name: 'manufacturingSlice',
  initialState: {
    listManufacturing: null,
    allManufacturing: null,
    totalManufacturing: 0,
    errorGetList: null,
    statusGetAll: null,
    logs: null,
    detail: null,
    err: null,
    manufacturingDetail: null,
    statusGetDetail: null,
    statusDeleteMulti: null,
    statusCreateManufacturing: null,
    statusUpdateManufacturing: null,
    statusDeleteManufacturing: null,
    statusGetTotal: null,
    statusGetFullDetail: null
  },

  reducers: {
    resetStatus: state => {
      state.statusGetAll = null;
      state.statusGetDetail = null;
      state.statusCreateManufacturing = null;
      state.statusUpdateManufacturing = null;
      state.statusDeleteManufacturing = null;
      state.statusDeleteMulti = null;
      state.statusGetTotal = null;
    },
    sortManufacturingByName: (state, action) => {
      if (action.payload === SORT_TYPE.DESC) {
        state.listManufacturing = ArrUtil.sortDescByKey(
          state.listManufacturing,
          'name'
        );
      } else {
        state.listManufacturing = ArrUtil.sortAscByKey(
          state.listManufacturing,
          'name'
        );
      }
    }
    //  every reducer must export to user
  },
  extraReducers: {
    [getListManufacturing.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.listManufacturing = null;
      state.totalManufacturing = 0;
    },
    [getListManufacturing.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.listManufacturing = action.payload.data;
      state.totalManufacturing = parseInt(action.payload.total);
    },
    [getListManufacturing.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [getFullManufacturing.pending]: state => {
      state.statusGetFullDetail = STATUS_API.PENDING;
      state.err = null;
      state.detail = null;
      state.logs = 0;
    },
    [getFullManufacturing.fulfilled]: (state, action) => {
      state.statusGetFullDetail = STATUS_API.SUCCESS;
      state.detail = action.payload;
      state.logs = action.payload?.logs;
    },
    [getFullManufacturing.rejected]: (state, action) => {
      state.statusGetFullDetail = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    [getAllManufacturing.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.allManufacturing = null;
    },
    [getAllManufacturing.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.allManufacturing = action.payload;
    },
    [getAllManufacturing.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    [getTotalManufacturing.pending]: state => {
      state.statusGetTotal = STATUS_API.PENDING;
      state.err = null;
    },
    [getTotalManufacturing.fulfilled]: (state, action) => {
      state.statusGetTotal = STATUS_API.SUCCESS;
      state.totalManufacturing = parseInt(action.payload.total);
    },
    [getTotalManufacturing.rejected]: (state, action) => {
      state.statusGetTotal = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================get detail=================================*/
    [getDetailManufacturing.pending]: state => {
      state.statusGetDetail = STATUS_API.PENDING;
      state.err = null;
      state.manufacturingDetail = null;
    },
    [getDetailManufacturing.fulfilled]: (state, action) => {
      state.statusGetDetail = STATUS_API.SUCCESS;
      state.manufacturingDetail = action.payload;
    },
    [getDetailManufacturing.rejected]: (state, action) => {
      state.statusGetDetail = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================create=================================*/

    [createManufacturing.pending]: state => {
      state.statusCreateManufacturing = STATUS_API.PENDING;
      state.err = null;
    },
    [createManufacturing.fulfilled]: (state, action) => {
      state.statusCreateManufacturing = STATUS_API.SUCCESS;
      if (!state.listManufacturing) state.listManufacturing = [];
      state.totalManufacturing += 1;
      if (state.listManufacturing.length < PAGE_SIZE_LIST)
        state.listManufacturing.unshift(action.payload);
      else {
        state.listManufacturing.pop();
        state.listManufacturing.unshift(action.payload);
      }
    },
    [createManufacturing.rejected]: (state, action) => {
      state.statusCreateManufacturing = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================update=================================*/

    [updateManufacturing.pending]: state => {
      state.statusUpdateManufacturing = STATUS_API.PENDING;
      state.err = null;
    },
    [updateManufacturing.fulfilled]: (state, action) => {
      state.statusUpdateManufacturing = STATUS_API.SUCCESS;
      state.listManufacturing = state.listManufacturing.map(item => {
        if (item.id === action.payload.id) {
          return action.payload;
        } else return item;
      });
    },
    [updateManufacturing.rejected]: (state, action) => {
      state.statusUpdateManufacturing = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    /*============================delete=================================*/
    [deleteManufacturing.pending]: state => {
      state.statusDeleteManufacturing = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteManufacturing.fulfilled]: (state, action) => {
      state.statusDeleteManufacturing = STATUS_API.SUCCESS;
      state.totalManufacturing -= 1;
      let deletedId = state.listManufacturing.findIndex(
        item => item.id === action.payload
      );
      state.listManufacturing.splice(deletedId, 1);
    },
    [deleteManufacturing.rejected]: (state, action) => {
      state.statusDeleteManufacturing = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [deleteMultiManufacturing.pending]: state => {
      state.statusDeleteMulti = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteMultiManufacturing.fulfilled]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.SUCCESS;
      state.totalManufacturing -= action.payload.length;
      let listDeleted = action.payload;
      for (const id of listDeleted) {
        let index = state.listManufacturing.findIndex(item => item.id === id);
        state.listManufacturing.splice(index, 1);
      }
    },
    [deleteMultiManufacturing.rejected]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    }
  }
});
export const {
  resetStatus,
  sortManufacturingByName
} = manufacturingSlice.actions;

export default manufacturingSlice.reducer;
