import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import {
  HTTP_GETTYPE,
  PAGE_SIZE_LIST,
  SORT_TYPE,
  STATUS_API,
  TYPE_UPDATE_USER
} from 'src/app/constant/config';
import { error } from 'src/app/constant/error_message';
import AxiosAdapter from './AxiosAdapter';
import { ArrUtil } from '../app/utils/commomService';
import { mapStringToObjectProperties } from '../app/utils/apiService';

export const GetAccInfo = AxiosAdapter.GetHttp(
  'userSlice/getCurrentInfo',
  '/users/current',
  HTTP_GETTYPE.ALL
);
export const getListUser = AxiosAdapter.GetHttp(
  'userSlice/getList',
  '/users/search',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const getTotalUser = AxiosAdapter.GetHttp(
  'userSlice/getTotal',
  '/users/search?query=&page=0&size=1',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const getDetailUser = AxiosAdapter.GetHttp(
  'userSlice/GetDetailUser',
  '/users/',
  HTTP_GETTYPE.DETAIL
);

export const createUser = AxiosAdapter.HttpPost(
  'userSlice/CreateUser',
  '/users/'
);
export const updateUser = AxiosAdapter.HttpPut(
  'userSlice/UpdateUser',
  '/users/'
);
export const updateSelf = AxiosAdapter.HttpPut(
  'userSlice/UpdateSelf',
  '/users/'
);
export const deleteUser = AxiosAdapter.HttpDelete(
  'userSlice/DeleteUser',
  '/users/'
);
export const deleteMultiUser = AxiosAdapter.HttpPost(
  'userSlice/DeleteMulti',
  '/users/batch-delete',
  false
);

export const userSlice = createSlice({
  name: 'userSlice',
  initialState: {
    listUser: null,
    totalUser: 0,
    accInfo: null,
    userDetail: null,

    errorGetList: null,
    statusGetAll: null,

    err: null,
    statusGetDetail: null,
    statusDeleteMulti: null,
    statusUpdateSelf: null,
    statusCreateUser: null,
    statusGetCurrent: null,
    statusUpdateUser: null,
    statusDeleteUser: null,
    statusGetTotal: null
  },

  reducers: {
    resetStatus: state => {
      state.statusGetAll = null;
      state.statusGetDetail = null;
      state.statusCreateUser = null;
      state.statusUpdateUser = null;
      state.statusDeleteUser = null;
      state.statusDeleteMulti = null;
    },
    sortUserByName: (state, action) => {
      if (action.payload === SORT_TYPE.DESC) {
        state.listUser = ArrUtil.sortDescByKey(state.listUser, 'name');
      } else {
        state.listUser = ArrUtil.sortAscByKey(state.listUser, 'name');
      }
    },
    clearAccInfo: state => {
      state.accInfo = null;
    }
    //  every reducer must export to user
  },
  extraReducers: {
    [getListUser.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.listUser = null;
      state.totalUser = 0;
    },
    [getListUser.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.listUser = action.payload.data;
      state.totalUser = parseInt(action.payload.total);
    },
    [getListUser.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================get detail=================================*/
    [getDetailUser.pending]: state => {
      state.statusGetDetail = STATUS_API.PENDING;
      state.err = null;
      state.userDetail = null;
    },
    [getDetailUser.fulfilled]: (state, action) => {
      state.statusGetDetail = STATUS_API.SUCCESS;
      state.userDetail = action.payload;
    },
    [getDetailUser.rejected]: (state, action) => {
      state.statusGetDetail = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [getTotalUser.pending]: state => {
      state.statusGetTotal = STATUS_API.PENDING;
      state.err = null;
    },
    [getTotalUser.fulfilled]: (state, action) => {
      state.statusGetTotal = STATUS_API.SUCCESS;
      state.totalUser = parseInt(action.payload.total);
    },
    [getTotalUser.rejected]: (state, action) => {
      state.statusGetTotal = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    /*============================create=================================*/

    [createUser.pending]: state => {
      state.statusCreateUser = STATUS_API.PENDING;
      state.err = null;
    },
    [createUser.fulfilled]: (state, action) => {
      state.statusCreateUser = STATUS_API.SUCCESS;
      if (!state.listUser) state.listUser = [];
      if (state.listUser.length < PAGE_SIZE_LIST)
        state.listUser.unshift(action.payload);
      else {
        state.listUser.pop();
        state.listUser.unshift(action.payload);
      }
    },
    [createUser.rejected]: (state, action) => {
      state.statusCreateUser = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    [updateSelf.pending]: state => {
      state.statusUpdateSelf = STATUS_API.PENDING;
      state.err = null;
    },
    [updateSelf.fulfilled]: (state, action) => {
      state.statusUpdateSelf = STATUS_API.SUCCESS;
      state.accInfo = action.payload;
    },
    [updateSelf.rejected]: (state, action) => {
      state.statusUpdateSelf = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================update=================================*/

    [updateUser.pending]: state => {
      state.statusUpdateUser = STATUS_API.PENDING;
      state.err = null;
    },
    [updateUser.fulfilled]: (state, action) => {
      state.statusUpdateUser = STATUS_API.SUCCESS;
      state.listUser = state.listUser.map(item => {
        if (item.id === action.payload.id) {
          return action.payload;
        } else return item;
      });
    },

    [updateUser.rejected]: (state, action) => {
      state.statusUpdateUser = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    [GetAccInfo.pending]: state => {
      state.statusGetCurrent = STATUS_API.PENDING;
      state.err = null;
    },
    [GetAccInfo.fulfilled]: (state, action) => {
      state.statusGetCurrent = STATUS_API.SUCCESS;
      state.accInfo = action.payload;
    },
    [GetAccInfo.rejected]: (state, action) => {
      state.statusGetCurrent = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    /*============================delete=================================*/
    [deleteUser.pending]: state => {
      state.statusDeleteUser = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteUser.fulfilled]: (state, action) => {
      state.statusDeleteUser = STATUS_API.SUCCESS;
      state.totalUser -= 1;
      let deletedId = state.listUser.findIndex(
        item => item.id === action.payload
      );
      state.listUser.splice(deletedId, deletedId + 1);
    },
    [deleteUser.rejected]: (state, action) => {
      state.statusDeleteUser = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [deleteMultiUser.pending]: state => {
      state.statusDeleteMulti = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteMultiUser.fulfilled]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.SUCCESS;
      state.totalUser -= action.payload.length;
      let listDeleted = action.payload;
      for (const id of listDeleted) {
        let index = state.listUser.findIndex(item => item.id === id);
        state.listUser.splice(index, 1);
      }
    },
    [deleteMultiUser.rejected]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    }
  }
});
export const { resetStatus, sortUserByName, clearAccInfo } = userSlice.actions;

export default userSlice.reducer;
