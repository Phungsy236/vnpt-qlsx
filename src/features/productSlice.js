import { createSlice } from '@reduxjs/toolkit';
import {
  HTTP_GETTYPE,
  PAGE_SIZE_LIST,
  SORT_TYPE,
  STATUS_API
} from 'src/app/constant/config';
import { error } from 'src/app/constant/error_message';
import { mapStringToObjectProperties } from 'src/app/utils/apiService';
import AxiosAdapter from './AxiosAdapter';
import { ArrUtil } from '../app/utils/commomService';

export const getListProduct = AxiosAdapter.GetHttp(
  'ProductSlice/getAll',
  '/product/search',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const getAllProduct = AxiosAdapter.GetHttp(
  'ProductSlice/getAllNoPagination',
  '/product/search?query=&page=0&size=10000',
  HTTP_GETTYPE.ALL
);
export const getTotalProduct = AxiosAdapter.GetHttp(
  'ProductSlice/getTotal',
  '/product/search?query=&page=0&size=1',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const getDetailProduct = AxiosAdapter.GetHttp(
  'productSlice/GetDetailProduct',
  '/product/',
  HTTP_GETTYPE.DETAIL
);
export const deleteMultiProduct = AxiosAdapter.HttpPost(
  'productSlice/DeleteMulti',
  '/product/batch-delete',
  false
);
export const createProduct = AxiosAdapter.HttpPost(
  'productSlice/CreateProduct',
  '/product'
);
export const updateProduct = AxiosAdapter.HttpPut(
  'productSlice/UpdateProduct',
  '/product/'
);
export const deleteProduct = AxiosAdapter.HttpDelete(
  'productSlice/DeleteProduct',
  '/product/'
);

export const productSlice = createSlice({
  name: 'productSlice',
  initialState: {
    listProduct: null,
    allProduct: null,
    totalProduct: 0,
    errorGetList: null,
    statusGetAll: null,

    err: null,
    productDetail: null,
    statusGetDetail: null,
    statusDeleteMulti: null,
    statusCreateProduct: null,
    statusUpdateProduct: null,
    statusDeleteProduct: null,
    statusGetTotal: null
  },

  reducers: {
    resetStatus: state => {
      state.statusGetAll = null;
      state.statusGetDetail = null;
      state.statusCreateProduct = null;
      state.statusUpdateProduct = null;
      state.statusDeleteProduct = null;
      state.statusDeleteMulti = null;
      state.statusGetTotal = null;
    },
    sortProductByName: (state, action) => {
      if (action.payload === SORT_TYPE.DESC) {
        state.listProduct = ArrUtil.sortDescByKey(state.listProduct, 'name');
      } else {
        state.listProduct = ArrUtil.sortAscByKey(state.listProduct, 'name');
      }
    }
    //  every reducer must export to user
  },
  extraReducers: {
    [getListProduct.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.listProduct = null;
      state.totalProduct = 0;
    },
    [getListProduct.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.listProduct = action.payload.data;
      state.totalProduct = parseInt(action.payload.total);
    },
    [getListProduct.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [getAllProduct.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.allProduct = null;
    },
    [getAllProduct.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.allProduct = action.payload;
    },
    [getAllProduct.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    [getTotalProduct.pending]: state => {
      state.statusGetTotal = STATUS_API.PENDING;
      state.err = null;
    },
    [getTotalProduct.fulfilled]: (state, action) => {
      state.statusGetTotal = STATUS_API.SUCCESS;
      state.totalProduct = parseInt(action.payload.total);
    },
    [getTotalProduct.rejected]: (state, action) => {
      state.statusGetTotal = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================get detail=================================*/
    [getDetailProduct.pending]: state => {
      state.statusGetDetail = STATUS_API.PENDING;
      state.err = null;
      state.productDetail = null;
    },
    [getDetailProduct.fulfilled]: (state, action) => {
      state.statusGetDetail = STATUS_API.SUCCESS;
      state.productDetail = action.payload;
    },
    [getDetailProduct.rejected]: (state, action) => {
      state.statusGetDetail = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================create=================================*/

    [createProduct.pending]: state => {
      state.statusCreateProduct = STATUS_API.PENDING;
      state.err = null;
    },
    [createProduct.fulfilled]: (state, action) => {
      state.statusCreateProduct = STATUS_API.SUCCESS;
      state.totalProduct += 1;
      if (!state.listProduct) state.listProduct = [];
      if (state.listProduct.length < PAGE_SIZE_LIST)
        state.listProduct.unshift(action.payload);
      else {
        state.listProduct.pop();
        state.listProduct.unshift(action.payload);
      }
    },
    [createProduct.rejected]: (state, action) => {
      state.statusCreateProduct = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================update=================================*/

    [updateProduct.pending]: state => {
      state.statusUpdateProduct = STATUS_API.PENDING;
      state.err = null;
    },
    [updateProduct.fulfilled]: (state, action) => {
      state.statusUpdateProduct = STATUS_API.SUCCESS;
      state.listProduct = state.listProduct.map(item => {
        if (item.id === action.payload.id) {
          return action.payload;
        } else return item;
      });
    },
    [updateProduct.rejected]: (state, action) => {
      state.statusUpdateProduct = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    /*============================delete=================================*/
    [deleteProduct.pending]: state => {
      state.statusDeleteProduct = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteProduct.fulfilled]: (state, action) => {
      state.statusDeleteProduct = STATUS_API.SUCCESS;
      state.totalProduct -= 1;
      let deletedId = state.listProduct.findIndex(
        item => item.id === action.payload
      );
      state.listProduct.splice(deletedId, deletedId + 1);
    },
    [deleteProduct.rejected]: (state, action) => {
      state.statusDeleteProduct = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [deleteMultiProduct.pending]: state => {
      state.statusDeleteMulti = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteMultiProduct.fulfilled]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.SUCCESS;
      state.totalProduct -= action.payload.length;
      let listDeleted = action.payload;
      for (const id of listDeleted) {
        let index = state.listProduct.findIndex(item => item.id === id);
        state.listProduct.splice(index, index + 1);
      }
    },
    [deleteMultiProduct.rejected]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    }
  }
});
export const { resetStatus, sortProductByName } = productSlice.actions;

export default productSlice.reducer;
