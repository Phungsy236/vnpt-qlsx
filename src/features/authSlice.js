import { createSlice } from '@reduxjs/toolkit';
import { HEADER_AUTH_KEY_NAME, STATUS_API } from 'src/app/constant/config';
import Cookie from 'js-cookie';
import { mapStringToObjectProperties } from 'src/app/utils/apiService';
import AxiosAdapter from './AxiosAdapter';
import { error } from 'src/app/constant/error_message';

export const getToken = AxiosAdapter.HttpPost(
  'authSlice/getToken',
  '/auth/token'
);

export const authSlice = createSlice({
  name: 'authSlice',
  initialState: {
    statusLogin: null,
    err: '',
    isLogin: false
  },
  reducers: {
    logOutAction: state => {
      state.isLogin = false;
      state.statusLogin = null;
      Cookie.remove('access-token');
    },
    resetStatus: state => {
      state.statusLogin = null;
    }
  },
  extraReducers: {
    [getToken.pending]: state => {
      state.err = '';
      state.statusLogin = STATUS_API.PENDING;
      state.errGetting = null;
    },
    [getToken.fulfilled]: (state, action) => {
      state.statusLogin = STATUS_API.SUCCESS;
      Cookie.set(HEADER_AUTH_KEY_NAME, action.payload['id_token']);
      // localStorage.setItem('access-token', action.payload.payload['token']);
    },
    [getToken.rejected]: (state, action) => {
      state.statusLogin = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    }
  }
});
export const { logOutAction } = authSlice.actions;

export default authSlice.reducer;
