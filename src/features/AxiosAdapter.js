import axios from 'axios';
import { createAsyncThunk } from '@reduxjs/toolkit';
import Cookie from 'js-cookie';
import {
  HEADER_AUTH_KEY_NAME,
  HTTP_GETTYPE,
  MSG_TIMEOUT_REQUEST,
  TIMEOUT_DEFAULT
} from 'src/app/constant/config';
import { _convertObjectToQuery } from 'src/app/utils/apiService';
class AxiosAdapter {
  static HttpPost(nameSlice, url, getServerResponse = true) {
    return createAsyncThunk(`${nameSlice}`, (payload, { rejectWithValue }) => {
      return new Promise((resolve, reject) => {
        axios({
          url: process.env.REACT_APP_BACKEND_URL + url,
          method: 'POST',
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            Authorization: 'Bearer ' + Cookie.get(HEADER_AUTH_KEY_NAME)
          },
          data: payload,
          timeout: TIMEOUT_DEFAULT
        })
          .then(res => {
            if (!getServerResponse) resolve(payload);
            resolve(res.data);
          })
          .catch(err => {
            if (err.code === 'ECONNABORTED') reject(MSG_TIMEOUT_REQUEST);
            if (!err.response) reject(err);
            reject(rejectWithValue(err.response?.data));
          });
      });
    });
  }
  static HttpPut(nameSlice, url, getServerResponse = true) {
    return createAsyncThunk(`${nameSlice}`, (payload, { rejectWithValue }) => {
      return new Promise((resolve, reject) => {
        axios({
          url: process.env.REACT_APP_BACKEND_URL + url + `${payload.id}`,
          method: 'PUT',
          headers: {
            'Access-Control-Allow-Origin': true,
            'Content-Type': 'application/json; charset=utf-8',
            Authorization: 'Bearer ' + Cookie.get('access-token')
          },
          data: payload,
          timeout: TIMEOUT_DEFAULT
        })
          .then(res => {
            if (!getServerResponse) resolve(payload);
            resolve(res.data);
          })
          .catch(err => {
            if (err.code === 'ECONNABORTED') reject(MSG_TIMEOUT_REQUEST);
            if (!err.response) reject(err);
            reject(rejectWithValue(err.response?.data));
          });
      });
    });
  }
  static GetHttp(nameSlice, url, type) {
    return createAsyncThunk(nameSlice, (payload, { rejectWithValue }) => {
      return new Promise((resolve, reject) => {
        let tmp = '';
        if (type === HTTP_GETTYPE.ALL) {
          tmp = process.env.REACT_APP_BACKEND_URL + url;
        } else if (type === HTTP_GETTYPE.ALL_PAGINATION) {
          tmp =
            process.env.REACT_APP_BACKEND_URL +
            url +
            '?' +
            _convertObjectToQuery(payload);
        } else tmp = process.env.REACT_APP_BACKEND_URL + url + payload;
        axios({
          url: tmp,
          method: 'GET',
          headers: {
            'Access-Control-Allow-Origin': true,
            'Content-Type': 'application/json; charset=utf-8',
            // Authorization: 'Bearer ' + Cookie.get('access-token'),
            Authorization: 'Bearer ' + Cookie.get(HEADER_AUTH_KEY_NAME)
          },
          timeout: TIMEOUT_DEFAULT
        })
          .then(res => {
            if (type === HTTP_GETTYPE.ALL_PAGINATION) {
              resolve({ data: res.data, total: res.headers['x-total-count'] });
            } else resolve(res.data);
          })
          .catch(err => {
            if (err.code === 'ECONNABORTED') reject(MSG_TIMEOUT_REQUEST);
            if (!err.response) reject(err);
            reject(rejectWithValue(err.response?.data));
          });
      });
    });
  }
  static HttpDelete(nameSlice, url) {
    return createAsyncThunk(nameSlice, (payload, { rejectWithValue }) => {
      return new Promise((resolve, reject) => {
        axios({
          url: process.env.REACT_APP_BACKEND_URL + url + `${payload.id}`,
          method: 'Delete',
          headers: {
            'Access-Control-Allow-Origin': true,
            'Content-Type': 'application/json; charset=utf-8',
            Authorization: 'Bearer ' + Cookie.get('access-token')
          },
          timeout: TIMEOUT_DEFAULT
        })
          .then(res => resolve(payload.id))
          .catch(err => {
            if (err.code === 'ECONNABORTED') reject(MSG_TIMEOUT_REQUEST);
            if (!err.response) reject(err);
            reject(rejectWithValue(err.response?.data));
          });
      });
    });
  }
}
export default AxiosAdapter;
