import { createSlice } from '@reduxjs/toolkit';
import {
  HTTP_GETTYPE,
  PAGE_SIZE_LIST,
  SORT_TYPE,
  STATUS_API
} from 'src/app/constant/config';
import { error } from 'src/app/constant/error_message';
import { mapStringToObjectProperties } from 'src/app/utils/apiService';
import AxiosAdapter from './AxiosAdapter';
import { ArrUtil } from '../app/utils/commomService';
import { Role } from '../app/models/Role';

export const getListRole = AxiosAdapter.GetHttp(
  'RoleSlice/getAll',
  '/roles/search',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const getAllRole = AxiosAdapter.GetHttp(
  'RoleSlice/getAllNoPagination',
  '/roles/search?query=&page=0&size=10000',
  HTTP_GETTYPE.ALL
);
export const getTotalRole = AxiosAdapter.GetHttp(
  'RoleSlice/getTotal',
  '/roles/search?query=&page=0&size=1',
  HTTP_GETTYPE.ALL_PAGINATION
);
export const getDetailRole = AxiosAdapter.GetHttp(
  'roleSlice/GetDetailRole',
  '/roles/',
  HTTP_GETTYPE.DETAIL
);
export const deleteMultiRole = AxiosAdapter.HttpPost(
  'roleSlice/DeleteMulti',
  '/roles/batch-delete',
  false
);
export const createRole = AxiosAdapter.HttpPost(
  'roleSlice/CreateRole',
  '/roles'
);
export const updateRole = AxiosAdapter.HttpPut(
  'roleSlice/UpdateRole',
  '/roles/'
);
export const deleteRole = AxiosAdapter.HttpDelete(
  'roleSlice/DeleteRole',
  '/roles/'
);

export const roleSlice = createSlice({
  name: 'roleSlice',
  initialState: {
    listRole: null,
    allRole: null,
    totalRole: 0,
    errorGetList: null,
    statusGetAll: null,

    err: null,
    roleDetail: null,
    statusGetDetail: null,
    statusDeleteMulti: null,
    statusCreateRole: null,
    statusUpdateRole: null,
    statusDeleteRole: null,
    statusGetTotal: null
  },

  reducers: {
    resetStatus: state => {
      state.statusGetAll = null;
      state.statusGetDetail = null;
      state.statusCreateRole = null;
      state.statusUpdateRole = null;
      state.statusDeleteRole = null;
      state.statusDeleteMulti = null;
      state.statusGetTotal = null;
      state.err = null;
      state.errorGetList = null;
    },

    sortRoleByName: (state, action) => {
      if (action.payload === SORT_TYPE.DESC) {
        state.listRole = ArrUtil.sortDescByKey(state.listRole, 'name');
      } else {
        state.listRole = ArrUtil.sortAscByKey(state.listRole, 'name');
      }
    }
    //  every reducer must export to user
  },
  extraReducers: {
    [getListRole.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.listRole = null;
      state.totalRole = 0;
    },
    [getListRole.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.listRole = action.payload.data;
      state.totalRole = parseInt(action.payload.total);
    },
    [getListRole.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [getAllRole.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.allRole = null;
    },
    [getAllRole.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.allRole = action.payload;
    },
    [getAllRole.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    [getTotalRole.pending]: state => {
      state.statusGetTotal = STATUS_API.PENDING;
      state.err = null;
    },
    [getTotalRole.fulfilled]: (state, action) => {
      state.statusGetTotal = STATUS_API.SUCCESS;
      state.totalRole = parseInt(action.payload.total);
    },
    [getTotalRole.rejected]: (state, action) => {
      state.statusGetTotal = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================get detail=================================*/
    [getDetailRole.pending]: state => {
      state.statusGetDetail = STATUS_API.PENDING;
      state.err = null;
      state.roleDetail = null;
    },
    [getDetailRole.fulfilled]: (state, action) => {
      state.statusGetDetail = STATUS_API.SUCCESS;
      state.roleDetail = action.payload;
    },
    [getDetailRole.rejected]: (state, action) => {
      state.statusGetDetail = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================create=================================*/

    [createRole.pending]: state => {
      state.statusCreateRole = STATUS_API.PENDING;
      state.err = null;
    },
    [createRole.fulfilled]: (state, action) => {
      state.statusCreateRole = STATUS_API.SUCCESS;
      state.totalRole += 1;
      if (!state.listRole) state.listRole = [];
      if (state.listRole.length < PAGE_SIZE_LIST)
        state.listRole.unshift(action.payload);
      else {
        state.listRole.pop();
        state.listRole.unshift(action.payload);
      }
    },
    [createRole.rejected]: (state, action) => {
      state.statusCreateRole = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================update=================================*/

    [updateRole.pending]: state => {
      state.statusUpdateRole = STATUS_API.PENDING;
      state.err = null;
    },
    [updateRole.fulfilled]: (state, action) => {
      state.statusUpdateRole = STATUS_API.SUCCESS;
      state.listRole = state.listRole.map(item => {
        if (item.id === action.payload.id) {
          return action.payload;
        } else return item;
      });
    },
    [updateRole.rejected]: (state, action) => {
      state.statusUpdateRole = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    /*============================delete=================================*/
    [deleteRole.pending]: state => {
      state.statusDeleteRole = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteRole.fulfilled]: (state, action) => {
      state.statusDeleteRole = STATUS_API.SUCCESS;
      state.totalRole -= 1;
      let deletedId = state.listRole.findIndex(
        item => item.id === action.payload
      );
      state.listRole.splice(deletedId, 1);
    },
    [deleteRole.rejected]: (state, action) => {
      state.statusDeleteRole = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [deleteMultiRole.pending]: state => {
      state.statusDeleteMulti = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteMultiRole.fulfilled]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.SUCCESS;
      state.totalRole -= action.payload.length;
      let listDeleted = action.payload;
      for (const id of listDeleted) {
        let index = state.listRole.findIndex(item => item.id === id);
        state.listRole.splice(index, 1);
      }
    },
    [deleteMultiRole.rejected]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    }
  }
});
export const { resetStatus, sortRoleByName } = roleSlice.actions;

export default roleSlice.reducer;
