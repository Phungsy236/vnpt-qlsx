import { createSlice } from '@reduxjs/toolkit';
import {
  HTTP_GETTYPE,
  PAGE_SIZE_LIST,
  SORT_TYPE,
  STATUS_API
} from 'src/app/constant/config';
import { error } from 'src/app/constant/error_message';
import { mapStringToObjectProperties } from 'src/app/utils/apiService';
import AxiosAdapter from './AxiosAdapter';
import { ArrUtil } from '../app/utils/commomService';
import { LifeCycle } from '../app/models/LifeCycle';

export const getListLifeCycle = AxiosAdapter.GetHttp(
  'LifeCycleSlice/getAll',
  '/cycleTime/search',
  HTTP_GETTYPE.ALL_PAGINATION
);

export const getDetailLifeCycle = AxiosAdapter.GetHttp(
  'cycleTimeSlice/GetDetailLifeCycle',
  '/cycleTime/',
  HTTP_GETTYPE.DETAIL
);
export const deleteMultiLifeCycle = AxiosAdapter.HttpPost(
  'cycleTimeSlice/DeleteMulti',
  '/cycleTime/batch-delete',
  false
);
export const createLifeCycle = AxiosAdapter.HttpPost(
  'cycleTimeSlice/CreateLifeCycle',
  '/cycleTime/'
);
export const updateLifeCycle = AxiosAdapter.HttpPut(
  'cycleTimeSlice/UpdateLifeCycle',
  '/cycleTime/'
);
export const deleteLifeCycle = AxiosAdapter.HttpDelete(
  'cycleTimeSlice/DeleteLifeCycle',
  '/cycleTime/'
);

export const cycleTimeSlice = createSlice({
  name: 'cycleTimeSlice',
  initialState: {
    listLifeCycle: null,
    totalLifeCycle: 0,
    errorGetList: null,
    statusGetAll: null,

    err: null,
    cycleTimeDetail: null,
    statusGetDetail: null,
    statusDeleteMulti: null,
    statusCreateLifeCycle: null,
    statusUpdateLifeCycle: null,
    statusDeleteLifeCycle: null
  },

  reducers: {
    resetStatus: state => {
      state.statusGetAll = null;
      state.statusGetDetail = null;
      state.statusCreateLifeCycle = null;
      state.statusUpdateLifeCycle = null;
      state.statusDeleteLifeCycle = null;
    },
    sortLifeCycleByName: (state, action) => {
      if (action.payload === SORT_TYPE.DESC) {
        state.listLifeCycle = ArrUtil.sortDescByKey(
          state.listLifeCycle,
          'name'
        );
      } else {
        state.listLifeCycle = ArrUtil.sortAscByKey(state.listLifeCycle, 'name');
      }
    }
    //  every reducer must export to user
  },
  extraReducers: {
    [getListLifeCycle.pending]: state => {
      state.statusGetAll = STATUS_API.PENDING;
      state.errorGetList = null;
      state.listLifeCycle = null;
      state.totalLifeCycle = 0;
    },
    [getListLifeCycle.fulfilled]: (state, action) => {
      state.statusGetAll = STATUS_API.SUCCESS;
      state.listLifeCycle = action.payload.data;
      state.totalLifeCycle = parseInt(action.payload.total);
    },
    [getListLifeCycle.rejected]: (state, action) => {
      state.statusGetAll = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================get detail=================================*/
    [getDetailLifeCycle.pending]: state => {
      state.statusGetDetail = STATUS_API.PENDING;
      state.err = null;
      state.cycleTimeDetail = null;
    },
    [getDetailLifeCycle.fulfilled]: (state, action) => {
      state.statusGetDetail = STATUS_API.SUCCESS;
      state.cycleTimeDetail = action.payload;
    },
    [getDetailLifeCycle.rejected]: (state, action) => {
      state.statusGetDetail = STATUS_API.ERROR;
      state.errorGetList =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================create=================================*/

    [createLifeCycle.pending]: state => {
      state.statusCreateLifeCycle = STATUS_API.PENDING;
      state.err = null;
    },
    [createLifeCycle.fulfilled]: (state, action) => {
      state.statusCreateLifeCycle = STATUS_API.SUCCESS;
      state.totalLifeCycle += 1;
      if (!state.listLifeCycle) state.listLifeCycle = [];
      if (state.listLifeCycle.length < PAGE_SIZE_LIST)
        state.listLifeCycle.unshift(action.payload);
      else {
        state.listLifeCycle.pop();
        state.listLifeCycle.unshift(action.payload);
      }
    },
    [createLifeCycle.rejected]: (state, action) => {
      state.statusCreateLifeCycle = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },

    /*============================update=================================*/

    [updateLifeCycle.pending]: state => {
      state.statusUpdateLifeCycle = STATUS_API.PENDING;
      state.err = null;
    },
    [updateLifeCycle.fulfilled]: (state, action) => {
      state.statusUpdateLifeCycle = STATUS_API.SUCCESS;
      state.listLifeCycle = state.listLifeCycle.map(item => {
        if (item.id === action.payload.id) {
          return action.payload;
        } else return item;
      });
    },
    [updateLifeCycle.rejected]: (state, action) => {
      state.statusUpdateLifeCycle = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    /*============================delete=================================*/
    [deleteLifeCycle.pending]: state => {
      state.statusDeleteLifeCycle = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteLifeCycle.fulfilled]: (state, action) => {
      state.statusDeleteLifeCycle = STATUS_API.SUCCESS;
      state.totalLifeCycle -= 1;
      let deletedId = state.listLifeCycle.findIndex(
        item => item.id === action.payload
      );
      state.listLifeCycle.splice(deletedId, 1);
    },
    [deleteLifeCycle.rejected]: (state, action) => {
      state.statusDeleteLifeCycle = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    },
    [deleteMultiLifeCycle.pending]: state => {
      state.statusDeleteMulti = STATUS_API.PENDING;
      state.err = null;
    },
    [deleteMultiLifeCycle.fulfilled]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.SUCCESS;
      state.totalLifeCycle -= action.payload.length;
      let listDeleted = action.payload;
      for (const id of listDeleted) {
        let index = state.listLifeCycle.findIndex(item => item.id === id);
        state.listLifeCycle.splice(index, 1);
      }
    },
    [deleteMultiLifeCycle.rejected]: (state, action) => {
      state.statusDeleteMulti = STATUS_API.ERROR;
      state.err =
        mapStringToObjectProperties(action.payload?.errorKey, error) ||
        action.error?.message;
    }
  }
});
export const { resetStatus, sortLifeCycleByName } = cycleTimeSlice.actions;

export default cycleTimeSlice.reducer;
