
## How to run
1. Install package
```
npm install
```
2. Add environment in .env file 

3. Run project
``` 
npm start
```
4. For dev , use mockApi with json-server 
   1.  Add json-server as global npm. 
    ```
    install i json-server -g
   ```
   2. Start 
   ```
   json-server  --watch fakedb.json --port 3001
   ```
   *fakedb.json file locate on current point cursor*
   3. Usage of api method see in :
      [Json-server repo](https://github.com/typicode/json-server).
    
